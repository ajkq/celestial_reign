<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="color-scheme" content="light dark">
  <link rel="icon" type="image/svg+xml" href="/favicon.svg">
  <link rel="mask-icon" href="/mask-icon.svg" color="#000000">
  <title>Preview</title>
  <link href="/style.css?v=1.0" rel="stylesheet">
</head>

<body>
  <h1>Celestial reign</h1>
  <h2>Preview</h2>
