<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta name="color-scheme" content="light dark">
  <link rel="icon" type="image/svg+xml" href="/favicon.svg">
  <link rel="mask-icon" href="/mask-icon.svg" color="#000000">
  <title>Documentation</title>
  <link href="/style.css?v=1.0" rel="stylesheet">
  <style>
    /* show first column only */
    table#list tr th:nth-child(n+2),
    table#list tr td:nth-child(n+2) {
      display: none;
    }
  </style>
</head>

<body>
  <h1>Celestial reign</h1>
  <h2>Documentation</h2>
  <p>Use HTML format for on-screen reading, PDF for printing</p>
