# Documentation

These documents are rendered from source files [here](documentation).

Click below links to download self-contained HTML files (including images) for on-screen reading and PDF files for printing.

## Install guide

[HTML](https://gitlab.com/ajkq/celestial_reign/-/raw/main/share/html/doc/install_guide.html?inline=false)

[PDF](https://gitlab.com/ajkq/celestial_reign/-/raw/main/share/html/doc/install_guide.pdf?inline=false)

## User guide

[HTML](https://gitlab.com/ajkq/celestial_reign/-/raw/main/share/html/doc/user_guide.html?inline=false)

[PDF](https://gitlab.com/ajkq/celestial_reign/-/raw/main/share/html/doc/user_guide.pdf?inline=false)
