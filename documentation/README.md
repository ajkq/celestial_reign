# Documentation

Render manuals and drawings

## Drawings

#### Requirements

Software: npm, Graphviz, svgo

macOS:

```
brew install graphviz node
sudo npm install -g svgo
```

Debian:

```
sudo apt install graphviz npm
sudo npm install -g svgo
```

#### Render

```
cd drawings
dot -Tsvg standard_install.gv -o standard_install.svg; svgo --final-newline standard_install.svg
dot -Tsvg standalone_install.gv -o standalone_install.svg; svgo --final-newline standalone_install.svg
dot -Tsvg scr_multiswitch.gv -o scr_multiswitch.svg; svgo --final-newline scr_multiswitch.svg
dot -Tsvg architecture.gv -o architecture.svg; svgo --final-newline architecture.svg
```

## Manuals

Manuals are written in GFM markdown and need to be rendered as HTML and
PDF for reading via web interface.

#### Requirements

Software: Pandoc, GNU sed

To satisfy GNU sed dependency on macOS, install with homebrew, then
export PATH before launching script:

macOS:

```
brew install gnu-sed pandoc
export PATH="/usr/local/opt/gnu-sed/libexec/gnubin:$PATH"
```

Debian:

```
sudo apt install pandoc
```

#### Editing

To force a page break in the PDF output, insert
`<div class="pagebreak"></div>`

The element is filtered when creating html output, and ignored by GFM
renderers.

After making changes, render PDF output and check if manual page breaks
need to be adjusted.

### HTML

HTML documents are rendered self-contained with images embedded.

#### Render specific manual

```
./render-docs.sh html user
./render-docs.sh html install
```

#### Render all manuals

```
./render-docs.sh html
```

Export HTML_TARGET_D to specify output directory, defaults to
`../share/html/doc`

### PDF

#### Requirements

Software: TeX distribution, pipx, pandoc-minted, pygments, cairosvg, qpdf,
exiftool, inkscape, pandoc

macOS:

```
brew install --cask mactex inkscape
brew install python exiftool qpdf pygments pipx pandoc
pipx install cairosvg
pipx install pandoc-minted
pipx ensurepath
```

Debian:

```
sudo apt install texlive texlive-luatex texlive-latex-extra \
  exiftool qpdf inkscape cairosvg python3-pygments pipx pandoc
pipx install pandoc-minted
pipx ensurepath
```

Fonts: [DejaVu Sans](https://dejavu-fonts.github.io),
[Office Code Pro](https://github.com/nathco/Office-Code-Pro)

#### Render specific manual

```
./render-docs.sh pdf user
./render-docs.sh pdf install
```

#### Render all manuals

```
./render-docs.sh pdf
```

Export PDF_TARGET_D to specify output directory, defaults to
`../share/html/doc`

## Miscellaneous

### Generate images

#### Requirements

Software: imagemagick ≥7, pipx, cairosvg

macOS:

```
brew install imagemagick pipx
pipx install cairosvg
```

#### Convert image

Gitlab Avatar image:

```
cairosvg --output-width 1200 -f png ../share/html/favicon.svg -o $HOME/avatar.png
mogrify -background none -bordercolor none -border 15% $HOME/avatar.png
```

Xfce icon:
```
cairosvg --output-width 128 -f png ../share/html/favicon.svg -o ../share/icon.png
```
