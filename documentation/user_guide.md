# Basics

User interaction happens through Adminer, a database management web
application. Access the system through a web browser, then click
"Management". Enter Username and Password to log in. Other fields are
pre-filled.

Click logout when done. Re-opening the browser allows resuming the
session for a limited time. A password change is possible without
entering the existing password due to technical limitations.

# Channel change

## Immediate

1. Click ›select‹ channel in the side pane.

   On a mobile device, the side pane may move to the bottom of the page.

2. Click ›Modify‹ in the top left of the table and edit the channel
   fields of the receivers you want to change. You can also use ›edit‹
   for modifying a single receiver.

   Changes to other fields are ignored.

3. Click ›Save‹.

## Scheduled

Click ›select‹ chan_schedule in the side pane.

On a mobile device, the side pane may move to the bottom of the page.

#### Add

1. Click ›New item‹.

2. Enter rec_id, sched_channel, and datetime. Omit auto-generated
   chan_schedule_id.

3. Click ›Save‹.

#### Edit

1. Control/⌘ click the fields you want to change for modifying one or
   more entries or click edit for modifying a single entry.

2. Click ›Save‹.

   Changes to chan_schedule_id are ignored.

#### Delete

1. Select the checkbox of one or more entries.

2. Click ›Delete‹.

3. Confirm the message "Are you sure?".

## Requirements

### Channels

Channel changes may be disabled on a per-receiver basis by the
administrator, indicated by "no" in the "enabled" column of the view
"channel". The scheduler does not apply entries for disabled receivers,
and updates to existing entries are rejected.

The administrator specifies which channels are permitted for all
receivers. To look up available channels, click ›select‹ chan_catalog in
the side pane.

Further restrictions may be imposed on a per-receiver basis. To check if
some channels are denylisted for a particular receiver, click ›select‹
chan_denylist in the side pane.

An attempt to enter an unavailable/restricted channel yields an error
message.

Restrictions are checked upon entry. If the administrator denylisted a
channel after it was added to chan_schedule, only deletions of entries
referencing that channel are permitted. Update attempts are rejected.

### Date/Time

<!--
Use HTML links since GFM shows Pandoc specific target and rel verbatim.
We do not need those links in the PDF output.
-->

The time zone is Europe/London to match the TV guide:
<a href="https://www.sky.com/tv-guide" target="_blank"
rel="noreferrer noopener">https://www.sky.com/tv-guide</a>.

Date/Time can be entered in several formats:
<a href="https://www.postgresql.org/docs/15/datatype-datetime.html#DATATYPE-DATETIME-INPUT"
target="_blank" rel="noreferrer noopener">https://www.postgresql.org/docs/15/datatype-datetime.html#DATATYPE-DATETIME-INPUT</a>

* Jul 3 16:00 2021

* 2021-07-03 16:00

* today 16:00

* today 17:00 Europe/Vienna

* 2021-07-03

* tomorrow -1

00:00 is implied if time is omitted. It is possible to specify a
different time zone, e.g., a geographical zone, +2 for Central European
Summer Time, etc. After saving, the time displays converted to London
time, e.g., +1 in summer.

tomorrow -1 yields 02:00 CET / 03:00 CEST. This time is recommended when
changing channels overnight.

Date/Time needs to be in the future.

# Channel reset

To manually reset a receiver to the assigned channel, perform the
following steps. It executes the same sequence as when the analyzer
detects audio or video loss.

1. Click ›localhost‹ at the top of the page to move to the main page.

2. Scroll down to "Routines".

3. Click ›rec_reset_channel‹, enter receiver ID and click ›Call‹.

   Enter an asterisk (\*) to reset all receivers, similar to "Reset All
   Rx's" on a Global SMC SMATV Master Controller.

# Password change

1. Click ›localhost‹ at the top of the page to move to the main page.

2. Scroll down to "Routines".

3. Click ›password_change‹.

4. Enter a new password and click ›Call‹.

5. Click ›localhost‹ at the top of the page.

   This triggers a logout with an error message due to the changed
   password.

6. Log in again with the new password.

# Remote control

This feature allows for remote control commands to be sent to the
receiver.

It requires Operator-level permissions and remains hidden for regular
users.

The administrator may enable/disable remote control on a per-receiver
basis. To show available commands for receivers with the feature
enabled, click ›select‹ avail_rc in the side pane, then ›edit‹ next to
the desired receiver.

On a mobile device, the side pane may move to the bottom of the page.

## Suspend analyzer

It is recommended to temporarily suspend the video analyzer before
navigating the menus for it not to interfere:

1. Click ›localhost‹ at the top of the page to move to the main page.

2. Scroll down to "Routines".

3. Click ›analyze_suspend‹, enter the receiver ID, and click ›call‹.

   The administrator-set time until the analyzer is suspended is shown.

## Transmit commands

1. Click ›localhost‹ at the top of the page to move to the main page.

2. Scroll down to "Routines".

3. Click ›rec_remote_control‹, enter the receiver ID and one or more
   space-separated commands. Then click ›Call‹.

   If one command is not valid, none are transmitted.

## Resume analyzer

Once done, resume the analyzer.

1. Click ›localhost‹ at the top of the page to move to the main page.

2. Scroll down to "Routines".

3. Click ›analyze_continue‹, enter the receiver ID and click ›call‹.

# Receiver reset

This feature power cycles the receiver, waits approximately 2 minutes
for it to start up, takes it out of standby, and puts it on the correct
channel.

It requires Operator-level permissions and remains hidden for regular
users.

The receiver must be connected to a compatible power distribution unit,
and the feature needs to be enabled by the administrator.

## Troubleshooting

A power cycle is typically only needed when a receiver stops working
properly, which usually manifests itself in one of the following ways:

* No or black signal. The receiver does not respond to remote control
  commands, physical remote control, or front-panel button presses.

  Recommended troubleshooting steps before issuing a reset:

  + Check log of corresponding control service for the message "Receiver
    unreachable".

  + Ensure issue is not caused by multicast encoder not outputting
    stream.

* Video output is frozen, no audio. The receiver does not respond to
  remote control commands, physical remote control, or front-panel
  button presses.

* A blue no signal screen is shown for every channel. Channel changes on
  SkyHD/Sky+HD receivers also yield a red "Channel not available" error
  message.

* Audio is playing, but video output is frozen. The receiver responds to
  control/status commands according to log messages.

* Analyze log reports SSDP URL not set, thus not performing PIN
  entry/verification; Discover log reports Soap API not responding. The
  receiver responds to control commands according to log messages. TV
  service may be working, or there is no audio/video output.

## Usage

1. Click ›localhost‹ at the top of the page to move to the main page.

2. Scroll down to "Routines".

3. Click ›rec_reset_power‹, enter the receiver ID and click ›Call‹.

   More than one reset within 5 minutes is rejected.

<div class="pagebreak"></div>

# Configuration

Admin-level access is required for application configuration.

The manual refers to views and functions to interact with the system:

#### Views

Click ›select‹ next to the view to be edited in the side pane.

##### Add

1. Click ›New item‹.

2. Enter the required information.

3. Click ›Save‹.

##### Edit

1. Control/⌘ click the fields you want to change to modify one or more
   entries or click edit to modify a single entry. You can also use
   ›Modify‹ to edit multiple entries. To NULL a field, select the option
   from the drop-down menu in single entry edit mode.

2. Click ›Save‹.

##### Delete

1. Select the checkbox of one or more entries.

2. Click ›Delete‹.

3. Confirm the message "Are you sure?".

Boolean values (true/false) are represented as checkboxes in single
entry edit mode, otherwise as t or f. To enable a feature, enter t or
tick the checkbox; to disable, enter f or untick the checkbox.

#### Functions

1. Click ›localhost‹ at the top of the page to move to the main page.

2. Scroll down to "Routines".

3. Click the function you want to execute.

## Channel catalog

View: chan_catalog

##### chan_catalog_id

Auto-generated ID, omit when adding/editing channels.

##### channel

A valid TV or Radio EPG channel.

Format: 100-999 with optional leading zero.

Entries cannot be deleted while referenced.

Assignable channels cannot be used as a backup-channel. This limitation
exists since recovering a stuck receiver requires a brief switch to
a different channel. The attempt to add a channel in use as backup_chan
in view "rec_cfg" is rejected.

The catalog contains sports channels on the European beam by default, in
both Standard and High definition.

## Global receiver configuration

View: glb_rec_cfg

##### site_name

Site/vessel name for email alerts. Shortened to 20 alphanumerical
characters in email alerts.

##### analyze_suspend

30 seconds ≤ N ≤ 30 minutes; Default: 15 minutes

Duration the analyzer suspends for when requested via the function
"analyze_suspend".

##### alert_net_thresh

15 minutes ≤ N ≤ 2 days; Default: 15 minutes

Threshold for network alert after a previous successful check.

##### alert_av_thresh

15 minutes ≤ N ≤ 2 days; Default: 1 hour

Threshold for A/V loss alert after a previous successful check.

Default is chosen not to cause any false alerts whenever the ship enters
a lock.

The threshold can be set shorter if the antenna control unit is
monitored (alarm feature) or geofencing is enabled with accurate
blockage zone definitions. Alerts are skipped if blockage/alarm states
are active.

##### alert_email

Email recipients for alerting feature

<!-- Span prevents GFM autolinking -->

Address(es) need to be enclosed in curly braces, e.g.,
{email<span>@</span>example.com,
another_email<span>@</span>example.com}.

Email server/sender is configured on the system level — refer to the
installation guide.

##### alert

Enable/disable email alerting.

The system sends only one alert per issue. Once rectified and if an
alert was issued, a recovery email is sent.

Notification states are reset when the service is disabled.

Requires alert_email to be configured.

Runs cr-alert.timer.

<div class="pagebreak"></div>

Example alert message:

```
Subject: ALERT: LAB: Rec. 1 (Test): A/V loss

*** Celestial reign ***

Site name: LAB

Notification type: ALERT
Receiver ID: 1
Rec. description: Test

State: Audio/Video loss

Date/Time: Thu Sep 16 2021 13:21 +02:00

Alert threshold: 1 hour
```

##### coverage

Enable/disable coverage evaluation. Suspends analysis whenever the
vessel is in blockage or out-of-footprint.

Definition files must be placed in `/opt/celestial_reign/etc`.

Coverage files must be named `coverage*.poly`, blockage files
`blockage*.pt` or `blockage*.poly`.

Files are evaluated alphabetically.

Inside footprint is implied without coverage files.

pt files define circles, one per line. Format:
latitude,longitude,radius_meters.

The included file `blockage-locks.pt` defines lock blockage zones on the
Danube east of Vilshofen, with the center point in the middle of the
chambers.

Polygons can be drawn and saved as kml file, then converted:
`gmt kml2gmt file.kml >file.poly`.

gmt is part of Generic Mapping Tools.

Check frequency: every minute.

Requires configured Global navigation satellite system (GNSS) data
source for gpsd.

cr-latlon.service maintains two files: `latlon` and `latlon_last`. The
former is deleted on GNSS loss. The latter is used by
cr-coverage.service and deleted if it is older than 2 hours (clears
blockage state). Using the cached location ensures that GNSS loss when
descending a lock does not clear blockage state.

Runs cr-latlon.service.

Runs cr-coverage.timer.

##### preview

Enable/disable preview rendering every 30 seconds.

Previews are accessible from the web management landing page. Uses
bandwidth-efficient AVIF format for faster load times over slow
satellite internet connections instead of JPEG if the machine has ≥4 CPU
cores. Optionally supports WebP. Refer to the installation guide.

The web page shows a “Feature inactive” message if the feature is
disabled or before the first run after enabling it/system boot.

<div class="pagebreak"></div>

Images rendered from local capture devices are stretched to a 16:9
aspect ratio in case capture runs at non-widescreen resolution.

Runs cr-preview.timer.

##### alarm

Enable/disable alarm feature.

Skips analysis and verification if the file
`/opt/celestial_reign/var/alarm` is present.

Allows, for example, monitoring of the Antenna control unit by an
external script which creates the alarm file if signal is below
threshold.

## Feed action configuration

View: feed_act

feed_act_group is referenced from rec_cfg for no_feed_act/res_act
feature. Four pre-defined entries are present, which can be changed.

These features trigger a custom script when a network stream goes
offline or switches to standard definition. See detailed description
under receiver configuration.

Entries cannot be deleted while referenced.

Changing settings while a delay is active does not have an effect. To
manually reset active delays, execute the function
"analyze_reset_feed_act_delay".

Create the script executed on a no_feed_act/res_act state:

```
cd /opt/celestial_reign/bin
nano feed-act.sh
chmod +x feed-act.sh
```

See `/opt/celestial_reign/bin/feed-act.sh.example` containing snippets
for rebooting Teracue ENC-400-HDMI2 and TBS 2603 encoders. The latter
switch to standard definition after a receiver reboot which
automatically happens for software updates at night. Rebooting the
encoder reverts to correct receiver output resolution.

feed_act_group is passed as parameter 1, "res" or "feed" as parameter 2.

Initial delay for both types is registered when the lock is acquired
since a no feed state caused by a receiver reboot may seamlessly change
to incorrect resolution.

Re-execution delay is set when the script is executed.

##### feed_act_group

Positive number

Cannot be changed while referenced.

##### feed_act_ini_delay

1 minute ≤ N ≤ 6 hours

Initial delay before action is taken when a no feed state occurs.

##### feed_act_rex_delay

5 minutes ≤ N ≤ 1 month

Re-execution delay before action is taken when a no feed state reoccurs.

##### res_act_ini_delay

1 minute ≤ N ≤ 6 hours

Initial delay before action is taken when an incorrect resolution state
occurs.

##### res_act_rex_delay

5 minutes ≤ N ≤ 1 month

Re-execution delay before action is taken when an incorrect resolution
state reoccurs.

## PDU connection

View: pdu_conn

Contains power distribution unit (PDU) connections which are referenced
from rec_cfg.

Entries cannot be deleted while referenced.

A typical four-receiver setup has one PDU for all receivers, thus one
entry which is then referenced for all receivers from rec_cfg.

##### pdu_conn

Positive number

Cannot be changed while referenced.

##### pdu_model/pdu_protocol

Supported combinations:

<!-- Table header divider: 4, 4 characters: 50%, 50% column widths -->

| pdu_model | pdu_protocol |
|:--- |:--- |
| netio     | json         |
| netio     | snmpv3       |
| apc-nmc   | ssh          |

NETIO JSON protocol uses SSL. Enable "Turn on secure connection (HTTPS)"
on the PDU. Certificate validation errors are ignored by the control
service when communicating with the PDU since NETIO PDUs only allow the
creation of self-signed certificates valid for one year.

NETIO JSON and APC-NMC SSH protocols set delay time to 5 seconds when
performing a power cycle. NETIO SNMPv3 requires the delay to be set via
the web interface.

##### pdu_host

Fully qualified domain name (FQDN), hostname, or IP address of the PDU.

##### pdu_user

PDU API user name.

##### pdu_password

PDU API password.

## Receiver configuration

View: rec_cfg

Deletions/changes to rec_id propagate to chan_denylist, chan_schedule.

##### rec_id

Receiver ID: 1 ≤ N ≤ 16

Changing rec_id is allowed with restrictions to ensure running
operations are not disrupted: proc_chan_chg, verify_chan, vid_analyze,
remote_ctrl: disabled, Time elapsed since last update: half a minute,
Time elapsed since last receiver reset: 5 minutes.

Adding an entry starts a control service instance for it
(cr-control<span>@</span>N).

##### descr

Description. Shortened to 20 alphanumerical characters in email alerts.

NULL field to remove.

##### channel

EPG channel present in view "chan_catalog".

Changing channel in view "channel" for configured receivers has an
identical effect.

Updates without changes do not perform a channel change.

##### proc_chan_chg

Enable/disable channel processing.

No channel changes are permitted when disabled; the scheduler does not
process existing entries. It keeps the most recent past entry (which it
applies once processing is again enabled). Manual channel resets are
permitted.

Analyzer resets receiver back to specified channel on signal loss.
Disabled thus effectively locks the receiver to the specified channel.

Changing from disabled to enabled performs a channel change.

When adding a receiver with proc_chan_chg disabled, the system stores
the channel and does not perform an initial channel change.

ctrl_conn must be defined to enable this setting.

##### rec_type

Receiver type: sky+hd or skyq.

Choose sky+hd for SkyHD DRX595 receivers.

##### ctrl_conn

Fully qualified domain name (FQDN), hostname, or IPv4 address of the
receiver.

Changes automatically restart cr-discover<span>@</span>N.service if
verify_chan feature is enabled.

NULL field to remove.

##### verify_chan

Enable/disable verification feature.

* Automatically transmit PIN for PIN-locked programs.

* Protect against inadvertent channel changes:

  + SkyHD/Sky+HD: Record internal service ID after channel change. Check
    if service ID matches. Enabling this feature triggers an initial
    channel set.

  + Sky Q: Map service ID to channel and check match.

Requests channel set on mismatch/not set. Service ID is incorrect during
an ERROR_SIGNAL state. It is deleted on a channel change in such a state
and not checked by the verification feature. Once the signal recovers, a
channel set is triggered on SkyHD/Sky+HD.

Logs device/model information.

Logs warning about a pending update on Sky Q.

Verification runs in cr-analyze<span>@</span>N.service, before A/V
checks.

Checks are skipped if the vessel is in a blockage zone (Coverage
feature) or the alarm state file exists (Alarm feature).
cr-analyze<span>@</span>N.service does not run if the vessel is
out-of-footprint. (Coverage feature).

Checks are temporarily skipped if requested by Operator-level user
(Function "analyze_suspend").

Check frequency: 4 times per minute. Same rate-limiting as for
vid_analyze applies.

ctrl_conn must be defined to enable this setting.

Service ID/channel check can be disabled if only PIN entry is desired.
Refer to the installation guide.

Runs cr-discover<span>@</span>N.service.

Runs cr-analyze<span>@</span>N.timer.

##### vid_src_conn

Video source configuration for analysis.

UDP multicast example:

```
{
    "type": "network",
    "udp": {
        "host": "233.252.9.11",
        "port": "1234",
        "localaddr": "10.10.150.30"
    }
}
```

localaddr may be omitted, provided the system has correct multicast
routes set on the interface connected to the IPTV VLAN.

<div class="pagebreak"></div>

Local capture example:

```
{
    "type": "local",
    "alsa": {
        "device": "hw:CARD=hdmi1",
        "channels": "2",
        "sample_rate": "48000"
    },
    "v4l2": {
        "device": "/dev/hdmi1",
        "framerate": "10",
        "video_size": "vga",
        "input_format": "mjpeg"
    }
}
```

Alsa or v4l2 section is required, allowing video/audio-only monitoring.

Local capture supports Video4Linux/alsa capture cards. The settings in
the example were tested with four Macrosilicon USB 2.0 capture cards.
Higher resolutions exceed bus bandwidth.

With these types of cards, udev rules are required to get persistent
names — refer to the installation guide.

For video_size, framerate properties, refer to FFmpeg documentation:
<a href="https://ffmpeg.org/ffmpeg-utils.html#Video-size" target="_blank"
rel="noreferrer noopener">https://ffmpeg.org/ffmpeg-utils.html#Video-size</a>.

To show formats supported by a capture device, run:

```
v4l2-ctl --list-formats-ext -Dd /dev/hdmi1
ffprobe -f v4l2 -list_formats all $(readlink -f /dev/hdmi1)
```

Changes automatically restart cr-capture<span>@</span>N.service if
vid_analyze feature is enabled.

##### vid_analyze

Enable/disable video analysis.

A channel reset is requested if silence is detected or the video is
frozen for more than 25 seconds. The thresholds were chosen for sports
programming and may need to be increased to avoid false positives for
movie channels — refer to the installation guide.

Capture not running is treated as input loss (except for local capture
device not present error), a channel reset is requested to wake the
receiver.

Picture-in-picture TV is cropped before detection on Sky Q. On
SkyHD/Sky+HD this feature is disabled by default since "Mini TV Mode"
can be disabled in settings. Refer to the installation guide, technical
reference/application services/capture for configuration.

A rate-limiting algorithm reduces the action frequency if a signal issue
on the target channel persists. The limiter can be manually reset by
executing the function "analyze_reset_rate_limit".

<!-- Table header divider: 3, 7 characters: 30%, 70% column widths -->

| Issue duration | Check frequency |
|:-- |:------ |
| <5 minutes | 4 times per minute       |
| ≥5 minutes | Approx. every minute     |
| ≥1 hour    | Approx. every 2 minutes  |
| ≥2 hours   | Approx. every 3 minutes  |
| ≥3 hours   | Approx. every 5 minutes  |
| ≥12 hours  | Approx. every 10 minutes |
| ≥1 day     | Approx. every 20 minutes |

Checks are skipped if the vessel is in a blockage zone (Coverage
feature) or the alarm state file exists (Alarm feature).
cr-analyze<span>@</span>N.service does not run if the vessel is
out-of-footprint.

Checks are temporarily skipped if requested by Operator-level user
(Function "analyze_suspend").

ctrl_conn, vid_src_conn must be defined to enable this setting.

Runs cr-capture<span>@</span>N.service.

Runs cr-analyze<span>@</span>N.timer.

##### no_feed_act

Execute a custom script if the network source stream is offline. All
receivers connected to a multi-input encoder need to have the same
feed_act_group specified. A locking algorithm ensures that only one
instance interacts with the encoder. Initial and re-execution delays
(from view "feed_act") are observed before executing the external script
`/opt/celestial_reign/bin/feed-act.sh`. feed_act_group is passed as
argument 1, the string "feed" as argument 2.

feed_act_group must be defined to enable this setting; vid_src_conn must
be of type network.

##### res_act

Execute a custom script if the network source stream is in standard
definition (horizontal resolution of 720 pixels). All receivers
connected to a multi-input encoder need to have the same feed_act_group
specified. A locking algorithm ensures that only one instance interacts
with the encoder. Initial and re-execution delays (from view "feed_act")
are observed before executing the external script
`/opt/celestial_reign/bin/feed-act.sh`. feed_act_group is passed as
argument 1, the string "res" as argument 2.

feed_act_group must be defined to enable this setting; vid_src_conn must
be of type network.

##### feed_act_group

feed_act_group present in view "feed_act".

Defines initial and re-execution delays for no_feed_act/res_act.
Changing the group releases possible lock and clears delays. Execute the
function "analyze_reset_feed_act_delay" to manually reset active delays.

NULL field to remove.

##### backup_chan

Valid EPG channel not present in view "chan_catalog".

Channel used to briefly switch to when performing a channel reset to
recover signal — recommended: 501.

A backup-channel cannot be available as an assignable channel. This
limitation exists since recovering a stuck receiver requires a brief
switch to a different channel. Attempting to specify a channel present
in view "chan_catalog" is rejected.

##### pin

PIN code transmitted with verify_chan enabled on channel change/analysis
when encountering a PIN-locked program.

NULL field to remove.

> PIN entry has not been tested yet on Sky Q. Feedback is welcome.

##### remote_ctrl

Enable/disable remote control feature available for Operator-level
users.

ctrl_conn must be defined to enable this setting.

##### pdu_ctrl

Enable/disable receiver reset feature available for Operator-level
users.

pdu_conn, pdu_outlet must be defined to enable this setting.

##### pdu_conn

pdu_conn present in view "pdu_conn".

NULL field to remove.

##### pdu_outlet

Non-negative number. PDU outlet receiver is connected to.

NULL field to remove.

##### alert_net

Enable/disable network alert.

Requires alert to be enabled in view "glb_rec_cfg".

Notification state is reset on next run after disabling feature.

##### alert_av

Enable/disable A/V alert.

Requires alert to be enabled in view "glb_rec_cfg".

Notification state is reset on next run after disabling feature.

## Channel denylist

View: chan_denylist

Block channels on a per-receiver basis.

##### chan_denylist_id

Auto-generated ID, omit when adding/editing entries.

##### rec_id

Receiver present in view "rec_cfg".

##### deny_channel

EPG channel present in view "chan_catalog".

## User management

Functions: user_create, user_delete, user_list, user_reset.

User name requirements: Lower case alphanumeric letters, hyphens; 3-32
characters.

Password complexity enforced by passwordcheck module.

Permission levels: admin, operator, user.

Only application users created through the UI can be deleted/reset.

Functions remain hidden without Admin-level permissions.

## Configuration reset

Function: config_reset

Erases all settings and applies default values for feed_act and
glb_rec_cfg.

Tick the checkbox _catalog_def_chan to add default channels to
chan_catalog.

Enter the phrase "Do it" (without quotes) into the field _action_confirm
to proceed.

Function remains hidden without Admin-level permissions.

# Utilities

## Mapping

Alarm and capture services can record the vessel's current position to
improve blockage definition files when Antenna control unit signal loss
(Alarm service) or Audio/video loss (Capture service) occurs. Refer to
the installation guide, technical reference/application services for
configuration.

The included utility `coord2kmz.sh` can convert these records to Keyhole
Markup Language format for visualization on maps. Events contain the
time and are named L for loss, R for recover, C for clear, followed by A
or V plus the receiver ID (Capture log only). "Clear" is the removal of
a loss state upon service restart.

Capture service does not record coordinates if the vessel is
out-of-footprint.

To convert the current capture log, execute:

```
cd /tmp
coord2kmz.sh /opt/celestial_reign/log/detect-coord.log
```

The destination file is written to the current directory. The alarm log
is named `detect-coord-alarm.log`.

Retrieve the file from the remote system and copy it to the current
directory on the local machine:

```
scp <USER>@<HOST>:/tmp/*.kmz .
```

Replace \<USER> with the user used to log into the server, \<HOST> with
the server's FQDN/IP.

The system rotates logs when reaching 100MiB. The first rotated log
bears the suffix ".1". Subsequent files are archived (four are kept). To
extract an archived file, run `gzip -kd <File>`.

# Troubleshooting

Application log files are accessible by clicking "Log" on the web
management landing page. Select the file to view in the top-left
drop-down menu. Files are grouped into current and past, where past
files are from the most recent log rotation, which happens daily except
for detect-coord\* logs.

N for files listed with the suffix <span>@</span>N below matches rec_id
for every configured receiver.

Not all files are present, depending on the features configured. No file
does not necessarily mean a service is not running. For instance, the
discovery service only outputs upon start and when messages are
changing. Preview only outputs warnings — scheduler errors and when it
is applying changes.

<!-- Table header divider: 3, 7 characters: 30%, 70% column widths -->

| Name | Description |
|:-- |:------ |
| events                     | Summary of all receiver-related events |
| cr-alarm-gpio              | Alarm contact monitoring               |
| cr-alarm-lmxp-net          | Sea tel LMXP telnet monitoring         |
| cr-alarm-lmxp-tty          | Sea tel LMXP serial monitoring         |
| cr-alert                   | Email alerting                         |
| cr-analyze<span>@</span>N  | Analysis/verification                  |
| cr-capture<span>@</span>N  | Capture/detection for analysis         |
| cr-comp-mgmt               | Component manager                      |
| cr-control<span>@</span>N  | Receiver control                       |
| cr-coverage                | Geofencing evaluation                  |
| cr-latlon                  | Location update                        |
| cr-discover<span>@</span>N | Discovery for verification             |
| cr-dvb-refclock            | DVB-S time source for NTP              |
| cr-preview                 | Preview rendering                      |
| cr-scheduler               | Scheduled channel changes              |
| detect-coord               | Coordinates of video loss events       |
| detect-coord-alarm         | Coordinates of alarm events            |
| detect-events              | Detection events                       |

<div class="pagebreak"></div>

# Receiver setup

Make sure the following settings are configured on each receiver to
ensure correct operation.

See appendix for remote control codes to enter the respective menu.

## Sky Q ES130

Hidden installer menu

* LNB type: SCR

* Enable Pushed on Demand: Off

* Disable both 2.4 and 5 GHz Wi-Fi

  Power cycle receiver to apply LNB change

Audio visual menu

* Resolution: 1080p

Preferences menu

* Standby: None

Parental menu

* Family Setting: Off

  Some programs may still require PIN entry

## Sky+HD DRX890/895

Sky+Setup menu

* Disk Space Management: Automatic

* Instant rewind: Off

* Standby Mode: None

Customize menu

* Mini TV Mode: Off

* On-Screen Icon Timeout: On

* Search and Scan Banner Timeout: 5 sec

* Highlight Programs Originated in HD: Off

* Enable Pushed on Demand: Off

* On Demand Download Notification: Off

* Back up to TV Guide: Off

Picture menu

* HD Resolution Output: 720p

Family menu

* Family Setting: Off

  Some programs may still require PIN entry

## SkyHD DRX595

Sky+Setup menu

* Standby Mode: None

Customize menu

* Mini TV Mode: Off

* On-Screen Icon Timeout: On

* Search and Scan Banner Timeout: 5 sec

* Highlight Programs Originated in HD: Off

* Back up to TV Guide: Off

Picture menu

* HD Resolution Output: 720p

Family menu

* Family Setting: Off

  Some programs may still require PIN entry

<div class="pagebreak"></div>

# Appendix

## Remote control snippets

These snippets leave any menu screen the receiver might be in and
navigate to the listed section:

##### System details

(Viewing card number)

* Sky+HD DRX890/895

  sky services right right right select right right

* SkyHD DRX595

  sky services right select right right

* Sky Q ES130

  home down down down down down down down down down down down select
  down

##### Standby

Sky+Setup

* Sky+HD DRX890/895

  sky services right right select select

* SkyHD DRX595

  sky services select right select

Preferences

* Sky Q ES130

  home down down down down down down down down down down down select
  down down down down select down down down down down select

##### Customize

(Mini-TV mode; OSD/banner timeouts; Back up to TV guide)

* Sky+HD DRX890/895

  sky services right right select right right right right right select

* SkyHD DRX595

  sky services select right right right right right select

##### PIN protection

Family

* Sky+HD DRX890/895

  sky services down select

* SkyHD DRX595

  sky services left select

Parental

* Sky Q ES130

  home down down down down down down down down down down down select
  down down select select

##### Resolution

Picture

* Sky+HD DRX890/895

  sky services right right right select down down down down down

* SkyHD DRX595

  sky services right select down down down down down

Audio visual

* Sky Q ES130

  home down down down down down down down down down down down select
  down down down down select down down down select

##### Network

* Sky+HD DRX890/895

  sky services right right right select right right right right select

* SkyHD DRX595

  sky services right select right right right right select

* Sky Q ES130

  home down down down down down down down down down down down select
  down down down down select select down select

##### Software update

* Sky Q ES130

  home down down down down down down down down down down down select
  down select down select right select select

##### Hidden installer menu

(Wideband/SCR mode; disable Wi-Fi; factory reset)

* Sky Q ES130

  home down down down down down down down down down down down 0 0 1
  select

(Analog RF out; factory reset)

* SkyHD DRX595, Sky+HD DRX890/895

  sky services 0 0 1 select

##### Close menu

* SkyHD DRX595, Sky+HD DRX890/895

  sky

* Sky Q ES130

  home dismiss
