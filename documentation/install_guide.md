# Features

Remote management and monitoring solution for Sky UK receivers.

The system is designed for River vessels and addresses A/V loss caused
by signal interruptions.

Features are explained in detail in the user's guide.

* Scheduled and immediate channel changes
* Global list of assignable channels
* Per-receiver channel denylist
* Lock receiver to a single channel
* Automatically enter PIN for PIN-locked programs
* Ensure the correct channel is tuned
* Automatic channel reset after A/V loss, standby, software update, with
  rate-limiting if the issue persists. Skip based on geofencing rules or
  antenna control unit's signal state
* Trigger script on no stream or stream in standard definition for
  network sources using separate initial and re-execution delays for
  both conditions
* Manually issued power cycle with timed channel reset using a
  compatible Power Distribution Unit
* Email alerts on A/V and network connectivity loss
* Remote control commands
* Preview images updated twice per minute
* User, operator, and admin permission levels

User interaction happens through the database management web application
"Adminer". The user's guide describes how to operate the software.
Minimal permissions ensure that no harmful actions can be performed.

# Deployment

## Requirements

This document outlines two setup scenarios for managing an IPTV headend
installation of multiple Sky UK receivers.

### Standard setup

The system runs on a virtual machine. A/V monitoring is done by
subscribing to the streams of the UDP multicast encoders feeding the
IPTV system.

Special care is required regarding network setup.

Sky Q requires an internet connection terminating in the United Kingdom.

To prevent accidentally connecting a unit to the internet with
a non-UK public IP, a dedicated VLAN should be created where default
route traffic is routed through a VPN connection with a kill switch.

SkyHD/Sky+HD does not require an internet connection; internet access
should be blocked. Connecting the receivers to the internet is useful
since it allows card re-pairing on the unit without contacting the Sky
vendor. The same caveats as for Sky Q apply.

Managing IP addresses of multiple receivers via DHCP reservations is
recommended since the remote controls cannot be paired to a receiver.
Navigating menus when, e.g., replacing a receiver causes the others to
move around the menus, too.

For SkyHD/Sky+HD setups with blocked internet access, the DHCP offer
needs to include DNS server and default router; otherwise, the receivers
ignore it. Access to the DNS server can be blocked.

The server needs to receive SSDP multicast messages from the devices,
which stay local to the subnet by default. Thus, a simple solution is a
separate VLAN for controlling the receivers, with the server having
interfaces in both receiver and IPTV VLANs.

Two methods are supported to avoid repeated channel resets whenever the
vessel is in temporary blockage, e.g., a lock. Geofencing: Utilizes GPSD
to connect to the Global navigation satellite system (GNSS) location
source, which supports a serial device server in TCP server mode. Alarm
state: Set by included service for monitoring a Sea tel LMXP antenna
control unit (ACU) via telnet when the ACU has no signal lock or by a
custom script. Both can be used at the same time. Geofencing is
recommended for ships leaving the footprint and switching to a different
satellite.

Access to an NTP server is required to synchronize the system clock.

Receivers should be powered from a compatible Power distribution unit.
The system offers a "receiver reset" feature which, when executed, power
cycles the unit, waits for it to power up, and then puts it back on the
correct channel. Since receivers do lock up occasionally, this feature
allows for a remote reset.

Support for the following PDUs is implemented:

<!-- Table header divider: 6, 8, 6 characters: 30%, 40%, 30% column widths -->

| Manufacturer | API | Model tested |
|:----- |:------- |:----- |
| NETIO | JSON/HTTPS, SNMPv3 | PowerPDU 4C |
| APC   | NMC SSH            | AP7921B     |

The system supports sending email alerts. Access to a mail server is
required. Configuration examples include both sending authenticated
emails, as well as unauthenticated for a relay accepting local IPs
sending to company-internal addresses.

4 vCPU cores, 4 GiB memory recommended.

Sky Q devices send BPDU messages — if using
`spanning-tree portfast bpduguard default`
on Cisco switches add `spanning-tree bpdufilter enable`
to the switch ports Sky Q receivers are connected to.

![Standard installation example](drawings/standard_install.svg)

<div class="pagebreak"></div>

### Standalone setup

The system runs on a multi-NIC computer. A/V monitoring is done via
Macrosilicon USB HDMI capture cards, or other Video4Linux/Alsa
compatible capture devices.

A machine with six ethernet NICs is used in the installation guide. It
supports four receivers, uses one port for directly connecting a
management PC, and one uplink port for network access during
installation. A wireless card is recommended to create a Wi-Fi AP for
management access.

For Sky Q support, the uplink port can be connected to a vendor-provided
VPN router and routing enabled in-between receiver and uplink ports to
provide internet access to the receivers without implementing VPN
routing on the ship's infrastructure.

It is otherwise assumed that network access is not permitted for the
standalone setup scenario. You may need to refer to sections from the
standard configuration if that is not the case.

Each of the receiver network ports runs a dedicated DHCP server instance
with a single-IP-sized pool. The lease file is on temporary storage; on
link change, the respective DHCP server instance restarts. This ensures
that the device gets an IP for the new port when moving it from one port
to another.

Two methods are supported to avoid repeated channel resets whenever the
vessel is in temporary blockage, e.g., a lock. Geofencing: Utilizes GPSD
to connect to the Global navigation satellite system (GNSS) location
source, which supports receiving data from an RS422 NMEA 0183 splitter via
adapter on an onboard RS232 port. Alarm state: Set by included services
for monitoring a Sea tel LMXP antenna control unit (ACU) via console
port when the ACU has no signal lock, NC/NO alarm contacts, or by a
custom script. Both can be used at the same time. Geofencing is
recommended for ships leaving the footprint and switching to a different
satellite.

GNSS data can also serve as a time reference. If not available, time
synchronization can be done with a DVB-S2 tuner card as well. The
scheduler requires accurate timekeeping.

A 3865U processor is the minimum requirement for 4 receivers. A
more powerful CPU with 4 cores is recommended.

![Standalone installation example](drawings/standalone_install.svg)

<div class="pagebreak"></div>

### Satellite

SkyHD/Sky+HD works with conventional satellite multiswitches. The only
requirement is that the dish used for Astra 2 (28.2° E) is connected to
System A since the receivers do not support DiSEqC.

Sky Q typically uses Wideband LNBs. The receivers can be set to SCR
mode to integrate them into a conventional installation.

An SCR multiswitch must be connected via trunk lines to a standard
setup, such as Triax TdSCR504 (Art. No. 318182).

![SCR multiswitch wiring](drawings/scr_multiswitch.svg)

<div class="pagebreak"></div>

## Installation

Note: After finishing the installation steps, continue with application
configuration outlined in the user's guide.

Requirements:

Debian GNU/Linux 12. "SSH server" and "Standard system utilities"
selected during installation.

Log into system and type `ip a` to lookup IP address if DHCP was used
during installation.

Connect via ssh: `ssh <USER>@<IP_ADDRESS>`. Replace \<USER> with the user
created during installation.

Become root: `su -` followed by the root user password.

Install sudo and add user to sudo group.

```
apt install sudo
usermod -aG sudo <USER>
```

From now on, become root by typing `sudo -i` followed by the user's
password. Installation steps assume a root shell.

To edit files, use `nano <FILENAME>`.

Code blocks starting with `cat <<"EOF"` are here-documents.
When copy-pasted, text until next EOF is written to the file specified
right of > in the first line.

### Network (standard)

#### Software

```
apt install --no-install-recommends firewalld openresolv
```

openresolv supports keeping DNS information up to date based on
dns-nameservers directives and information received from DHCP servers.
Alternatively edit `/etc/resolv.conf`.

#### Interfaces

Edit network configuration: `/etc/network/interfaces`.

The first interface in the following example is on the IPTV VLAN, second
interface on the Receiver VLAN.

```
auto ens18
allow-hotplug ens18
iface ens18 inet static
  address 10.10.150.30/24
  gateway 10.10.150.1
  dns-nameservers 10.10.100.5 10.10.100.6
  dns-search example.com
  metric 0
  up ip route add 239.0.0.0/8 dev ens18

auto ens19
allow-hotplug ens19
iface ens19 inet static
  address 10.10.200.10/24
  gateway 10.10.200.1
  metric 1
  up ip route del default dev ens19
```

Multicast route 239.0.0.0–239.255.255.255 is added to the IPTV
interface. The interface used for the default route is
non-deterministic. Metric is used to get multiple default routes in the
correct order; the not-needed default route on the receiver interface is
deleted via hook.

Apply configuration for an interface by executing
`systemctl restart ifup@<INTERFACE>`, e.g.,
`systemctl restart ifup@ens18`.
Configuration for all interfaces marked "auto" can be applied with
`ifup -a`.

#### Firewall rules

```
firewall-cmd --permanent --new-service=rabbitmq-mgmt
firewall-cmd --permanent --service=rabbitmq-mgmt \
  --set-description="RabbitMQ HTTP management API"
firewall-cmd --permanent --service=rabbitmq-mgmt --set-short=rabbitmq-mgmt
firewall-cmd --permanent --service=rabbitmq-mgmt --add-port=15672/tcp
firewall-cmd --permanent --zone=public --add-service=http \
  --add-service=https --add-service=rabbitmq-mgmt
firewall-cmd --permanent --zone=public --add-port=1234/udp
firewall-cmd --permanent --zone=public --add-protocol=igmp \
  --add-protocol=icmp
firewall-cmd --permanent --zone=public --add-interface=ens18
firewall-cmd --permanent --new-zone=receiver
firewall-cmd --permanent --zone=receiver --add-service=ssdp
firewall-cmd --permanent --zone=receiver --add-protocol=igmp \
  --add-protocol=icmp
firewall-cmd --permanent --zone=receiver --add-interface=ens19
firewall-cmd --complete-reload
```

Adjust port 1234 depending on IPTV multicast stream setup.

Test SSDP messages once receivers are connected to the network and
required software is installed. You should see "resource available"
messages:

```
gssdp-discover --interface=ens19 --rescan-interval=5
```

### Network (standalone)

This network configuration example uses NIC 1-4 for receivers.
A specially configured single IP pool DHCP server instance runs for each
port which is restarted on link change by networkd-dispatcher to delete
the lease file. This enables devices to reliably get an offer for the
required IP when moved from one port to another.
NIC 5 runs DHCP for connecting a management PC. NIC 6 is used for uplink
during installation. Routing can be enabled between receiver and uplink
ports to provide internet to receivers when a vendor-provided VPN router
is connected.
Two Wi-Fi cards are used in this example, one to provide an access point
for management, the other as uplink.

Ensure that existing configuration created during installation is
removed from `/etc/network/interfaces` before starting.

Replace interface names in the examples. Execute `ip l` to show
interfaces.

Apply configuration for an interface by executing
`systemctl restart ifup@<INTERFACE>`, e.g.,
`systemctl restart ifup@ens18`.
Configuration for all interfaces marked "auto" can be applied with
`ifup -a`.

#### Software

```
apt install --no-install-recommends firewalld openresolv
apt install dnsmasq hostapd iw networkd-dispatcher
```

#### Interfaces

```
cat <<"EOF" >/etc/network/interfaces.d/enp1s0
auto enp1s0
allow-hotplug enp1s0

iface enp1s0 inet static
  address 10.246.147.1/30
EOF
```

```
cat <<"EOF" >/etc/network/interfaces.d/enp2s0
auto enp2s0
allow-hotplug enp2s0

iface enp2s0 inet static
  address 10.246.147.5/30
EOF
```

```
cat <<"EOF" >/etc/network/interfaces.d/enp3s0
auto enp3s0
allow-hotplug enp3s0

iface enp3s0 inet static
  address 10.246.147.9/30
EOF
```

```
cat <<"EOF" >/etc/network/interfaces.d/enp4s0
auto enp4s0
allow-hotplug enp4s0

iface enp4s0 inet static
  address 10.246.147.13/30
EOF
```

```
cat <<"EOF" >/etc/network/interfaces.d/enp5s0
auto enp5s0
allow-hotplug enp5s0

iface enp5s0 inet static
  address 10.246.148.1/24
EOF
```

```
cat <<"EOF" >/etc/network/interfaces.d/enp6s0
auto enp6s0
allow-hotplug enp6s0

iface enp6s0 inet dhcp
  metric 0
EOF
```

```
cat <<"EOF" >/etc/network/interfaces.d/wlx00005e00530a
auto wlx00005e00530a
allow-hotplug wlx00005e00530a

iface wlx00005e00530a inet static
  address 10.246.149.1/24
EOF
```

```
cat <<"EOF" >/etc/network/interfaces.d/wlx00005e0053b0
auto wlx00005e0053b0
allow-hotplug wlx00005e0053b0

iface wlx00005e0053b0 inet dhcp
  metric 1
EOF
```

Configure Wi-Fi networks:
```
wpa_passphrase SSID PASSWORD \
  >>/etc/wpa_supplicant/wpa_supplicant-wlx00005e0053b0.conf
systemctl enable --now wpa_supplicant@wlx00005e0053b0
```

After future configuration changes restart wpa_supplicant instance and
interface.

Apply interfaces configuration:

```
ifup -a
```

#### Access point

```
cat <<"EOF" >/etc/hostapd/wlx00005e00530a.conf
logger_stdout=-1
logger_stdout_level=2

interface=wlx00005e00530a
country_code=AT
hw_mode=g

ieee80211n=1
ht_capab=[SHORT-GI-20]

ssid=cr
channel=11
ignore_broadcast_ssid=0

auth_algs=1
wpa=2
wpa_passphrase=12345678
wpa_key_mgmt=WPA-PSK-SHA256
rsn_pairwise=CCMP

wmm_enabled=1
macaddr_acl=0
EOF
```

```
systemctl enable --now hostapd@wlx00005e00530a
```

<div class="pagebreak"></div>

#### DHCP server (management)

DHCP server configuration for management interfaces (wired and Wi-Fi
access point):

```
cat <<"EOF" >/etc/dnsmasq.d/loc-mgmt.conf
interface=enp5s0
interface=wlx00005e00530a

bind-interfaces
dhcp-authoritative

# Disable DNS server
port=0

dhcp-range=set:nr,10.246.148.10,10.246.148.254,255.255.255.0,12h
dhcp-range=set:nr,10.246.149.10,10.246.149.254,255.255.255.0,12h

# Disable sending out gateway information
dhcp-option=set:nr,option:router

log-facility=-
EOF
```

Disable resolv.conf using dnsmasq as resolver:

```
sed -i.bak '/^#DNSMASQ_EXCEPT/s/^#//' /etc/default/dnsmasq
systemctl restart dnsmasq.service
```

#### DHCP server (receiver)

DHCP server configuration for receiver interfaces:

```
cat <<"EOF" >/opt/celestial_reign/etc/dhcpd.conf
dhcp-authoritative

dhcp-range=10.246.147.2,10.246.147.2,255.255.255.252,2h
dhcp-range=10.246.147.6,10.246.147.6,255.255.255.252,2h
dhcp-range=10.246.147.10,10.246.147.10,255.255.255.252,2h
dhcp-range=10.246.147.12,10.246.147.12,255.255.255.252,2h

dhcp-option=option:dns-server,1.1.1.1
EOF
```

Sky Q receivers do not send a DHCP request after a link down/up event.
Moving the receiver to another port results in no network connectivity
until lease expiration (2 hours) unless a network reset is performed.
SkyHD/Sky+HD receivers immediately get a new IP address when moved to
another port.

Add aliases for application:

```
cat <<"EOF" >>/etc/hosts
10.246.147.2  p1
10.246.147.6  p2
10.246.147.10 p3
10.246.147.14 p4
EOF
```

<div class="pagebreak"></div>

Configure networkd-dispatcher to restart receiver DHCP service instance
on link down:

```
cat <<"EOF" >/etc/networkd-dispatcher/no-carrier.d/50-cr-dhcpd.sh
#!/usr/bin/env bash

set -o errexit
set -o nounset

if [[ "${IFACE}" =~ ^enp1s0|enp2s0|enp3s0|enp4s0$ ]]; then
  systemctl --no-block restart "cr-dhcpd@${IFACE}.service"
fi
EOF
```

Add systemd-networkd dependency to networkd-dispatcher unit file:

```
mkdir -p /etc/systemd/system/networkd-dispatcher.service.d
cat <<"EOF" >/etc/systemd/system/networkd-dispatcher.service.d/override.conf
[Unit]
Requires=systemd-networkd.service
After=systemd-networkd.service
EOF
```

Disable networkd-dispatcher startup triggers:
```
sed -i.bak '/networkd_dispatcher_args/ s/^#*/#/' \
  /etc/default/networkd-dispatcher
systemctl restart networkd-dispatcher
```

Enable receiver DHCP server interface instances:

```
systemctl link /opt/celestial_reign/systemd/cr-dhcpd@.service
systemctl enable --now cr-dhcpd@{enp4s0,enp3s0,enp2s0,enp1s0}
```

#### Firewall rules

```
firewall-cmd --permanent --new-service=rabbitmq-mgmt
firewall-cmd --permanent --service=rabbitmq-mgmt \
  --set-description="RabbitMQ HTTP management API"
firewall-cmd --permanent --service=rabbitmq-mgmt --set-short=rabbitmq-mgmt
firewall-cmd --permanent --service=rabbitmq-mgmt --add-port=15672/tcp
firewall-cmd --permanent --zone=public --add-protocol=igmp \
  --add-protocol=icmp
firewall-cmd --permanent --zone=public --add-service=http \
  --add-service=https --add-service=dhcp --add-service=rabbitmq-mgmt
firewall-cmd --permanent --zone=public --add-interface=enp6s0 \
  --add-interface=enp5s0 --add-interface=wlx00005e0053b0 \
  --add-interface=wlx00005e00530a
firewall-cmd --permanent --new-zone=receiver
firewall-cmd --permanent --zone=receiver --add-service=ssdp \
  --add-service=dhcp
firewall-cmd --permanent --zone=receiver --add-protocol=igmp \
  --add-protocol=icmp
firewall-cmd --permanent --zone=receiver --add-interface=enp1s0 \
  --add-interface=enp2s0 --add-interface=enp3s0 --add-interface=enp4s0
firewall-cmd --complete-reload
```

#### Routing rules

If routing is desired between receiver ports and the uplink port,
execute the following commands:

```
cat <<"EOF" >/etc/sysctl.d/50-forward.conf
net.ipv4.ip_forward = 1
EOF
sysctl -p
```

```
firewall-cmd --permanent --new-policy inet-access
firewall-cmd --permanent --policy inet-access --add-ingress-zone receiver
firewall-cmd --permanent --policy inet-access --add-egress-zone public
firewall-cmd --permanent --policy inet-access --add-masquerade
firewall-cmd --permanent --policy inet-access --set-target=ACCEPT
firewall-cmd --complete-reload
```

### Software

#### Packages

```
apt update
apt install --no-install-recommends curl alsa-utils gupnp-tools \
ca-certificates jq bc amqp-tools gnupg v4l-utils lshw jo ntpsec man-db \
rabbitmq-server gmt dnsutils zip sshpass snmp libemail-valid-perl lbzip2 \
mediainfo npm redis-server postgresql postgresql-plperl-15 sysstat \
gpsd gpsd-tools gpsd-clients expect bash-builtins ffmpeg adminer php-fpm \
php-pgsql nginx libnginx-mod-http-fancyindex libnss3-tools s-nail brotli \
netcat-openbsd locales wget xz-utils dvbtune dosfstools bsdutils findutils \
coreutils grep hostname iproute2 libc-bin openssl rsyslog
```

```
npm install -g sky-remote-cli
```

#### Binaries

```
curl -LJO https://github.com/corvus-ch/rabbitmq-cli-consumer/releases/download/2.3.3/rabbitmq-cli-consumer_2.3.3_linux_amd64.tar.gz
tar xf rabbitmq-cli-consumer_2.3.3_linux_amd64.tar.gz \
  rabbitmq-cli-consumer --one-top-level=/usr/local/bin
```

```
curl -LJO https://github.com/bformet/tailon/releases/download/v1.1.0.2/tailon_1.1.0.2_linux-64.tar.bz2
tar xf tailon_1.1.0.2_linux-64.tar.bz2 tailon \
  --one-top-level=/usr/local/bin
```

```
curl -LJO https://github.com/vi/websocat/releases/download/v1.13.0/websocat.x86_64-unknown-linux-musl
chmod +x websocat.x86_64-unknown-linux-musl
mv websocat.x86_64-unknown-linux-musl /usr/local/bin/websocat
```

```
curl -LJO https://github.com/benibela/xidel/releases/download/Xidel_0.9.8/xidel-0.9.8.linux64.tar.gz
tar xf xidel-0.9.8.linux64.tar.gz xidel --one-top-level=/usr/local/bin
```

#### From source

Several software components need to be compiled from source using
provided dockerfiles. It is recommended to run them on a dedicated
machine, then copy the build artifacts extracted with `podman run` to
the target machine. Docker or Podman (`apt install podman`) is required.

If using Docker, substitute `docker` for `podman`. When using Docker
Desktop for Windows with cmd.exe, substitute `\` with `^` and `$(pwd)`
with `%cd%` ; with PowerShell substitute `\` with `` ` `` and pass the
dockerfile differently:
`Get-Content Dockerfile_... | docker build -t tag:1 -` .

Build machine:

1. `cd build-scripts`

2. Compile software and extract build artifacts:

    ```
    podman build -t pg-amqp:1 -<Dockerfile_pg-amqp-build
    podman run --rm -v "$(pwd):/tmp/cur:Z" pg-amqp:1 \
      sh -c "cp /root/*.deb /tmp/cur/"
    ```

    TSDuck may also be installed from an official binary release
    package; The dockerfile provides an identical build.

    ```
    podman build -t tsduck:1 - <Dockerfile_tsduck-build \
      --build-arg TSDUCK_VER=v3.37-3670
    podman run --rm -v "$(pwd)":/tmp/cur:Z tsduck:1 \
      sh -c "cp /root/*.deb /tmp/cur/"
    ```

    dvbdate_ntp is only needed for DVB-S time synchronization.

    ```
    podman build -t dvbdate-ntp:1 - <Dockerfile_dvbdate-ntp-build
    podman run --rm -v "$(pwd):/tmp/cur:Z" dvbdate-ntp:1 \
      sh -c "cp /root/*.tar.xz /tmp/cur/"
    ```

3. Copy build artifacts to target machine:
   `scp *.deb *.xz <USER>@<HOST>:/home/<USER>`

Target machine:

1. `cd /home/<USER>`

2. Install build artifacts:

    ```
    dpkg -i postgresql-15-pg-amqp_*.deb
    tar xf dvbdate-ntp-*.tar.xz --one-top-level=/usr/local/bin
    dpkg -i tsduck_*.debian12_amd64.deb
    apt install --fix-broken
    ```

#### Application files

```
cd /opt
mkdir celestial_reign
curl -LJO https://gitlab.com/ajkq/celestial_reign/-/archive/v1.0.5/celestial_reign-v1.0.5.tar.gz
tar xf celestial_reign-v1.0.5.tar.gz --directory celestial_reign \
  --strip-components 1 --no-same-permissions
```

### Configuration

The following configuration steps are common to both setups.

#### PostgreSQL

Load required libraries:

```
echo "shared_preload_libraries = 'pg_amqp.so'" \
  >>/etc/postgresql/15/main/postgresql.conf
echo "shared_preload_libraries = '\$libdir/passwordcheck'" \
  >>/etc/postgresql/15/main/postgresql.conf
```

Create mapping/login between database and operating system user:

```
echo "cr celestial_reign cr_scheduler" \
  >>/etc/postgresql/15/main/pg_ident.conf
sed -i.bak -r '/^local +all +all +peer/ s/$/ map=cr/' \
  /etc/postgresql/15/main/pg_hba.conf
```

```
systemctl restart postgresql@15-main
```

#### Redis

Generate and configure password; enable AOF persistence:

```
openssl rand -base64 12 >/opt/celestial_reign/etc/.redis_pass
REDIS_PASS="$(</opt/celestial_reign/etc/.redis_pass)"
sed -i.bak "/^# requirepass/crequirepass ${REDIS_PASS}" \
  /etc/redis/redis.conf
sed -i.bak "/^appendonly/ s/no/yes/" /etc/redis/redis.conf
systemctl restart redis-server
```

#### RabbitMQ

Import configuration, enable management (required by component manager)
and tracing:

```
rabbitmqctl import_definitions /opt/celestial_reign/deploy/rabbit.json
rabbitmq-plugins enable rabbitmq_management
rabbitmq-plugins enable rabbitmq_tracing
```

Change admin password, delete guest user:

```
rabbitmqctl change_password admin
rabbitmqctl delete_user guest
```

Generate and set service password:

```
openssl rand -base64 12 >/opt/celestial_reign/etc/.amqp_pass
AMQP_PASS="$(</opt/celestial_reign/etc/.amqp_pass)"
rabbitmqctl change_password celestial_reign "$AMQP_PASS"
```

#### Application setup


Create application user:

```
adduser --system --group --no-create-home --home /nonexistent \
  celestial_reign
```

Change ownership and permission of password files:

```
chown celestial_reign:celestial_reign /opt/celestial_reign/etc/.*_pass
chmod 400 /opt/celestial_reign/etc/.*_pass
```

Create folders required to be writeable by application services:

```
mkdir -p /opt/celestial_reign/{log,var}
chown celestial_reign:celestial_reign /opt/celestial_reign/{log,var}
```

Link application units:

```
units="/opt/celestial_reign/systemd"
for unit in "${units}"/*.{service,timer,mount}; do
  systemctl link "${unit}"
done
```

Enable component manager service:

```
systemctl enable --now cr-comp-mgmt.service
```

Load redis password when logging in as root user for debugging purposes:

```
echo 'export REDISCLI_AUTH="$(</opt/celestial_reign/etc/.redis_pass)"' \
  >>/root/.bashrc
```

Add application binaries to search path:

```
echo 'PATH="$PATH:/opt/celestial_reign/bin"' \
  >/etc/profile.d/path-opt-celestial_reign-bin.sh
. /etc/profile
```

Enable web-based log viewer:

```
systemctl enable --now cr-log-viewer
```

Set up logging to files:

```
ln -sf /opt/celestial_reign/etc/rsyslog-cr.conf /etc/rsyslog.d/cr.conf
systemctl restart rsyslog
```

Set up log rotation:

```
chown root /opt/celestial_reign/etc/logrotate-cr.conf
systemctl enable --now cr-log-rotate.timer
```

Deploy web server configuration:

```
sed -i.bak 's/TLSv1 TLSv1.1 //' /etc/nginx/nginx.conf
rm /etc/nginx/sites-enabled/default
ln -sf /opt/celestial_reign/etc/nginx-site.conf /etc/nginx/sites-enabled/cr
mkdir -p /var/www/html/cr/doc/
ln -sf /opt/celestial_reign/share/html/doc/*.{html,pdf} \
  /var/www/html/cr/doc/
ln -sf /opt/celestial_reign/share/html/*.{html,css,inc,svg} \
  /var/www/html/cr/
```

#### Database deployment

Deploy database schemas:

```
sudo -u postgres psql -f /opt/celestial_reign/deploy/app.sql \
  -f /opt/celestial_reign/deploy/ui.sql
```

Set password of the application super admin user:

```
sudo -u postgres psql -c "\password sadmin"
```

Configure broker:

```
AMQP_PASS="$(</opt/celestial_reign/etc/.amqp_pass)"
sudo -u postgres psql <<EOF
\connect celestial_reign
INSERT INTO amqp.broker (broker_id, host, port, vhost, username, password)
VALUES('1', 'localhost', 5672, 'celestial_reign', 'celestial_reign',
'$AMQP_PASS')
ON CONFLICT ON CONSTRAINT broker_pkey
DO UPDATE SET password = EXCLUDED.password;
EOF
```

Apply default settings:

```
sudo -u postgres psql -f /opt/celestial_reign/deploy/defaults.sql
```

This triggers events to the component manager via RabbitMQ broker.

Enable scheduler application component:

```
systemctl enable --now cr-scheduler.timer
```

### Configuration (standard)

The following configuration steps are specific to standard setups.

#### Time zone

Set the system time zone used for email alerts:

```
ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime
```

Log messages and preview render times are always in UTC.

If no time zone change automation is employed on vessels operating in
multiple time zones, use Coordinated Universal Time:

```
ln -sf /usr/share/zoneinfo/UTC /etc/localtime
```

#### Web server

Generate self-signed web server certificate valid for 10 years. All
configured FQDNs and IP addresses are added as subject alternative
name:

```
/opt/celestial_reign/bin/gen-cert.sh
```

Configure more efficient brotli compression (vs. gzip) for significantly
faster page load speeds when accessing the system remotely while the
ship is in challenging terrain.

```
apt install libnginx-mod-http-brotli-filter \
  libnginx-mod-http-brotli-static
```

```
cat <<"EOF" >/etc/nginx/conf.d/brotli.conf
brotli on;
brotli_static on;
brotli_types image/svg+xml image/x-icon text/plain text/css text/javascript
  text/xml application/javascript application/json application/xml
  application/xhtml+xml;
EOF
```

Create compressed artifacts for static files. Must be re-run after
updating files under /opt/celestial_reign/share/html/ (linked; served
from /var/www/html/cr/):

```
/opt/celestial_reign/bin/gen-web-compr.sh
```

Reload configuration:

```
systemctl reload nginx.service
```

#### Email

An s-nail account "celestial_reign" needs to be configured for the alert
feature to send emails. Recipients are configured on the
application level.

Unauthenticated example (Server accepting local IPs sending to
company-internal addresses):

```
cat <<"EOF" >>/etc/s-nail.rc
account celestial_reign {
set v15-compat
set mta=smtp://server.example.com:25
set reply-to=itofficer.xx@example.com
set from="XX - Celestial reign <cr.xx@example.com>"
}
EOF
```

If from address is omitted, sender is "celestial_reign@\<FQDN>".

Authenticated example:

```
cat <<"EOF" >>/etc/s-nail.rc
account celestial_reign {
set v15-compat
set mta=smtp://user%40example.com:12345678@server.example.com:587 smtp-use-starttls
set reply-to=itofficer.xx@example.com
set from="XX - Celestial reign <cr.xx@example.com>"
}
EOF
```

If sender address does not accept emails, add a reply-to address.

Test:

```
s-nail -A celestial_reign -s "test" -. recipient@example.com <<<"test"
```

#### Time synchronization

To use on-premise time sources, disable NTP pool servers:
`sed -i.bak '/^pool/ s/^#*/#/' /etc/ntpsec/ntp.conf `.

To use time from NTP servers, add each one on a new line to
`/etc/ntpsec/ntp.conf`.

```
server 192.0.2.2 iburst
```

The default configuration requires minimum 3 servers, comment the line
containing the directive if necessary:
`sed -i.bak '/^tos min/ s/^#*/#/' /etc/ntpsec/ntp.conf`

Restart NTP after changing configuration: `systemctl restart ntpsec.service`.

It may take two minutes to synchronize, indicated by `leap_none`.
Check NTP status: `ntpq -p -c as -c rv`. Check accuracy: `ntpstat`.

Avoid two time sources only as NTP rejects both if the difference
between them is too big. If you must use two, declare one as
truechimer:

```
server 192.0.2.2 iburst true
server 192.0.2.3 iburst
```

#### Geofencing

If geofencing is desired to avoid repeated channel resets whenever the
ship is in a lock, configure GPSD. Blockage zone definitions for locks
on the Danube east of Vilshofen are included.

Edit `/etc/default/gpsd` (Serial device server used in example):

```
# Devices gpsd should collect to at boot time.
# They need to be read/writeable, either by user gpsd or the group dialout.
DEVICES="tcp://<HOST>:<PORT>"

# Other options you want to pass to gpsd
GPSD_OPTIONS="-n"

# Automatically hot add/remove USB GPS devices via gpsdctl
USBAUTO="false"
```

Restart GPSD after changing the configuration:
`systemctl restart gpsd.service`. Check if GNSS data is being received:
`gpspipe -w` and `cgps --unit m --silent`.

#### Antenna control unit (ACU) monitoring

Repeated channel resets whenever the ship is in blockage can be
prevented by monitoring the antenna control unit's signal lock state.

##### Sea tel LMXP ACU (telnet)

A service for monitoring a Sea Tel LMXP ACU via telnet is included. To
utilize it, set the used Telnet port to CLI on the web interface under
Configuration/Interfaces.

At a minimum, the configuration of HOST is required. Other options shown
in the example below are the default:
`systemctl edit cr-alarm-lmxp-net.service`.

```
[Service]
Environment=HOST=192.0.2.43
Environment=USERNAME=User
Environment=PASSWORD=seatel1
Environment=PORT=2003
```

Start the service: `systemctl enable --now cr-alarm-lmxp-net.service`.

See Technical reference/Application services/Alarm for additional
options. The service can log the ship's current coordinates to improve
geofencing blockage definition files whenever a state change occurs.

##### Other ACU/device

When creating a script to monitor an Antenna control unit, create the
file `/opt/celestial_reign/var/alarm` (can be empty) on a no signal
state. Remove the file when the signal recovers.

### Configuration (standalone)

The following configuration steps are specific to standalone setups.

#### Time zone

Set the system time zone used for email alerts.

```
ln -sf /usr/share/zoneinfo/Europe/Vienna /etc/localtime
```

Log messages and preview render times are always in UTC.

For vessels operating in multiple time zones, use Coordinated Universal
Time:

```
ln -sf /usr/share/zoneinfo/UTC /etc/localtime
```

#### Web server

Generate self-signed web server certificate valid for 10 years. All
configured FQDNs and IP addresses are added as subject alternative
name. `INCL_LOCAL=true` adds localhost and loopback IP addresses for
local management access.

```
INCL_LOCAL=true /opt/celestial_reign/bin/gen-cert.sh
systemctl reload nginx.service
```

#### Power management

Safely shut down when the power button is pressed:

```
apt install acpid
```

#### HDMI capture

If Macrosilicon USB HDMI capture devices are used, udev rules are
required to get consistent device names.

The devices do not have a serial number that could be used to identify
them, instead the plugged in USB port must be used.

The example below has four cards connected to a USB hub plugged into
port 1-4 of the system.

Cards are USB 2.0 (even if the label/connector on some says USB 3.0),
thus USB 2.0 bandwith limits apply. It does not matter whether a hub is
used or not, as typically, all ports on the system are connected to the
same controller unless separate PCIe USB controller cards are installed.

4 cards capturing MJPEG 640x480 at 10fps (sufficient for A/V monitoring)
were tested successfully.

Monitor events and plug in one device at a time:

```
udevadm monitor --kernel --subsystem-match=sound \
  --subsystem-match=video4linux
```

Get details to build rules:

```
udevadm info -a -p /devices/pci0000:00/0000:00:14.0/usb1/1-4/1-4.4/1-4.4:1.0/video4linux/video6
udevadm info -a -p /devices/pci0000:00/0000:00:14.0/usb1/1-4/1-4.4/1-4.4:1.2/sound/card3
```

Example rules:

```
cat <<"EOF" >/etc/udev/rules.d/85-usb-hdmi-capture.rules
# Consistent names for multiple Macrosilicon USB HDMI capture devices based on
# plugged in USB port/hub port

SUBSYSTEM!="video4linux|sound", GOTO="usb_hdmi_capture_end"
ACTION!="add|change", GOTO="usb_hdmi_capture_end"

SUBSYSTEM=="video4linux", ATTR{index}=="0", SUBSYSTEMS=="usb", KERNELS=="1-4.4", ATTRS{idVendor}=="534d", ATTRS{idProduct}=="2109", SYMLINK+="hdmi1"
SUBSYSTEM=="sound", SUBSYSTEMS=="usb", KERNELS=="1-4.4", ATTRS{idVendor}=="534d", ATTRS{idProduct}=="2109", ATTR{id}="hdmi1"

SUBSYSTEM=="video4linux", ATTR{index}=="0", SUBSYSTEMS=="usb", KERNELS=="1-4.1", ATTRS{idVendor}=="534d", ATTRS{idProduct}=="2109", SYMLINK+="hdmi2"
SUBSYSTEM=="sound", SUBSYSTEMS=="usb", KERNELS=="1-4.1", ATTRS{idVendor}=="534d", ATTRS{idProduct}=="2109", ATTR{id}="hdmi2"

SUBSYSTEM=="video4linux", ATTR{index}=="0", SUBSYSTEMS=="usb", KERNELS=="1-4.2", ATTRS{idVendor}=="534d", ATTRS{idProduct}=="2109", SYMLINK+="hdmi3"
SUBSYSTEM=="sound", SUBSYSTEMS=="usb", KERNELS=="1-4.2", ATTRS{idVendor}=="534d", ATTRS{idProduct}=="2109", ATTR{id}="hdmi3"

SUBSYSTEM=="video4linux", ATTR{index}=="0", SUBSYSTEMS=="usb", KERNELS=="1-4.3", ATTRS{idVendor}=="534d", ATTRS{idProduct}=="2109", SYMLINK+="hdmi4"
SUBSYSTEM=="sound", SUBSYSTEMS=="usb", KERNELS=="1-4.3", ATTRS{idVendor}=="534d", ATTRS{idProduct}=="2109", ATTR{id}="hdmi4"

LABEL="usb_hdmi_capture_end"
EOF
```

<div class="pagebreak"></div>

Reload and apply rules:
```
udevadm control --reload-rules
udevadm trigger
```

Show supported formats:

```
v4l2-ctl --list-formats-ext -Dd /dev/hdmi1
ffprobe -f v4l2 -list_formats all "$(readlink -f /dev/hdmi1)"
```

Test device and create a short recording:

```
ffmpeg -s 1280x720 -f v4l2 -input_format mjpeg \
-i "$(readlink -f /dev/hdmi1)" -f alsa -ar 48000 \
-ac 2 -i hw:CARD=hdmi1 -c copy -ss 2 -t 5 /tmp/1.mkv -y
```

Retrieve the file from the remote system and copy it to the current
directory on the local machine: `scp <USER>@<HOST>:/tmp/*.mkv .`

Show details about connected devices:

```
lshw -class multimedia -numeric -json |
  jq '.[]|select(.description | contains("Video"))'
```

#### Geofencing

If geofencing is desired to avoid repeated channel resets whenever the
ship is in a lock, configure GPSD. It is also required for time
synchronization, unless DVB-S is utilized for that purpose assuming
network access is not permitted. Blockage zone definitions for locks on
the Danube east of Vilshofen are included.

Edit `/etc/default/gpsd` (second on-board serial port used in example).
`-n` is important when used as time reference source:

```
# Devices gpsd should collect to at boot time.
# They need to be read/writeable, either by user gpsd or the group dialout.
DEVICES="/dev/ttyS1"

# Other options you want to pass to gpsd
GPSD_OPTIONS="-n"

# Automatically hot add/remove USB GPS devices via gpsdctl
USBAUTO="false"
```

If an RS232 port needs to be used to interface with an RS422 NMEA 0183
splitter, an RS485/RS422 port-powered converter is required.

Restart GPSD after changing the configuration:
`systemctl restart gpsd.service`. Check if GNSS data is being received:
`gpspipe -w` and `cgps --unit m`.

#### Time synchronization

To use on-premise time sources, disable NTP pool servers:
`sed -i.bak '/^pool/ s/^#*/#/' /etc/ntpsec/ntp.conf`.

The default configuration requires minimum 3 servers, the directive
needs to be commented:
`sed -i.bak '/^tos min/ s/^#*/#/' /etc/ntpsec/ntp.conf`

Check if time data is coming in for DVB/GNSS sources via shared memory
driver: `ntpshmmon -n 5`.

Restart NTP after changing configuration: `systemctl restart ntpsec.service`.

It may take two minutes to synchronize, indicated by `leap_none`.
Check NTP status: `ntpq -p -c as -c rv`. Check accuracy: `ntpstat`.

##### Navigation satellite

To use the time from the ship's GNSS receiver, be sure that GPSD is
configured and running, then add the following to `/etc/ntpsec/ntp.conf`.

```
server 127.127.28.0 minpoll 4
fudge 127.127.28.0 refid GPS
```

##### Television satellite

To use time from DVB satellite, first enable cr-dvb-refclock.service:

```
systemctl enable --now cr-dvb-refclock.service
```

Default values should be acceptable in most cases. See Technical
reference/Application services/DVB-refclock for options. Next, add the
following to `/etc/ntpsec/ntp.conf`:

```
server 127.127.28.2 minpoll 4
fudge 127.127.28.2 refid DVB flag1 1
```

`flag1 1`: ignore local/remote difference of more than `time2`
defaulting to 4h. Required for vessels leaving/re-entering footprint
with clock gone out-of-sync.

DVB time information suffers from jitter and lags by ~2 seconds. If
using it together with one more clock source, NTP ignores both as it
cannot determine which one is correct. To use it as fallback together
with, e.g., GPS time, define GPS as truechimer. NTP then declares
DVB time as falseticker. Only when GPS time is unavailable, it starts
using DVB time.
```
server 127.127.28.0 minpoll 4 true
fudge 127.127.28.0 refid GPS
server 127.127.28.2 minpoll 4
fudge 127.127.28.2 refid DVB flag1 1
```

#### Antenna control unit (ACU) monitoring

Repeated channel resets whenever the ship is in blockage can be
prevented by monitoring the antenna control unit's signal lock state.

##### Sea tel LMXP ACU (console)

A service for monitoring a Sea Tel LMXP ACU via console port is
included. To use it, set Console to CLI, baud rate to 115200, and enable
flow control on the web interface under Configuration/Interfaces.

Configure the serial device: `systemctl edit cr-alarm-lmxp-tty.service`.
Example:

```
[Service]
Environment=SERIAL_DEVICE=/dev/ttyUSB.LMXP
```

Then, start the service: `systemctl enable --now cr-alarm-lmxp-tty.service`.

See Technical reference/Application services/Alarm for additional
options. The service can log the ship's current coordinates whenever a
state change occurs to improve geofencing blockage definition files.

##### Alarm contacts

Alarm relay outputs can be monitored with an Adafruit FT232H breakout board.

If a Cisco D98xx receiver is used with the same dish, use a D-SUB DB15
(VGA) male breakout adapter and connect to PIN 15 and 10.

The default configuration uses PIN D4 and GND in normally closed mode.

See Technical reference/Application services/Alarm for configuration
options. The service can log the ship’s current coordinates to improve
geofencing blockage definition files whenever a state change occurs.

Add necessary permissions:

```
cat <<"EOF" >/etc/udev/rules.d/11-ftdi.rules
SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6001", GROUP="plugdev", MODE="0660"
SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6011", GROUP="plugdev", MODE="0660"
SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6010", GROUP="plugdev", MODE="0660"
SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6014", GROUP="plugdev", MODE="0660"
SUBSYSTEM=="usb", ATTR{idVendor}=="0403", ATTR{idProduct}=="6015", GROUP="plugdev", MODE="0660"
EOF
```

Reload and apply rules:
```
udevadm control --reload-rules
udevadm trigger
```

Software requirements:

```
apt install python3-pip python3-venv
cd /opt/celestial_reign
python3 -m venv venv
venv/bin/python -m pip install pyftdi adafruit-blinka
```

Start the service: `systemctl enable --now cr-alarm-gpio.service`.

##### Other ACU/device

When creating a script to monitor an Antenna control unit, create the
file `/opt/celestial_reign/var/alarm` (can be empty) on a no signal
state. Remove the file when the signal recovers.

### Miscellaneous

#### DHCP

Information is relevant to network configuration via
`/etc/network/interfaces`.

##### NTP

DHCP-advertised NTP servers automatically override NTP configuration. To
disable, perform the following steps:

```
sed -i.bak -E 's/(IGNORE_DHCP=")(")/\1yes\2/' \
  /etc/default/ntpsec
systemctl restart ntpsec
```

##### Client ID

Older DHCP servers in IPv4-only environments have issues with
DUID client identifiers.

* Send MAC address as hardware-address for a specific interface

  Add `client no` to the interface stanza in `/etc/network/interfaces`

* Send MAC address as client-identifier for all interfaces

  ```
  echo "send dhcp-client-identifier = hardware;" >>/etc/dhcp/dhclient.conf
  ```

##### Hostname

Customize the hostname sent to the DHCP server:

```
echo 'interface "enp6s0" { send host-name ""; }' >>/etc/dhcp/dhclient.conf
```

#### Serial USB adapter

If a serial USB adapter is used, use an udev rule to create a persistent
alias for it as its name may change.

Show device info, then use the information to create the rule:
`udevadm info -a -n /dev/ttyUSB0`.

```
cat <<"EOF" >/etc/udev/rules.d/85-usb-serial.rules
SUBSYSTEM!="tty", GOTO="usb_serial_end"
ACTION!="add|change", GOTO="usb_serial_end"

SUBSYSTEM=="tty", SUBSYSTEMS=="usb", ATTRS{idProduct}=="6001", ATTRS{idVendor}=="0403", ATTRS{serial}=="12345678", SYMLINK+="ttyUSB.GNSS"

LABEL="usb_serial_end"
EOF
```

Reload and apply rules:
```
udevadm control --reload-rules
udevadm trigger
```

#### Graphical user interface

The following steps cover configuring a locked-down graphical
environment with automatic login for local management access on
standalone setups.

Create user:

```
adduser --disabled-password --shell /bin/bash --gecos "Appliance" appliance
```

Install X server, minimal desktop environment without display manager:

```
apt install --no-install-recommends xserver-xorg-core xinit \
  xserver-xorg-input-kbd xserver-xorg-input-mouse \
  xserver-xorg-input-libinput xserver-xorg-video-intel \
  xserver-xorg-video-vesa
```

```
apt install --no-install-recommends libxfce4ui-utils thunar xfce4-panel \
  xfce4-session xfce4-settings xfce4-terminal xfconf xfdesktop4 xfwm4 \
  thunar-volman polkitd gvfs at-spi2-core udisks2 fuse dosfstools \
  exfat-fuse exfatprogs chromium fonts-liberation fonts-dejavu-core \
  p7zip xarchiver mousepad
```

Delete default bookmarks:

```
rm /usr/share/chromium/initial_bookmarks.html
```

Add trust for the self-signed certificate used by the web server to the
appliance user's Network Security Services database:

```
/opt/celestial_reign/bin/add-cert-trust.sh appliance
```

Shortcut:

```
mkdir -p /usr/local/share/applications
```

```
cat <<"EOF" >/usr/local/share/applications/vendor-cr.desktop
[Desktop Entry]
Version=1.0
Type=Link
Name=Celestial reign
Icon=/opt/celestial_reign/share/icon.png
URL=https://localhost/
EOF
```

```
chmod +x /usr/local/share/applications/vendor-cr.desktop
```

Auto-login:

```
mkdir -p /etc/systemd/system/getty@tty1.service.d
cat <<"EOF" >/etc/systemd/system/getty@tty1.service.d/appliance.conf
[Service]
Type=simple
ExecStart=
ExecStart=-/usr/sbin/agetty --autologin appliance --noclear %I $TERM
EOF
```

```
sudo -u appliance mkdir -p /home/appliance/Desktop
```

```
cat <<"EOF" >/etc/profile.d/appliance.sh
[ "$USER" = "appliance" ] && [ -z "$DISPLAY" ] &&
  [ "$(tty)" = "/dev/tty1" ] && {
  cp --preserve=mode \
    /usr/local/share/applications/vendor-cr.desktop Desktop/
  rm -f .bash_history 2>/dev/null
  exec startx -- :1 2>.startx.log
}
EOF
```

<div class="pagebreak"></div>

Hide applications:

```
contents="[Desktop Entry]
Hidden=true"

units=(alevt gupnp-av-cp gupnp-universal-cp thunar-bulk-rename xgpsspeed
  xgps gupnp-network-light termit xfce4-mail-reader xfce-wm-settings
  panel-preferences xfce4-accessibility-settings xfce4-color-settings
  xfce-wmtweaks-settings xfce-workspaces-settings xfce4-settings-editor
  xfce4-session-logout xfce-backdrop-settings xfce-ui-settings
  xfce-session-settings xfce4-mime-settings xfce-settings-manager
  thunar-volman-settings)

root="/home/appliance/.local/share/applications"
sudo -u appliance mkdir -p "${root}"

for unit in "${units[@]}"; do
  echo "${contents}" >"${root}/${unit}.desktop"
done
```

Lock down settings:

```
cd /opt/celestial_reign/etc/xfce/
cp xfce4-desktop.xml /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/
cp xfwm4.xml /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/
```

Choose whether to show power options:


* Show restart/shutdown menu

  ```
  cp xfce4-panel.xml-sysmenu \
    /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-panel.xml
  ```

* Do not show restart/shutdown menu

  ```
  cp xfce4-panel.xml /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/
  ```

```
sed -i.bak -E 's/(<channel name="xsettings" version="1.0")/\1 locked="*" unlocked="%sudo"/' \
  /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml
sed -i.bak -E 's/(<channel name="xfce4-session" version="1.0")/\1 locked="*" unlocked="%sudo"/' \
  /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/xfce4-session.xml
sed -i.bak -E 's/(<channel name="thunar-volman" version="1.0")/\1 locked="*" unlocked="%sudo"/' \
  /etc/xdg/xfce4/xfconf/xfce-perchannel-xml/thunar-volman.xml
```

```
mkdir -p /etc/xdg/xfce4/kiosk/
cat <<"EOF" >/etc/xdg/xfce4/kiosk/kioskrc
[xfce4-session]
CustomizeSplash=%sudo
CustomizeChooser=%sudo
CustomizeLogout=%sudo
CustomizeCompatibility=%sudo
Shutdown=ALL
CustomizeSecurity=%sudo
SaveSession=NONE

[xfdesktop]
UserMenu=%sudo
CustomizeBackdrop=%sudo
CustomizeDesktopMenu=%sudo
CustomizeWindowlist=%sudo
CustomizeDesktopIcons=%sudo
EOF
```

```
mkdir -p /etc/systemd/sleep.conf.d
cat <<"EOF" >/etc/systemd/sleep.conf.d/nosuspend.conf
[Sleep]
AllowSuspend=no
AllowHibernation=no
AllowSuspendThenHibernate=no
AllowHybridSleep=no
EOF
```

```
cat <<"EOF" >/etc/polkit-1/rules.d/90-halt.rules
polkit.addRule(function(action, subject) {
  if (action.id.indexOf("org.freedesktop.login1.halt") == 0 ||
      action.id.indexOf("org.freedesktop.login1.halt-ignore-inhibit") == 0 ||
      action.id.indexOf("org.freedesktop.login1.halt-multiple-sessions") == 0
  ) {
      return polkit.Result.AUTH_ADMIN;
  }
});
EOF
```

If the GUI was configured to show restart/shutdown options, omit the
following policies.

Otherwise you may want to enable them, as the options can still be
accessed by executing `xfce4-session-logout` from Terminal or by right
clicking the panel.

Disabling shutdown via kioskrc does not block terminal commands from
doing so. Restricting it via polkit prevents both.

```
cat <<"EOF" >/etc/polkit-1/rules.d/90-power.rules
polkit.addRule(function(action, subject) {
  if (action.id.indexOf("org.freedesktop.login1.power-off") == 0 ||
      action.id.indexOf("org.freedesktop.login1.power-off-ignore-inhibit") == 0 ||
      action.id.indexOf("org.freedesktop.login1.power-off-multiple-sessions") == 0
  ) {
      return polkit.Result.AUTH_ADMIN;
    }
  })
EOF
```

```
cat <<"EOF" >/etc/polkit-1/rules.d/90-reboot.rules
polkit.addRule(function(action, subject) {
  if (action.id.indexOf("org.freedesktop.login1.reboot") == 0 ||
      action.id.indexOf("org.freedesktop.login1.reboot-ignore-inhibit") == 0 ||
      action.id.indexOf("org.freedesktop.login1.reboot-multiple-sessions") == 0 ||
      action.id.indexOf("org.freedesktop.login1.set-reboot-to-boot-loader-menu") == 0 ||
      action.id.indexOf("org.freedesktop.login1.set-reboot-to-boot-loader-entry") == 0 ||
      action.id.indexOf("org.freedesktop.login1.set-reboot-to-firmware-setup") == 0 ||
      action.id.indexOf("org.freedesktop.login1.set-reboot-parameter") == 0
  ) {
      return polkit.Result.AUTH_ADMIN;
    }
  })
EOF
```

Restart Polkit to apply the rules:

```
systemctl restart polkit.service
```

Start the desktop environment:

```
systemctl daemon-reload
systemctl restart getty@tty1
```

#### Serial console

The following steps cover configuring serial console access on
standalone setups.

Change `ttyS0` and `--unit=0` to match the used serial port.

Edit `/etc/default/grub` and add `console=tty0 console=ttyS0,115200n8` to
`GRUB_CMDLINE_LINUX_DEFAULT=` for serial console access, e.g.:

```
GRUB_CMDLINE_LINUX_DEFAULT="quiet console=tty0 console=ttyS0,115200n8"
```

Add the following lines for the boot loader to be accessible through the
serial console:

```
GRUB_TERMINAL="console serial"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit=0 --word=8 --parity=no --stop=1"
```

Apply configuration changes:

```
update-grub
```

If the BIOS has a serial port console redirection feature, set the
terminal type to VT100 and port settings as above.

Reboot the system to activate the serial console.

Get the resize command from the xterm package without installing GUI
package dependencies:

```
apt download xterm
dpkg --fsys-tarfile xterm_*_amd64.deb | tar xOf - ./usr/bin/resize \
  >/usr/local/bin/resize
chmod +x /usr/local/bin/resize
```

<div class="pagebreak"></div>

Automatically resize the console from the default 80 columns x 24 rows
to match the terminal window size. Avoid running the command multiple
consecutive times (at login) on bash ≥5:

```
cat <<"EOF" >/etc/profile.d/serial_console.sh
if [ "${BASH-}" ] && [ "${BASH}" != "/bin/sh" ] && [ -n "${PS1-}" ] &&
  [ "$(tty)" = "/dev/ttyS0" ]; then

  resize() {
    [ -n "${EPOCHSECONDS-}" ] &&
    [ "${RESIZE_LAST_RUN-}" = "${EPOCHSECONDS-}" ] && return
    command resize >/dev/null
    RESIZE_LAST_RUN="${EPOCHSECONDS-}"
  }
  trap resize DEBUG
fi
EOF
```

If authentication is handled through a console server, you may want to
enable automatic login:

Create user:

```
adduser --disabled-password --shell /bin/bash --gecos "Appliance" appliance
```

Auto-login:

```
mkdir -p /etc/systemd/system/serial-getty@ttyS0.service.d
cat <<"EOF" >/etc/systemd/system/serial-getty@ttyS0.service.d/appliance.conf
[Service]
Type=simple
ExecStart=
ExecStart=-/sbin/agetty --autologin appliance --noclear --keep-baud 115200,57600,38400,9600 %I $TERM
EOF
```

```
systemctl daemon-reload
systemctl restart serial-getty@ttyS0
```

Log files shown through the web interface are stored at
`/opt/celestial_reign/log`. To view, e.g., the event log, run

```
tail -f /opt/celestial_reign/log/events.log
```

Log into the database with an application user, e.g.:

```
psql --host=localhost --dbname=celestial_reign --username=admin
```

Show currently assigned channels:

```
SELECT * FROM ui.channel;
```

Change channel on receiver 1:

```
UPDATE ui.channel SET channel = 401 WHERE rec_id = 1;
```

Perform a channel reset on all receivers:

```
SELECT * FROM ui.rec_reset_channel('*');
```

#### Virtualization

Considerations for selecting a hypervisor for testing with USB video
capture devices passed through from the host:

* ESXi 7: Device is visible in guest but does not work. VMware does not
  support video devices for passthrough (KB 1021345).
* Hyper-V 9.0: Does not support USB passthrough
* XCP-ng 8.2: Works normally, but no snapshots are supported with USB
  devices attached. Devices are passed through in alphabetical order
  (using XCP-ng center).
* Proxmox VE 7, 8: Bus path randomly changes between guest reboots,
  device paths stay the same. Use `ATTRS{devpath}` instead of `KERNELS`
  for udev rules. Ports can be passed through in the correct order with
  devpath matching (usb0 = devpath 1, usb1 = devpath 2, etc.).

Show detected usb devices in the guest OS after boot:
`dmesg | grep --perl-regexp "usb \d+-\d+"`.

#### Automatic updates

Prompt for services to be restarted after a manual upgrade:

```
apt install --no-install-recommends needrestart
```

Automatically install updates:

```
apt install --no-install-recommends unattended-upgrades
```

```
mv /etc/apt/apt.conf.d/50unattended-upgrades \
  /etc/apt/apt.conf.d/50unattended-upgrades.orig
cat <<"EOF" >/etc/apt/apt.conf.d/50unattended-upgrades
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::AutocleanInterval "7";
Unattended-Upgrade::Origins-Pattern {
"origin=Debian,codename=${distro_codename},label=Debian";
"origin=Debian Backports,codename=${distro_codename}-backports,label=Debian Backports";
"origin=Debian,codename=${distro_codename},label=Debian-Security";
"origin=Debian,codename=${distro_codename}-security,label=Debian-Security";
};
Unattended-Upgrade::AutoFixInterruptedDpkg "true";
Unattended-Upgrade::Remove-Unused-Dependencies "true";
Unattended-Upgrade::Remove-Unused-Kernel-Packages "true";
Unattended-Upgrade::InstallOnShutdown "false";
Unattended-Upgrade::Automatic-Reboot "false";
Unattended-Upgrade::MinimalSteps "true";
EOF
```

<div class="pagebreak"></div>

#### System performance

Set the CPU governor to performance:

```
sed -i.bak -E \
  's/(GRUB_CMDLINE_LINUX_DEFAULT)="(.*)"/\1="\2 cpufreq.default_governor=performance"/' \
  /etc/default/grub
update-grub
```

For Intel CPUs, set the Energy performance bias to performance:

```
cat <<"EOF" >/etc/udev/rules.d/40-cpu-epb.rules
KERNEL=="cpu", SUBSYSTEM=="event_source", ACTION=="add", RUN+="/bin/sh -c 'echo 15 | tee /sys/devices/system/cpu/cpu*/power/energy_perf_bias'"
EOF
```

Reboot for both settings to take effect.

<div class="pagebreak"></div>

# Technical reference

## Architecture

PostgreSQL contains configuration and takes care of data validation.

Any change to a field relevant to a worker service synchronizes the
aggregated configuration for the receiver via RabbitMQ (AMQP 0-9-1) to
the component manager, which stores it in Redis. Service changes are
also sent to the component manager, which propagates it to systemd.
Channel changes, receiver resets, and remote control commands are sent
to the receiver's
control service.

Latlon maintains location data; Coverage evaluates it and signals
blockage or out-of-footprint. Location data, blockage, out-of-footprint,
and alarm states are files. Other state data is saved in Redis.

Capture runs A/V loss detection and stream-copies segments for the
preview render service. The discover service learns the API endpoint for
verification.

The analyzer skips if blockage/out-of-footprint/alarm state files are
present. It transmits the PIN code if required, then verifies if the
correct channel is tuned. It checks A/V loss state and sends channel
set/reset actions to the associated control service if necessary.

The alert service reports A/V loss unless any of the aforementioned
states are active. It also warns if a receiver is not responding on the
network.

The scheduler processes the schedule table and updates the receiver
configuration, triggering a sync event to the associated control
service.

PostgreSQL and the analyzer service instances transmit messages to
RabbitMQ; the component manager, and the control service instances
consume messages.

![Architecture](drawings/architecture.svg)

## Application services

Several services allow (advanced) settings to be changed via environment
variables.

Create a systemd drop-in configuration, either for the template unit
affecting all instances, or a specific instance. E.g.:
`systemctl edit cr-capture@.service` or
`systemctl edit cr-capture@1.service`.

If systemctl cannot find the unit, it may be necessary to link it since
disabling a unit also unlinks it. Units managed by the component manager
are linked automatically. To link all application units, execute

```
units="/opt/celestial_reign/systemd"
for unit in "${units}"/*.{service,timer,mount}; do
  systemctl link "${unit}"
done
```

Specify environment variables like in the following example:
```
[Service]
Environment=EXAMPLE1=true
Environment=EXAMPLE2=test
```

Restart non-timer-run services, e.g.,
`systemctl restart cr-capture@1.service`.

Timers have `RandomizedDelaySec` set to 5 (except 10 for Alert) to
smoothen CPU usage.

Services restart automatically, except when a configuration error is
detected or a software dependency is unmet.

Ordering dependencies are defined weakly (Wants=), allowing dependent
services to run on other systems.

To review the output of a unit, use `show-log.sh <UNIT_NAME>` for opening
the journal at the end, `follow-log.sh <UNIT_NAME>` to follow the journal.

### Alarm

The services can record the vessel's current position on signal state
change to improve blockage definition files. The log contains
comma-separated values latitude, longitude, state (0 for loss, 1 for
recover, 2 for clear), event (S), receiver (0), and unix time.
cr-latlon.service must be running, which is started automatically when
the coverage feature is enabled. To start it manually, execute
`systemctl enable --now cr-latlon.service`.
See User's guide for visualization on maps.

#### cr-alarm-lmxp-net.service

Monitor a Sea Tel LMXP Antenna control unit via telnet and signal alarm
state when the satellite dish does not get a locked-in signal. Analysis
and verification are skipped with an active alarm state.

Runs alarm-lmxp-net.exp

Working directory: /opt/celestial_reign

Requires Telnet port in CLI mode (Web interface:
Configuration/Interfaces).

Service removes alarm state on stop. Service triggers
alarm state and restarts when an error occurs (e.g. telnet connection
drops, connection refused), or several consecutive unexpected responses
are received. The state file is present while the antenna has no signal.

Service is unmanaged; to start it, first, configure connection settings,
then execute: `systemctl enable --now cr-alarm-lmxp-net.service`.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| USERNAME   | User                       | ACU username                            |
| PASSWORD   | seatel1                    | ACU password                            |
| HOST       |                            | ACU FQDN/IP                             |
| PORT       | 2003                       | ACU Telnet port                         |
| INTERVAL_S | 5                          | Check frequency in seconds              |
| ALARM_F    | var/alarm                  | Alarm state file                        |
| POS_LOG    | false                      | Record GNSS coordinates on state change |
| POS_LOG_F  | log/detect-coord-alarm.log | File for recording coordinates          |
| POS_F      | tmp/latlon                 | File containing current coordinates     |

#### cr-alarm-lmxp-tty.service

Monitor a Sea Tel LMXP Antenna control unit via console port and signal
alarm state when the satellite dish does not get a locked-in signal.
Analysis and verification are skipped with an active alarm state.

Runs alarm-lmxp-tty.exp

Working directory: /opt/celestial_reign

Requirements (Web interface: Configuration/Interfaces):

* Mode: CLI

* For default configuration:

  + Flow control (RTS/CTS): checked

  + Baud rate: 115200

Define SET_TTY=false if using a PTY or udev rules to configure the
device.

Service removes alarm state on exit. No messages are output in regular
operation, except for alarm state changes. Repeated timeout messages
indicate ACU is rebooting. Timeout messages minutes apart are caused by
a disconnected cable or a Baud rate change on the web interface. An ACU
reboot is needed to recover console functionality in the latter case.
Several consecutive unexpected responses remove alarm state file if
present. The state file is present while the antenna has no signal.

Service is unmanaged; to start it, first, configure connection settings,
then execute: `systemctl enable --now cr-alarm-lmxp-tty.service`.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| SERIAL_DEVICE |                                      | Serial device                           |
| SET_TTY       | true                                 | Configure device                        |
| BAUD_RATE     | 115200                               | Baud rate                               |
| TIMEOUT_S     | 2                                    | Serial read timeout in seconds          |
| INTERVAL_S    | 5                                    | Check frequency in seconds              |
| ALARM_F       | var/alarm                            | Alarm state file                        |
| BASH_LIB      | /usr/lib/bash or /usr/local/lib/bash | bash-builtins module directory          |
| POS_LOG       | false                                | Record GNSS coordinates on state change |
| POS_LOG_F     | log/detect-coord-alarm.log           | File for recording coordinates          |
| POS_F         | tmp/latlon                           | File containing current coordinates     |

#### cr-alarm-gpio.service

Monitor alarm contacts (normally closed or normally open) with an
Adafruit FT232H breakout board and signal alarm state. Connect one alarm
contact to the configured PIN, the other to GND. Analysis and
verification are skipped with alarm state active.

Service removes alarm state on stop.

Runs /opt/celestial_reign/venv/bin/python3 -u bin/alarm-gpio.py

Requires Python ≥3.8.

Working directory: /opt/celestial_reign

Service is unmanaged; to start it, first, configure settings, then
execute: `systemctl enable --now cr-alarm-gpio.service`.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| MODE          | nc                         | nc\|no                                  |
| BOARD_PIN     | D4                         | GPIO input PIN                          |
| INTERVAL_S    | 5                          | Check frequency in seconds              |
| ALARM_F       | var/alarm                  | Alarm state file                        |
| BLINKA_FT232H | 1                          | Board (set from unit file)              |
| POS_LOG       | false                      | Record GNSS coordinates on state change |
| POS_LOG_F     | log/detect-coord-alarm.log | File for recording coordinates          |
| POS_F         | tmp/latlon                 | File containing current coordinates     |

### Alert

cr-alert.service/cr-alert.timer

Check network and A/V activity. Skips A/V email alert if blockage/alarm
states are active, and for configured periods after boot/service
activation.

Runs alert.sh

Working directory: /opt/celestial_reign

Timer is managed by comp-mgmt and automatically enabled when
configured in the database.

s-nail account "celestial_reign" must be configured.

<!-- Table header divider: 4, 6, 10 characters: 20%, 30%, 50% column widths -->

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| REDIS_URL     | Generated from variables below | redis-cli URL                                              |
| REDIS_USER    | default                        | Redis user                                                 |
| REDIS_PASS_F  | etc/.redis_pass                | Redis password file. (6\|4)00 permissions required         |
| REDIS_PASS    | Read from $REDIS_PASS_F        | Redis password                                             |
| REDIS_HOST    | localhost                      | Redis host                                                 |
| REDIS_PORT    | 6379                           | Redis port                                                 |
| REDIS_DB      | 0                              | Redis database                                             |
| NO_COV_F      | var/out_of_coverage            | Out-of-footprint state file                                |
| BLOCKAGE_F    | var/blockage                   | Blockage state file                                        |
| ALARM_F       | var/alarm                      | Alarm state file                                           |
| SVC_ENA_TOL_S | 180                            | A/V check skip period after service activation in seconds |
| UPTIME_TOL_S  | 120                            | A/V check skip period after system boot in seconds        |

### Analyze

cr-analyze<span>@</span>.service/cr-analyze<span>@</span>.timer

Service skips execution if receiver control is busy or the vessel is out
of the footprint. Presence of blockage or alarm state files skips
analysis and verification.

Perform channel reset on A/V loss, channel set on verification
mismatch, enter PIN for locked programs. Execute feed_act/res_act for
network sources.

Runs analyze.sh

Working directory: /opt/celestial_reign

Timer is managed by comp-mgmt and automatically instantiated if
verify_chan or vid_analyze is enabled for the receiver in the
database table rec_cfg.


| Variable | Default value | Description |
|:--- |:----- |:--------- |
| REDIS_URL     | Generated from variables below | redis-cli URL                                                        |
| REDIS_USER    | default                        | Redis user                                                           |
| REDIS_PASS_F  | etc/.redis_pass                | Redis password file. (6\|4)00 permissions required                   |
| REDIS_PASS    | Read from $REDIS_PASS_F        | Redis password                                                       |
| REDIS_HOST    | localhost                      | Redis host                                                           |
| REDIS_PORT    | 6379                           | Redis port                                                           |
| REDIS_DB      | 0                              | Redis database                                                       |
| FEED_ACT_BIN  | bin/feed-act.sh                | Feed/res action script                                               |
| TIMER_FREQ_M  | 4                              | Timer unit frequency per minute to calculate approximate rate limit  |
| FEED_ACT_RE_S | 120                            | Recent feed action flag lifetime                                     |
| ACT_WIN_EX_S  | 120                            | Action window lifetime in seconds                                    |
| BLOCKAGE_F    | var/blockage                   | Blockage state file                                                  |
| CHECK_SID     | true                           | Check service ID/channel with verify_chan feature enabled            |
| SID_CH_MAP    | true                           | Use Service ID-Channel mapping for verify_chan feature on Sky Q      |
| ALARM_F       | var/alarm                      | Alarm state file                                                     |
| SHOW_PIN_ERR  | true                           | Show set PIN response on failure. Requires curl ≥7.76.0              |
| TOTAL_TIMEOUT | 8                              | curl max-time                                                        |
| CONN_TIMEOUT  | 4                              | curl connect-timeout                                                 |
| SVC_ENA_TOL_S | 60                             | Capture not running grace period after service activation in seconds |
| UPTIME_TOL_S  | 120                            | A/V check grace period after system boot in seconds                  |
| CAPT_FAIL_TH  | 8                              | Request channel reset after N failed capture restarts                |
| TZ            | UTC0                           | Time zone. Assign empty value to match system                        |

### Capture

cr-capture<span>@</span>.service

A/V detection and video segment stream copy for preview render
service.

Runs capture.sh

Working directory: /opt/celestial_reign

The service can record the vessel's current position on A/V state change
to improve blockage definition files. The log contains comma-separated
values latitude, longitude, state (0 for loss, 1 for recover, 2 for
clear), event (A/V), receiver id, and unix time. cr-latlon.service must
be running, which is started automatically when the coverage feature is
enabled. To start it manually, execute
`systemctl enable --now cr-latlon.service`.
See User's guide for visualization on maps.

Systemd watchdog restarts service if ffmpeg progress does not update.
Service automatically restarts after 24 hours to prevent long-runtime
issues. Video loss exceeding LOSS_TH_M also triggers a service restart.
Loss state is cleared, then re-detected if it persists.

Timer is managed by comp-mgmt and automatically instantiated if
vid_analyze is enabled for the receiver in the database table
rec_cfg.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| REDIS_URL    | Generated from variables below               | redis-cli URL                                                    |
| REDIS_USER   | default                                      | Redis user                                                       |
| REDIS_PASS_F | etc/.redis_pass                              | Redis password file. (6\|4)00 permissions required               |
| REDIS_PASS   | Read from $REDIS_PASS_F                      | Redis password                                                   |
| REDIS_HOST   | localhost                                    | Redis host                                                       |
| REDIS_PORT   | 6379                                         | Redis port                                                       |
| REDIS_DB     | 0                                            | Redis database                                                   |
| LOGLEVEL     | level+info                                   | ffmpeg loglevel                                                  |
| SEGMENTS     | 10                                           | Keep N segments on disk (network capture); N\*2 (local capture)  |
| NOTIFY_S     | 60                                           | systemd watchdog notification interval                           |
| LOSS_CHK_M   | 15                                           | Check loss duration every N minutes                              |
| LOSS_TH_M    | 240                                          | Exit if loss duration exceeds N minutes                          |
| EVENT_LOG    | true                                         | Log detection events to syslog, identifier cr-detect-events      |
| POS_LOG      | false                                        | Record GNSS coordinates state change to file if in-footprint     |
| POS_LOG_F    | log/detect-coord.log                         | File for recording coordinates                                   |
| POS_F        | tmp/latlon                                   | File containing current coordinates                              |
| NO_COV_F     | var/out_of_coverage                          | Out-of-footprint state file                                      |
| VID_DUR_S    | 25                                           | Report video freeze after ≥N seconds                             |
| AUD_DUR_S    | 25                                           | Report audio silence after ≥N seconds                            |
| VID_THRESH   | -60                                          | freezedetect threshold in dB                                     |
| AUD_THRESH   | -80                                          | silencedetect threshold in dB                                    |
| EXIT_ERR     | true                                         | Stop when receiving corrupt data (ffmpeg -xerror)                |
| AUDIO        | true                                         | Enable/disable audio capture                                     |
| PROG_PORT    | 49200+\<instance>                            | Local port used for progress monitoring                          |
| BASH_LIB     | /usr/lib/bash or /usr/local/lib/bash         | bash-builtins module directory                                   |
| FEED         | true for network, non-rawvideo local capture | HLS/segment stream copy for preview. Must be enabled for res_act |
| CROP_PIP     | false; true for Sky Q                        | Crop video horizontally for detection filter                     |
| CROP_H_POS   | 0; out_w for Sky Q                           | ffmpeg crop filter x parameter value                             |
| CROP_WIDTH   | 65; 75 for Sky Q                             | Crop width in percent                                            |
| ADDL_SED_SCR |                                              | Additional sed script commands to filter log output              |
| UPTIME_RND_S | 120                                          | Up to 10 sec randomized start delay if uptime is \<N seconds     |

<div class="pagebreak"></div>

### Comp-mgmt

cr-comp-mgmt.service

Process service/configuration sync messages from database via RabbitMQ.
Enables+starts and stops+disables services/timers as needed. Syncs
configuration to Redis.

Runs comp-mgmt.sh

Working directory: /opt/celestial_reign

Spawns comp-mgmt-exec.sh for every message received

Service must be enabled/running at all times.

The service runs as root with all available hardening options (systemd
247) applied to the unit file. If the unit should run as regular user
with sudo used for systemctl actions, authorize the application user to
run the scripts used for service management:

```
cat <<"EOF" >/etc/sudoers.d/celestial_reign
celestial_reign ALL=(root) NOPASSWD: /opt/celestial_reign/bin/service_ctl.sh
celestial_reign ALL=(root) NOPASSWD: /opt/celestial_reign/bin/service_inst_ctl.sh
EOF
```

For sudo to work, add `User=celestial_reign`, `ReadWritePaths=/run`,
disable all sandboxing hardening options implicitly enabling
`NoNewPrivileges=` (ref. `man systemd.exec`), and add the following
capabilities CAP_AUDIT_WRITE, CAP_SYS_ADMIN, CAP_SETGID, CAP_SETUID,
CAP_SETPCAP.

AMQP_URL is used by `rabbitmq-cli-consumer` to consume messages.
`rabbitmqadmin` uses RMQ_M_PORT and AMQP_* variables to purge queue(s)
on control service stop events.

Out-of-footprint and blockage state files are deleted on coverage
service stop. The preview image directory is deleted on preview service
stop.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| RMQ_M_PORT   | 15672                          | RabbitMQ management port                              |
| AMQP_URL     | Generated from variables below | rabbitmq-cli-consumer AMQP URL                        |
| AMQP_USER    | celestial_reign                | RabbitMQ user                                         |
| AMQP_PASS_F  | etc/.amqp_pass                 | RabbitMQ password file. (6\|4)00 permissions required |
| AMQP_PASS    | Read from $AMQP_PASS_F         | RabbitMQ password                                     |
| AMQP_HOST    | localhost                      | RabbitMQ host                                         |
| AMQP_PORT    | 5672                           | RabbitMQ AMQP port                                    |
| AMQP_VHOST   | celestial_reign                | RabbitMQ virtual host                                 |
| REDIS_URL    | Generated from variables below | redis-cli URL                                         |
| REDIS_USER   | default                        | Redis user                                            |
| REDIS_PASS_F | etc/.redis_pass                | Redis password file. (6\|4)00 permissions required    |
| REDIS_PASS   | Read from $REDIS_PASS_F        | Redis password                                        |
| REDIS_HOST   | localhost                      | Redis host                                            |
| REDIS_PORT   | 6379                           | Redis port                                            |
| REDIS_DB     | 0                              | Redis database                                        |
| NO_COV_F     | var/out_of_coverage            | Out-of-footprint state file                           |
| BLOCKAGE_F   | var/blockage                   | Blockage state file                                   |
| PREVIEW_D    | tmp/preview                    | Preview image directory                               |
| UNIT_ROOT    | $PWD/systemd                   | Systemd unit file directory                           |
| USE_SUDO     | false                          | Use sudo for actions requiring root privileges        |

### Control

cr-control<span>@</span>.service

Process channel change, channel set, channel reset, receiver reset, remote
control messages via RabbitMQ.

Runs control.sh

Working directory: /opt/celestial_reign

Spawns control-exec.sh for every message received

Service is managed by comp-mgmt and automatically instantiated for each
receiver in the database table rec_cfg.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| AMQP_URL      | Generated from variables below       | rabbitmq-cli-consumer AMQP URL                          |
| AMQP_USER     | celestial_reign                      | RabbitMQ user                                           |
| AMQP_PASS_F   | etc/.amqp_pass                       | RabbitMQ password file. (6\|4)00 permissions required   |
| AMQP_PASS     | Read from $AMQP_PASS_F               | RabbitMQ password                                       |
| AMQP_HOST     | localhost                            | RabbitMQ host                                           |
| AMQP_PORT     | 5672                                 | RabbitMQ AMQP port                                      |
| AMQP_VHOST    | celestial_reign                      | RabbitMQ virtual host                                   |
| REDIS_URL     | Generated from variables below       | redis-cli URL                                           |
| REDIS_USER    | default                              | Redis user                                              |
| REDIS_PASS_F  | etc/.redis_pass                      | Redis password file. (6\|4)00 permissions required      |
| REDIS_PASS    | Read from $REDIS_PASS_F              | Redis password                                          |
| REDIS_HOST    | localhost                            | Redis host                                              |
| REDIS_PORT    | 6379                                 | Redis port                                              |
| REDIS_DB      | 0                                    | Redis database                                          |
| TIMEOUT       | 5                                    | wget/ssh timeout                                        |
| SHOW_PIN_ERR  | true                                 | Show set PIN response on failure. Requires curl ≥7.76.0 |
| TOTAL_TIMEOUT | 8                                    | curl max-time                                           |
| CONN_TIMEOUT  | 4                                    | curl connect-timeout                                    |
| BASH_LIB      | /usr/lib/bash or /usr/local/lib/bash | bash-builtins module directory                          |

### Coverage

cr-coverage.service/cr-coverage.timer

Service skips execution if `$POS_F` is empty (Service truncates stale file).

Evaluates if the vessel is in a blockage zone or out of the footprint.
Cached location data is used to prevent clearing the blockage state on
GNSS loss, e.g., while descending a lock. See user's guide for details
on definition files. State files are present during an active state.

Runs coverage.sh

Working directory: /opt/celestial_reign

Timer is managed by comp-mgmt and automatically enabled when
configured in the database.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| POS_F       | tmp/latlon_last     | File containing cached coordinates                                    |
| NO_COV_F    | var/out_of_coverage | Out-of-footprint state file                                           |
| BLOCKAGE_F  | var/blockage        | Blockage state file                                                   |
| GEO_DEF_D   | etc                 | Definition file directory                                             |
| POS_CACHE_M | 120                 | Maximum age (min) for location data before clearing coord/state files |

### DHCPD

cr-dhcpd<span>@</span>.service

Dnsmasq configured as DHCP server for standalone setup using a separate
instance for each interface. Lease files are stored on tmpfs. On link
state change, networkd-dispatcher restarts the respective service
instance.

Service is unmanaged. Refer to standalone network configuration for usage.

### Discover

cr-discover<span>@</span>.service

SSDP service discovery for learning receiver's SOAP API SkyPlay:2
endpoint which changes across device reboots.

Runs discover.sh

Working directory: /opt/celestial_reign

Systemd watchdog restarts service if no updates are received.

Timer is managed by comp-mgmt and automatically instantiated if
verify_chan is enabled for the receiver in the database table
rec_cfg.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| REDIS_URL     | Generated from variables below | redis-cli URL                                      |
| REDIS_USER    | default                        | Redis user                                         |
| REDIS_PASS_F  | etc/.redis_pass                | Redis password file. (6\|4)00 permissions required |
| REDIS_PASS    | Read from $REDIS_PASS_F        | Redis password                                     |
| REDIS_HOST    | localhost                      | Redis host                                         |
| REDIS_PORT    | 6379                           | Redis port                                         |
| REDIS_DB      | 0                              | Redis database                                     |
| INTERVAL_S    | 60                             | SSDP discovery rescan interval                     |
| EXPIRE_S      | 120                            | Stored SSDP reply lifetime in seconds              |
| IFACE         | auto-detected (routing table)  | SSDP listening interface                           |
| TOTAL_TIMEOUT | 8                              | curl max-time                                      |
| CONN_TIMEOUT  | 4                              | curl connect-timeout                               |

### DVB-refclock

cr-dvb-refclock.service

DVB satellite NTP shared memory clock source.

Runs dvb-refclock.sh

Requires a Video4Linux compatible DVB-S2 tuner card.

Systemd watchdog restarts service if no time information is received,
for instance, after signal loss.

Service is unmanaged; to start it, first, configure settings, then
execute: `systemctl enable --now cr-dvb-refclock.service`.

Check if time data is sent via shared memory driver: `ntpshmmon -n 5`.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| CARD | 0     | DVB adapter        |
| TP   | 11778 | Transponder (MHz)  |
| SR   | 27500 | Symbol rate (kS/s) |
| POL  | V     | Polarization       |
| PORT | 0     | DiSEqC port        |
| UNIT | 2     | SHM unit           |

### Latlon

cr-latlon.service

Maintains current/cached location data.

Runs latlon.sh

Working directory: /opt/celestial_reign/tmp

Requires GPSD to be configured.

Systemd watchdog restarts the service if no time-position-velocity (TPV)
reports are received.

Service is automatically started as a dependency of cr-coverage.service
if the feature is enabled in the database table glb_rec_cfg. Hence it
does not stop automatically after the coverage feature is disabled until
the machine is rebooted or the service is stopped manually
(`systemctl stop cr-latlon.service`).

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| POS_F          | latlon              | Current coordinates file                                                     |
| POS_TMP_F      | .${POS_F}.tmp       | Temporary current coordinates file                                           |
| POS_LAST_F     | ${POS_F}_last       | Cached coordinates file                                                      |
| POS_LAST_TMP_F | .\${POS_LAST_F}.tmp | Temporary cached coordinates file                                            |
| GPSD_HOST      | localhost           | GPSD host                                                                    |
| GPSD_PORT      | 2947                | GPSD port                                                                    |
| GPSD_DEV       |                     | GPSD device (not set: all devices)                                           |
| NO_LOC_TH      | 10                  | Clear current coord. file after N consecutive updates without location data  |

### Log-viewer

cr-log-viewer.service

"Tailon" web log viewer to display application logs. Configured to be
served from reverse proxy.

Service is unmanaged. Must be enabled/running.

### Preview

cr-preview.service/cr-preview.timer

Render preview images from stream segments twice per minute.

Runs preview.sh

Working directory: /opt/celestial_reign

Timer is managed by comp-mgmt and automatically enabled when
configured in the database.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| REDIS_URL    | Generated from variables below | redis-cli URL                                      |
| REDIS_USER   | default                        | Redis user                                         |
| REDIS_PASS_F | etc/.redis_pass                | Redis password file. (6\|4)00 permissions required |
| REDIS_PASS   | Read from $REDIS_PASS_F        | Redis password                                     |
| REDIS_HOST   | localhost                      | Redis host                                         |
| REDIS_PORT   | 6379                           | Redis port                                         |
| REDIS_DB     | 0                              | Redis database                                     |
| LOGLEVEL     | panic                          | ffmpeg loglevel                                    |
| FORMAT       | avif; jpeg if \<4 CPU cores    | Image file format: avif\|webp\|jpeg                |
| QUALITY      | avif: 40; webp: 50; jpeg: 8    | ffmpeg -q:v (webp, jpeg), -crf:v (avif)            |
| LOG_EXEC     | false                          | Print the executed ffmpeg command to stderr        |
| PROCS        | available processing units     | Render images from N receivers in parallel         |
| TZ           | UTC0                           | Time zone. Assign empty value to match system      |

### Scheduler

cr-scheduler.service/cr-scheduler.timer

Process channel schedule.

Runs scheduler.sh

Timer is unmanaged. Must be enabled.

| Variable | Default value | Description |
|:--- |:----- |:--------- |
| PGDATABASE | celestial_reign | PostgreSQL database |
| PGUSER     | cr_scheduler    | PostgreSQL user     |

If running the database on another host, refer to
<a href="https://www.postgresql.org/docs/15/libpq-envars.html" target="_blank"
rel="noreferrer noopener">https://www.postgresql.org/docs/15/libpq-envars.html</a>
for connection variables.

### Log-rotate

cr-log-rotate.service/cr-log-rotate.timer

Rotate application logs daily at 00:00 UTC.

Configuration: /opt/celestial_reign/etc/logrotate-cr.conf

The service runs as root with all available hardening options (systemd
247) applied to the unit file.

Timer is unmanaged. Must be enabled.

### Tmp mount

opt-celestial_reign-tmp.mount

Tmpfs mount: /opt/celestial_reign/tmp.

Size: 250M

Recommended unless the Capture service segment stream-copy function is
disabled and Preview, Latlon services are not enabled. To disable, mask
unit, manually create the folder, and change ownership to the user
"celestial_reign".

<div class="pagebreak"></div>

## System services

### GPSD

gpsd.service

GNSS interface used for providing time to NTP, and vessel position to
application components via cr-latlon.service.

Configuration: /etc/default/gpsd

Unavailability causes Latlon service to clear the current location and
repeatedly restart until service is restored.

### NTP

ntpsec.service

Time synchronization via NTP server, GNSS, or DVB satellite

Configuration: /etc/ntpsec/ntp.conf

### Nginx

nginx.service

Web server and reverse proxy.

Adminer host and database are set in the Nginx configuration file.

Web root is located at /var/www/html/cr/ with files linked from
/opt/celestial_reign/share/html/

Configuration:
/etc/nginx/sites-enabled/cr → /opt/celestial_reign/etc/nginx-site.conf

### Syslog

rsyslog.service

Write application service log messages to files

Configuration:
/etc/rsyslog.d/cr.conf → /opt/celestial_reign/etc/rsyslog-cr.conf

### PostgreSQL

postgresql<span>@</span>15-main.service

SQL database

Configuration:\
/etc/postgresql/15/main/postgresql.conf\
/etc/postgresql/15/main/pg_hba.conf\
/etc/postgresql/15/main/pg_ident.conf

Unavailability has the following impact:

- No user interaction is possible.
- The scheduler cannot run and catches up once availability is restored.

### Redis

redis-server.service

Key/value store

Configuration:
/etc/redis/redis.conf

Unavailability has the following impact:

- Timer-run services Alert, Preview, and Analyze bail out.
- Component manager and receiver control services fail to process
  messages, then repeatedly restart until service is restored; messages are
  re-queued.
- Capture services bail out on start or fail when signaling a state
  change, then repeatedly restart until service is restored.
- Once availability is restored, the analyzer instances may trigger a
  channel reset if running before the associated capture service
  instance has started up again.

### RabbitMQ

rabbitmq-server.service

Message broker

No configuration file

Unavailability has the following impact:

- Database configuration (rec_cfg, glb_rec_cfg, pdu_conn, feed_act) and
  (scheduled) channel changes are rejected.
- Component manager and receiver control services stop, then repeatedly
  restart until service is restored.
- Analyze services fail to request a channel reset.

<div class="pagebreak"></div>

## Changelog

##### v1.0.1

* Minor fixes

* Breaking: Split capture service A/V detection thresholds

  + Edit drop-in configuration files and replace DETECT_DUR_S with
    AUD_DUR_S and VID_DUR_S

* Documentation updates

##### v1.0.2

* Minor improvements

* Purge control queue(s) on receiver deletion/configuration reset

* Documentation updates

##### v1.0.4

* Minor improvements

* Change preview to atomically update images

* Fix rate limiter logic

* Update default channels

##### v1.0.5

* Bug fixes

* Update default channels

* Documentation updates for Debian 12

* Breaking: Changed configuration format of receiver DHCP service
  (standalone setups)

## Upgrade

### → v1.0.5

This version is tested on Debian 12. See below for upgrade steps.

Extract files over existing installation, update database,
regenerate compressed artifacts, and restart application services:

```
cd /opt
curl -LJO https://gitlab.com/ajkq/celestial_reign/-/archive/v1.0.5/celestial_reign-v1.0.5.tar.gz
tar xf celestial_reign-v1.0.5.tar.gz --directory celestial_reign \
  --strip-components 1 --no-same-permissions
ln -sf /opt/celestial_reign/share/html/*.{html,css,inc,svg} \
  /var/www/html/cr/
/opt/celestial_reign/bin/gen-web-compr.sh
sudo -u postgres psql -f /opt/celestial_reign/deploy/upgrade-1.0.5.sql
systemctl daemon-reload
systemctl try-restart 'cr-*' --type=service
```

#### Caveats (standalone)

`cr-dhcp@.service` switches to `dnsmasq` due to upstream EOL of
`isc-dhcp-server`. This requires a new configuration file to be
created. See section Deployment/Installation/Network (standalone)/DHCP
server (receiver).

To keep using the existing configuration, execute the following before
applying the update:

```
cp --remove-destination /opt/celestial_reign/systemd/cr-dhcpd@.service \
  /etc/systemd/system
```

On Debian 12, disable the interfering apparmor profile:

```
ln -s /etc/apparmor.d/usr.sbin.dhcpd /etc/apparmor.d/disable/
apparmor_parser -R /etc/apparmor.d/usr.sbin.dhcpd
```

### Debian 11 → 12

Refer to official upgrade instructions:
<a href="https://www.debian.org/releases/bookworm/amd64/release-notes/ch-upgrading.en.html"
target="_blank" rel="noreferrer noopener">https://www.debian.org/releases/bookworm/amd64/release-notes/ch-upgrading.en.html</a>

Build pg-amqp and tsduck packages as outlined under
Deployment/Installation/Software/From source.

Make sure the system is up-to-date. Ensure the `postgresql` metapackage,
`netcat-openbsd` and `zstd` packages are installed
(`apt install postgresql netcat-openbsd zstd`).

Stop application services:

```
sudo -u postgres psql -d celestial_reign \
  -c "SELECT app.f_manual_sync_stop()"
```

Disallow connections to the database to prevent user/service
interaction:

```
sudo -u postgres psql -c "UPDATE pg_database SET datallowconn = FALSE
WHERE datname = 'celestial_reign'"
```

Disable and remove Nginx brotli modules if installed as they are
available through the repository after the upgrade (standard setups):

```
rm /etc/nginx/modules-available/mod-brotli.conf \
  /etc/nginx/modules-enabled/50-mod-brotli.conf \
  /usr/lib/nginx/modules/ngx_http_brotli_*.so
mv /etc/nginx/conf.d/brotli.conf /etc/nginx/conf.d/brotli.conf.bak
```

Modify package sources:

```
rm /etc/apt/sources.list.d/bullseye-backports.list
sed -i.bak -E -e '/http/ s/bullseye/bookworm/g' \
  -e '/bookworm/ s/(non-free)/\1 non-free-firmware/' \
  /etc/apt/sources.list
```

Upgrade the system. Current configuration files can be kept when
prompted:

```
apt update
apt upgrade --without-new-pkgs
apt full-upgrade
```

<div class="pagebreak"></div>

Re-enable Nginx brotli modules if they were installed before.

```
apt install libnginx-mod-http-brotli-filter \
  libnginx-mod-http-brotli-static
mv /etc/nginx/conf.d/brotli.conf.bak /etc/nginx/conf.d/brotli.conf
systemctl reload nginx
```

Update node packages (sky-control-cli):

```
npm -g update
```

Install the previously built pg-amqp package:

```
dpkg -i postgresql-15-pg-amqp_*.deb
```

Install the required postgresql-plperl-15 package:

```
apt install postgresql-plperl-15
```

Install the previously built TSDuck package for Debian 12:

```
dpkg -i tsduck_*.debian12_amd64.deb
apt install --fix-broken
```

Reboot the system.

Remove the automatically created empty database cluster:

```
pg_dropcluster 15 main --stop
```

Migrate the database:

```
pg_upgradecluster 13 main
```

Re-allow connections to the database:

```
sudo -u postgres psql -c "UPDATE pg_database SET datallowconn = TRUE
WHERE datname = 'celestial_reign'"
```

Drop the public schema:

```
sudo -u postgres psql -d celestial_reign \
  -c "DROP SCHEMA public;"
```

Update the application files as outlined under Technical
reference/Upgrade/→ v1.0.5 since the control and preview services fail
to start without adjusted systemd unit settings.

Start application services:

```
sudo -u postgres psql -d celestial_reign \
  -c "SELECT app.f_manual_sync_start()"
```

Delete the old database cluster:

```
pg_dropcluster 13 main
```

List obsolete packages: `apt list '~o'`

Remove obsolete packages except local, required packages:

```
apt list '~o' | tail -n+2 | cut -f1 -d/ | grep -v -E \
  'tsduck|postgresql-15-pg-amqp' | xargs apt purge -y
```

Remove leftover package configuration files: `apt purge '~c'`

Purge unneeded packages: `apt autopurge`

#### Caveats (standalone)

hostapd may require the channel to be set manually if the service stops
because of a failing automatic channel selection.

Declare systemd-networkd dependency for networkd-dispatcher to ensure
receiver interface DHCP server `cr-dhcpd@` works properly. See section
Deployment/Installation/Network (standalone)/DHCP server (receiver).

If a GUI is installed for local management access, new polkit
configuration files are required to prevent shutdown/reboot by the
appliance user. Remove old configuration files
(`rm /etc/polkit-1/localauthority/50-local.d/*.pkla`).
Uninstall legacy support package polkit-pkla (`apt purge polkit-pkla`)
and create new configuration files as outlined under
Deployment/Installation/Miscellaneous/Graphical user interface.

firewalld forwarding rules need to be recreated if the system is
connected to a vendor-provided VPN router. Delete old direct rules
(`rm /etc/firewalld/direct.xml`), then re-create rules as outlined
under Deployment/Installation/Network (standalone)/Routing rules.

Upgrade virtual environment for cr-alarm-gpio.service:

```
apt install python3-pip python3-venv
cd /opt/celestial_reign
python3 -m venv --upgrade --upgrade-deps venv
venv/bin/python -m pip install --upgrade pyftdi adafruit-blinka
```

<div class="pagebreak"></div>

## Database

### Dump/restore

Dump/restore during development without affecting running worker
services.

Create backup

```
sudo -u postgres pg_dump \
  --table amqp.broker \
  --table app.glb_rec_cfg \
  --table app.feed_act \
  --table app.pdu_conn \
  --table app.chan_catalog \
  --table app.rec_cfg \
  --table app.chan_denylist \
  --table app.chan_schedule \
  --data-only --column-inserts \
  celestial_reign >/tmp/backup.sql
```

Disallow connections, disconnect users:

```
sudo -u postgres psql -c "UPDATE pg_database SET datallowconn = FALSE
WHERE datname = 'celestial_reign'" \
  -c "SELECT pg_terminate_backend(pg_stat_activity.pid)
FROM pg_stat_activity WHERE pg_stat_activity.datname = 'celestial_reign'
AND pid <> pg_backend_pid()"
```

Delete database:

```
sudo -u postgres psql -c "DROP DATABASE celestial_reign"
```

Deploy schemas without applying default settings:

```
cd /opt/celestial_reign/deploy
sudo -u postgres psql -f app.sql -f ui.sql
```

Restore backup without firing any synchronization triggers:

```
sudo -u postgres psql -d celestial_reign \
  -c "SET session_replication_role = replica" -f /tmp/backup.sql
```

### Manual re-sync

If any changes were made in-between creation/restore of a backup, the
configuration of the worker services is out of sync. Manually re-sync
service state, and configuration in Redis.

First stop all services:

```
sudo -u postgres psql -d celestial_reign \
  -c "SELECT app.f_manual_sync_stop()"
```

Wait until the component manager is done, then start all configured
services:

```
sudo -u postgres psql -d celestial_reign \
  -c "SELECT app.f_manual_sync_start()"
```

### Recovery

Recover from a database deletion without backup.

Deploy schemas:

```
cd /opt/celestial_reign/deploy
sudo -u postgres psql -f app.sql -f ui.sql
```

Add broker configuration:

```
AMQP_PASS="$(</opt/celestial_reign/etc/.amqp_pass)"
sudo -u postgres psql <<EOF
\connect celestial_reign
INSERT INTO amqp.broker (broker_id, host, port, vhost, username, password)
VALUES('1', 'localhost', 5672, 'celestial_reign', 'celestial_reign',
'$AMQP_PASS')
ON CONFLICT ON CONSTRAINT broker_pkey
DO UPDATE SET password = EXCLUDED.password;
EOF
```

Stop all services:

```
sudo -u postgres psql -d celestial_reign \
  -c "SELECT app.f_manual_sync_stop()"
```

Wait until the component manager is done.

Apply default configuration:

```
cd /opt/celestial_reign/deploy
sudo -u postgres psql -f defaults.sql
```

Reset password of the application super admin user if required:

```
sudo -u postgres psql -c "\password sadmin"
```

Log in and configure receivers.

### Restore onto configured system

Restore backup from different machine onto an already configured system.

Create backup:

```
sudo -u postgres pg_dump \
  --table amqp.broker \
  --table app.glb_rec_cfg \
  --table app.feed_act \
  --table app.pdu_conn \
  --table app.chan_catalog \
  --table app.rec_cfg \
  --table app.chan_denylist \
  --table app.chan_schedule \
  --data-only --column-inserts \
  celestial_reign >/tmp/backup.sql
```

Backup does not contain users.

<div class="pagebreak"></div>

Clear configuration without applying defaults:

```
sudo -u postgres psql -d celestial_reign \
  -c "SELECT app.f_config_clear()"
```

Wait until component manager is done stopping services.

Restore backup. Synchronization triggers fire and start services. Since
the destination machine has the broker already configured, it is not
restored from the backup.

```
sudo -u postgres psql -d celestial_reign -f /tmp/backup.sql
```

### Application roles

Deployment creates several roles only if they do not already exist.

The following commands delete all roles to start fresh after deleting
the database.

Delete all application users created through the UI schema:

```
sudo -u postgres psql <<"EOF"
DO $$
  DECLARE
    _row record;
  BEGIN
    FOR _row IN
      SELECT m.rolname
      FROM pg_catalog.pg_auth_members a
      INNER JOIN pg_catalog.pg_roles m ON (a.member = m.oid)
      INNER JOIN pg_catalog.pg_roles g ON (a.grantor = g.oid)
      WHERE g.rolname = 'cr_usermgmt'
    LOOP
      RAISE NOTICE 'DROP ROLE %', _row.rolname;
      EXECUTE FORMAT('DROP ROLE %I', _row.rolname);
    END LOOP;
  END;
$$;
EOF
```

Delete all remaining roles created during deployment:

```
sudo -u postgres psql <<"EOF"
DROP ROLE sadmin;
DROP ROLE cr;
DROP ROLE cr_app;
DROP ROLE cr_ui;
DROP ROLE cr_scheduler;
DROP ROLE cr_usermgmt;
DROP ROLE cr_g_users;
DROP ROLE cr_g_admin;
DROP ROLE cr_g_operator;
DROP ROLE cr_g_user;
DROP ROLE cr_g_super;
EOF
```
