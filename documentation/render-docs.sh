#!/usr/bin/env bash

set -o errexit
set -o nounset

function check_deps() {
  local arr cmd
  arr=("${@}")
  for cmd in "${arr[@]}"; do
    if ! type "${cmd}" >/dev/null 2>&1; then
      echo >&2 "${cmd} is missing, not executable, or not defined"
      exit 5
    fi
  done
}

function cleanup() {
  exit_status=$?
  [[ -d "${tmp_dir-}" ]] && rm -rf "${tmp_dir}"
  exit "${exit_status}"
}

trap "exit" INT TERM
trap cleanup EXIT

HTML_TARGET_D="${HTML_TARGET_D:-"../share/html/doc"}"
PDF_TARGET_D="${PDF_TARGET_D:-"../share/html/doc"}"

check_deps cat cp mkdir mktemp pandoc rm sed

if ! tmp_dir="$(mktemp -d)"; then
  print "Failed to create temporary directory"
  exit 1
fi

sed --version 2>/dev/null | grep --quiet --fixed-strings "GNU sed" || {
  echo >&2 "GNU sed required"
  exit 1
}

[[ -d "${HTML_TARGET_D}" ]] || {
  echo >&2 "HTML target directory ${HTML_TARGET_D} does not exist"
  exit 1
}
[[ -d "${PDF_TARGET_D}" ]] || {
  echo >&2 "PDF target directory ${PDF_TARGET_D} does not exist"
  exit 1
}

function usage() {
  cat <<EOF >&2
Render manuals
Usage:  ${0} <format> <guide>
Format: html|pdf
Guide:  user|install
        Omit this option to render all manuals
EOF
}

format="${1-}"
guide="${2-}"

[[ "${format}" =~ ^(html|pdf)$ ]] || {
  usage
  exit 1
}

if [[ -z "${guide}" ]]; then
  files=("user_guide.md" "install_guide.md")
elif [[ "${guide}" =~ ^(user|install)$ ]]; then
  files=("${guide}_guide.md")
else
  usage
  exit 1
fi

header='\\\usepackage[headsepline]{scrlayer-scrpage}\n'
header+='\\clearpairofpagestyles\n'
header+='\\ofoot{\\pagemark}\n'
# shellcheck disable=SC2016
header+='\\ohead{$title$}\\ihead{$subtitle$}\n'
header+='\\addtokomafont{pageheadfoot}{\\upshape}\n'

mkdir -p "${tmp_dir}"

if [[ "${format}" == "pdf" ]]; then

  check_deps cairosvg exiftool pandoc-minted pygmentize lualatex qpdf inkscape

  cp drawings/*.svg "${tmp_dir}/"

  # Convert svg to pdf. cairosvg generated files render at native size,
  # no image size template patching necessary; pdfs also use correct
  # font and do not include T3fonts as with rsvg-convert generated
  # files
  for file in "${tmp_dir}"/*.svg; do
    cairosvg -f pdf "${file}" -o "${file%.*}.pdf"
    # Strip metadata for reproducible builds
    exiftool -q -q -all:all= "${file%.*}.pdf"
    qpdf --deterministic-id --linearize --replace-input "${file%.*}.pdf"
  done

  # Inject header into template since in header-includes subtitle cannot
  # be referenced
  pandoc -D latex | sed "/\\\$for(header-includes)\\$/i ${header}" \
    >"${tmp_dir}/template.latex.tmp"
  # Add page break after TOC
  sed -i.bak "/\\\$body\\$/i \\\\\newpage{}" "${tmp_dir}/template.latex.tmp"

elif [[ "${format}" == "html" ]]; then

  # Strip IE9 shiv from template
  pandoc -D html | sed -e '/<!--\[if lt IE 9\]>/,/<!\[endif\]-->/d' \
    >"${tmp_dir}/template.html.tmp"

fi

# Replace div with class pagebreak with a latex page break, strip for
# html output
filter="
function Div (el)
  if el.classes:includes'pagebreak' then
    if FORMAT:match 'html.*' then
      return {}
    elseif FORMAT:match 'latex' then
      return pandoc.RawBlock('tex', '\\\\newpage{}')
    end
  end
end
"

if [[ "${format}" == "html" ]]; then
  if pandoc --help | grep --fixed-strings --quiet -- --embed-resources; then
    dyn_args=(--embed-resources --standalone)
  else
    # Deprecated as of Pandoc 2.19 (2022-08-03)
    dyn_args=(--self-contained)
  fi
fi

for file in "${files[@]}"; do

  name="${file##*/}"
  name="${name%%.*}"

  if [[ "${format}" == "html" ]]; then

    pandoc --toc --toc-depth=3 "${dyn_args[@]}" --strip-comments \
      -f markdown --wrap=preserve --template="${tmp_dir}/template.html.tmp" \
      --number-sections --lua-filter <(printf "%s" "${filter}") \
      "${file}" "version.html.yaml" metadata/common.html.yaml \
      "metadata/${name}.html.yaml" \
      -o "${HTML_TARGET_D}/${name}.html"

  elif [[ "${format}" == "pdf" ]]; then

    # Direct pdf output does not work because of minted package
    pandoc --top-level-division=section \
      -f markdown --template="${tmp_dir}/template.latex.tmp" \
      --filter pandoc-minted --lua-filter <(printf "%s" "${filter}") \
      "${file}" "version.pdf.yaml" metadata/common.pdf.yaml \
      "metadata/${name}.pdf.yaml" \
      -o "${tmp_dir}/${name}.tex"

    # Patch document to use pdf graphics
    sed -i.bak -E -e "s|drawings(.*)svg|${tmp_dir}\1pdf|" \
      -e "s|(outputdir=)temp|\1${tmp_dir}|" "${tmp_dir}/${name}.tex" \
      -e "s|\\\\\includesvg|\\\\\includegraphics|"

    # Run twice for correct table widths
    for ((i = 0; i < 2; ++i)); do
      lualatex --shell-escape --interaction=batchmode \
        --output-directory="${tmp_dir}" "${tmp_dir}/${name}.tex" || :
    done

    # Create optimized file
    qpdf --deterministic-id --linearize "${tmp_dir}/${name}.pdf" \
      "${PDF_TARGET_D}/${name}.pdf"

  fi

done
