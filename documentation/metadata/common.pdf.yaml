---
lang: en-US
title: Celestial reign
mainfont: DejaVu Sans
monofont: Office Code Pro
monofontoptions: 'Scale=0.95'
documentclass: scrartcl
fontsize: 10pt
classoption: egregdoesnotlikesansseriftitles, twoside, titlepage
numbersections: true
secnumdepth: 3
indent: false
toc: true
toc-depth: 3
geometry: a4paper,margin=2cm
author: |
  Copyright © 2021-2024 Andreas Kohout\
  Licensed under the EUPL-1.2 or later\
  gitlab.com/ajkq/celestial_reign
author-meta: Andreas Kohout
# Make PDFs reproducible; Hyphenate typewriter text only (required for
# breaking lines in minted); Keep items together; Row spacing in tables;
# Do not justify text; Avoid widows/orphans; Do not fill pages
# vertically; Code blocks: frame, avoid page break, break lines;
# Re-declare h4 and h5 to be free-standing, make h4 bold and h5
# bold+italicised since we only number to depth 3; Adjust footer
# position; Redefine spacing for TOC numbers/text; Make subtitle larger
# and not bold, author+date smaller; Change look of quote, remove
# vertical space after; Remove figure caption label
header-includes: |
  \pdfvariable suppressoptionalinfo \numexpr1+32+64+128+512\relax
  \usepackage{etoolbox}
  \usepackage[htt]{hyphenat}
  \usepackage[framemethod=TikZ]{mdframed}
  \usepackage[defaultlines=4,all]{nowidow}
  \usepackage{microtype}
  \usepackage[outputdir=temp]{minted}
  \usepackage{caption}

  \captionsetup[figure]{labelformat=empty}

  \mdfdefinestyle{quote}{%
    linecolor=darkgray,
    rightline=false, topline=false, bottomline=false,
    linewidth=2pt,
    innerleftmargin=0pt
  }
  \setminted{
    breaklines=true,
    breakafter=/,
    autogobble=true,
    frame=single,
    framesep=2mm,
  }
  \setmintedinline{
    breaklines=false
  }

  \BeforeBeginEnvironment{minted}{\begin{minipage}{\linewidth}}
  \AfterEndEnvironment{minted}{\end{minipage}}

  \BeforeBeginEnvironment{item}{\begin{minipage}{\linewidth}}
  \AfterEndEnvironment{item}{\end{minipage}}

  \BeforeBeginEnvironment{longtable}{\renewcommand{\arraystretch}{1.4}}

  \BeforeBeginEnvironment{quote}{\begin{mdframed}[style=quote]}
  \AfterEndEnvironment{quote}{\end{mdframed}\vspace{-\baselineskip}}
  \patchcmd{\quote}{\rightmargin}{\leftmargin 0.75em \rightmargin}{}{}

  \raggedright
  \raggedbottom
  \addtolength{\topskip}{0pt plus 10pt}

  \RedeclareSectionCommand[
    beforeskip=-10pt plus -2pt minus -1pt,
    afterskip=1sp plus -1sp minus 1sp,
    font=\normalfont\bfseries]{paragraph}
  \RedeclareSectionCommand[
    beforeskip=-10pt plus -2pt minus -1pt,
    afterskip=1sp plus -1sp minus 1sp,
    font=\normalfont\bfseries\itshape,
    indent=0pt]{subparagraph}

  \setlength{\footskip}{1.25cm}

  \RedeclareSectionCommand[tocindent=0cm,tocnumwidth=0.9cm]{section}
  \RedeclareSectionCommand[tocindent=0.3cm,tocnumwidth=1.2cm]{subsection}
  \RedeclareSectionCommand[tocindent=0.6cm,tocnumwidth=1.5cm]{subsubsection}

  \setkomafont{subtitle}{\Large}
  \addtokomafont{author}{\large}
  \addtokomafont{date}{\large}
---
