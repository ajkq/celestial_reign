#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

REJECT=3

trap "exit" INT TERM

# Maintain a terse event log (syslog tag cr-events)
function log() {
  ev_log "${inst-}" "${@}"
}

service_ctl_bin="${BASH_SOURCE%/*}/service_ctl.sh"
service_inst_ctl_bin="${BASH_SOURCE%/*}/service_inst_ctl.sh"

check_deps cat jq print rabbitmqadmin rm systemctl timeout xargs \
  "${service_ctl_bin}" "${service_inst_ctl_bin}"

# Functions
check_deps check_bash_ver check_redis ev_log exe format_interval \
  gen_redis_url get_file_passwd verify_vars_set

# mapfile
check_bash_ver 4

del_match="$(<"${BASH_SOURCE%/*}/del_match.lua")"

payload="$(timeout 2s cat /dev/stdin)" || {
  print "stdin data input timed out"
  exit "${FAILURE}"
}

NO_COV_F="${NO_COV_F:-"var/out_of_coverage"}"
BLOCKAGE_F="${BLOCKAGE_F:-"var/blockage"}"
PREVIEW_D="${PREVIEW_D:-"tmp/preview"}"
USE_SUDO="${USE_SUDO:-"false"}"

AMQP_USER="${AMQP_USER:-"celestial_reign"}"
AMQP_PASS_F="${AMQP_PASS_F:-"etc/.amqp_pass"}"
AMQP_PASS="${AMQP_PASS:-$(get_file_passwd "${AMQP_PASS_F}")}"
AMQP_HOST="${AMQP_HOST:-"localhost"}"
RMQ_M_PORT="${RMQ_M_PORT:-"15672"}"
# default if not set, allow variable to be empty
AMQP_VHOST="${AMQP_VHOST-"celestial_reign"}"

REDIS_URL="${REDIS_URL:-"$(gen_redis_url)"}"

check_redis
redis_cmd=(redis-cli -u "${REDIS_URL-}" --no-auth-warning)

sudo_cmd=()
if [[ "${USE_SUDO}" == "true" ]]; then
  check_deps sudo
  sudo_cmd=(sudo -n)
fi

cur_epoch="${EPOCHSECONDS:-"$(date -u '+%s')"}"

# //"" Return empty string instead of false/null
pl_elem='.type//"",.action//"",.instance//"",.data//"",.requestor//""'
mapfile -t pl_val < <(jq -r -c "${pl_elem}" <<<"${payload}")

type="${pl_val[0]-}"
action="${pl_val[1]-}"
inst="${pl_val[2]-}"
data="${pl_val[3]-}"
requestor="${pl_val[4]-}"

# regular units (app.glb_rec_cfg)
if [[ "${type}" == "svc_sync" ]]; then

  # discover is the longest word at 8 characters
  log "Service" "$(printf '%-8s' "${data}")" "${action}"

  print "Event: '${type}'; Action: '${action}'; Component: '${data}'" \
    "Requestor: '${requestor}'"

  "${sudo_cmd[@]}" "${service_ctl_bin}" "${action}" "${data}" || :

  if [[ "${data}" == "coverage" && "${action}" == "stop" ]]; then

    rm -vf "${NO_COV_F}"
    rm -vf "${BLOCKAGE_F}"

  elif [[ "${data}" == "preview" && "${action}" == "stop" &&
    -d "${PREVIEW_D}" ]]; then

    rm -rvf "${PREVIEW_D}"

  elif [[ "${data}" == "alert" && "${action}" == "stop" ]]; then

    print "Clearing notification states"
    "${redis_cmd[@]}" --scan --pattern "alert_notif:i*" |
      xargs -r "${redis_cmd[@]}" del

  fi

# instantiated units (app.rec_cfg)
elif [[ "${type}" == "svc_inst_sync" ]]; then

  log "Service" "$(printf '%-8s' "${data}")" "${action}"

  print "Event: '${type}'; Action: '${action}'; Component: '${data}';" \
    "Instance: '${inst}'; Requestor: '${requestor}'"

  "${sudo_cmd[@]}" "${service_inst_ctl_bin}" "${action}" "${data}" \
    "${inst}" || :

  # Clear/release no feed action state data/lock on service stop
  # Exclude rec_id *, since on truncate cfg_sync clear will wipe redis
  if [[ "${data}" == "capture" && "${action}" == "stop" &&
    "${inst}" != "*" ]]; then

    # keys are created on service shutdown, after configuration for the
    # receiver is cleared
    "${redis_cmd[@]}" DEL "capture_fail_count:i${inst}" >&-
    "${redis_cmd[@]}" DEL "capture_recent_stop:i${inst}" >&-

    group="$("${redis_cmd[@]}" --raw GET "feed_act_group:i${inst}")"
    "${redis_cmd[@]}" DEL "feed_act_group:i${inst}" >&-

    lock_key="feed_act_lock:g${group}"
    # Release lock if owned by instance
    [[ "$("${redis_cmd[@]}" --raw EVAL "${del_match}" 1 "${lock_key}" \
      "${inst}")" == "OK" ]] && {

      print "Releasing feed/res act lock, clearing delay records"
      "${redis_cmd[@]}" --scan --pattern "*:g${group}" |
        xargs -r "${redis_cmd[@]}" del

    }

    [[ -d "${PREVIEW_D}" ]] && rm -vf "${PREVIEW_D}/${inst}".{webp,jpeg}

  elif [[ "${data}" == "verify" && "${action}" == "stop" &&
    "${inst}" != "*" ]]; then

    "${redis_cmd[@]}" DEL "target_sid:i${inst}" >&-

  elif [[ "${data}" == "capture" && "${action}" == "start" ]]; then

    # record the time the service was enabled
    "${redis_cmd[@]}" SET "capture_svc_ena:i${inst}" "${cur_epoch}" >&-

  # clear queued messages on receiver deletion
  elif [[ "${data}" == "control" && "${action}" == "stop" ]]; then

    args=(--host="${AMQP_HOST}" --port="${RMQ_M_PORT}"
      --user="${AMQP_USER}" --password="${AMQP_PASS}")
    [[ -n "${AMQP_VHOST}" ]] && args+=(--vhost="${AMQP_VHOST}")

    if [[ "${inst}" == "*" ]]; then
      read -ra queues < <(rabbitmqadmin --format=bash "${args[@]}" \
        list queues || :)
    else
      queues=("rec_ctrl.${inst}")
    fi

    for queue in "${queues[@]}"; do
      if [[ "${queue}" == "rec_ctrl."* ]]; then
        print "Clearing RabbitMQ queue ${queue}"
        rabbitmqadmin --format=bash "${args[@]}" purge queue \
          name="${queue}" || :
      fi
    done

  fi

elif [[ "${type}" == "svc_misc" ]]; then

  if [[ "${data}" == "analyze" ]]; then

    print "Event: '${type}'; Action: '${action}'; Component: '${data}';" \
      "Instance: '${inst}'; Requestor: '${requestor}'"

    # Set auto-expiring key for temporarily suspending analyze service if
    # user needs to navigate menus with remote control commands
    if [[ "${action}" == "suspend" ]]; then

      cfg_glb="$("${redis_cmd[@]}" --raw GET "cfg:i0")"
      analyze_suspend="$(jq -r -c '.analyze_suspend//""' <<<"${cfg_glb}")"

      verify_vars_set analyze_suspend && {

        "${redis_cmd[@]}" SET "analyze_suspend:i${inst}" 0 \
          EX "${analyze_suspend}" >&-

        interval="$(format_interval "${analyze_suspend}")"
        msg="Analyze suspend (expires in ${interval})"
        log "${msg}"
        print "${msg}"

      }

    elif [[ "${action}" == "continue" ]]; then

      # only log continue event if suspend key was present
      [[ "$("${redis_cmd[@]}" --raw DEL \
        "analyze_suspend:i${inst}")" != "0" ]] && {

        msg="Analyze continue"
        log "${msg}"
        print "${msg}"

      }

    elif [[ "${action}" == "reset_feed_act_delay" ]]; then

      [[ "$("${redis_cmd[@]}" --raw DEL "feed_ini_act:g${inst}")" != "0" ]] &&
        print "Reset group ${inst} feed initial delay"

      [[ "$("${redis_cmd[@]}" --raw DEL "res_ini_act:g${inst}")" != "0" ]] &&
        print "Reset group ${inst} res initial delay"

      [[ "$("${redis_cmd[@]}" --raw DEL "feed_rex_act:g${inst}")" != "0" ]] &&
        print "Reset group ${inst} feed re-exec delay"

      [[ "$("${redis_cmd[@]}" --raw DEL "res_rex_act:g${inst}")" != "0" ]] &&
        print "Reset group ${inst} res re-exec delay"

    elif [[ "${action}" == "reset_rate_limit" ]]; then

      # rate limiter resets on expired action window
      "${redis_cmd[@]}" DEL "act_window:i${inst}" >&-

    else

      print "Invalid action; rejecting message"
      exit "${REJECT}"

    fi

  else

    print "Event: '${type}'; Invalid data '${data}'; rejecting message;" \
      "Requestor: '${requestor}'"
    exit "${REJECT}"

  fi

elif [[ "${type}" == "cfg_sync" ]]; then

  if [[ "${action}" == "write" ]]; then

    key="cfg:i${inst}"
    # Redact password, email, pin for stderr output if present
    jq_cmd='if .pdu_password? then .pdu_password="[REDACTED]" else . end |'
    jq_cmd+='if .alert_email? then .alert_email[]="[REDACTED]" else . end |'
    jq_cmd+='if .pin? then .pin="[REDACTED]" else . end'
    log_data="$(jq -r -c "${jq_cmd}" <<<"${data}" || :)"

    print "Event: '${type}'; Action: '${action}'; Key: '${key}';" \
      "Data: '${log_data}'; Requestor: '${requestor}'"
    "${redis_cmd[@]}" SET "${key}" "${data}"

    # Receiver 0 is used for global config data, do not add to rec_id_list
    [[ "${inst}" -ne 0 ]] && "${redis_cmd[@]}" SADD "rec_id_list" "${inst}" >&-

  elif [[ "${action}" == "remove" ]]; then

    # Receiver 0 is used for global config data, only remove key
    if [[ "${inst}" -eq 0 ]]; then

      key="cfg:i${inst}"
      print "Event: '${type}'; Action: '${action}'; Key: '${key}';" \
        "Requestor: '${requestor}'"
      "${redis_cmd[@]}" DEL "${key}"

    # Remove all keys of a receiver on removal
    else

      pattern="*:i${inst}"
      print "Event: '${type}'; Action: '${action}'; Pattern: '${pattern}';" \
        "Requestor: '${requestor}'"
      "${redis_cmd[@]}" --scan --pattern "${pattern}" |
        xargs -r "${redis_cmd[@]}" del

      "${redis_cmd[@]}" SREM "rec_id_list" "${inst}" >&-

    fi

  # Remove everything on truncation of table app.rec_cfg
  elif [[ "${action}" == "clear" ]]; then

    print "Event: '${type}'; Action: '${action}'; Requestor: '${requestor}'"
    "${redis_cmd[@]}" flushdb

  else

    print "Event: '${type}'; Invalid action '${action}'; rejecting message; " \
      "Requestor: '${requestor}'"
    exit "${REJECT}"

  fi

else

  print "Invalid event type '${type}'; rejecting message;" \
    "Requestor: '${requestor}'"
  exit "${REJECT}"

fi

exit "${SUCCESS}"
