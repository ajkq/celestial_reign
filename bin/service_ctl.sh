#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

check_deps systemctl

# Functions
check_deps exe print

function usage() {
  cat <<EOF >&2
Usage:
${0} <Action> <Component>
Action:     start|stop
Component:  alert|coverage|preview
EOF
}

if ((EUID != 0)); then
  print "Must be run as root"
  exit "${CONFIG}"
fi

action="${1-}"
component="${2-}"

UNIT_ROOT="${UNIT_ROOT:-"${PWD}/systemd"}"

[[ "${action}" =~ ^(start|stop)$ ]] || {
  print "Action validation failed\n"
  usage
  exit "${FAILURE}"
}

[[ "${component}" =~ ^(alert|coverage|preview)$ ]] || {
  print "Component validation failed\n"
  usage
  exit "${FAILURE}"
}

if [[ "${action}" == "stop" ]]; then

  if [[ "${component}" =~ ^(alert|coverage|preview)$ ]]; then

    unit="cr-${component}.timer"
    # disable --now causes unit to remain in failed state, with error
    # Unit vanished (systemd 247)
    exe systemctl stop "${unit}"
    exe systemctl disable "${unit}"

    unit="cr-${component}.service"
    exe systemctl disable "${unit}"

  else

    unit="cr-${component}.service"
    exe systemctl stop "${unit}"
    exe systemctl disable "${unit}"

  fi

elif [[ "${action}" == "start" ]]; then

  [[ "${component}" =~ ^(coverage|alert|preview)$ ]] && {
    unit="${UNIT_ROOT}/cr-${component}.timer"
    exe systemctl link "${unit}"
  }

  # make sure dependency of coverage is linked
  [[ "${component}" == "coverage" ]] && {
    unit="${UNIT_ROOT}/cr-latlon.service"
    exe systemctl link "${unit}"
  }

  unit="${UNIT_ROOT}/cr-${component}.service"
  exe systemctl link "${unit}"

  if [[ "${component}" =~ ^(coverage|alert|preview)$ ]]; then
    unit="cr-${component}.timer"
  else
    unit="cr-${component}.service"
  fi

  exe systemctl enable --now "${unit}"

fi

exit "${SUCCESS}"
