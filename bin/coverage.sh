#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset
shopt -s nullglob

check_deps find gmt mkdir rm touch truncate

# Functions
check_deps print ev_log check_vars_pos_int check_gnu_grep check_bash_ver

check_gnu_grep

POS_F="${POS_F:-"tmp/latlon_last"}"
NO_COV_F="${NO_COV_F:-"var/out_of_coverage"}"
BLOCKAGE_F="${BLOCKAGE_F:-"var/blockage"}"
GEO_DEF_D="${GEO_DEF_D:-"etc"}"
# Maximum age for stale location data before deleting coordinates and
# blockage files
POS_CACHE_M="${POS_CACHE_M:-"120"}"

check_vars_pos_int POS_CACHE_M

# Maintain a terse event log (syslog tag cr-events)
function log() {
  ev_log "" "${@}"
}

[[ -f "${HOME}/.gmt/server/gmt_hash_server.txt" ]] || {
  mkdir -p "${HOME}/.gmt/server/"
  touch "${HOME}/.gmt/server/gmt_hash_server.txt"
}

[[ -s "${POS_F}" ]] || {
  print "Coordinates file not present/empty"
  exit "${FAILURE}"
}

[[ "$(find "${POS_F}" -mmin +"${POS_CACHE_M}")" ]] && {

  print "Clearing stale location data. GNSS input loss" \
    ">${POS_CACHE_M} minutes ago"
  logmsg="Stale location data (>${POS_CACHE_M} min)"
  truncate -s 0 "${POS_F}"

  [[ -f "${BLOCKAGE_F}" ]] && {

    print "Clearing blockage state"
    statemsg="Blockage"
    rm -f "${BLOCKAGE_F}"

  }
  [[ -f "${NO_COV_F}" ]] && {

    print "Clearing out of coverage state"
    [[ -n "${statemsg}" ]] && statemsg+="+"
    statemsg+="Out of coverage"
    rm -f "${NO_COV_F}"

  }
  [[ -n "${statemsg-}" ]] && logmsg+=". Clearing ${statemsg} state"
  log "${logmsg}"

  exit "${SUCCESS}"

}

read -r latlon <"${POS_F}"

# -90 to 90 with optional decimals
regex_lat="(?:-[1-9]|-?[1-8][0-9]|-?90|[0-9])(?:\.[0-9]+)?"
# -180 to 180 with optional decimals
regex_lon="(?:-[1-9]|-?[1-9][0-9]|-?1[0-7][0-9]|-?180|[0-9])(?:\.[0-9]+)?"

grep --quiet --perl-regexp "^${regex_lat},${regex_lon}$" <<<"${latlon}" || {

  print "Malformed input coordinates (Expected: lat,lon)"
  exit "${FAILURE}"

}

lonlat="${latlon#*,},${latlon%,*}"

blockage="false"
coverage="false"

i=0
for cv_poly in "${GEO_DEF_D}"/coverage*.poly; do

  ((++i))
  [[ -n "$(gmt "select" -F"${cv_poly}" -fg <<<"${lonlat}")" ]] && {

    print "Coverage match in '${cv_poly}'"
    coverage="true"
    break

  }

done

# Assume to be in footprint without coverage files
((i == 0)) && coverage="true"

[[ ${coverage} == "true" ]] && for bk_pt in "${GEO_DEF_D}"/blockage*.pt; do

  [[ -n "$(gmt "select" -C"${bk_pt}"+d0 -fg -: <<<"${latlon}")" ]] && {

    print "Blockage match in '${bk_pt}'"
    blockage="true"
    break

  }

done

[[ ${coverage} == "true" && ${blockage} != "true" ]] &&
  for bk_poly in "${GEO_DEF_D}"/blockage*.poly; do

    [[ -n "$(gmt "select" -F"${bk_poly}" -fg <<<"${lonlat}")" ]] && {

      print "Blockage match in '${bk_poly}'"
      blockage="true"
      break

    }

  done

if [[ ${coverage} != "true" ]]; then

  [[ -f "${NO_COV_F}" ]] || {

    print "Setting out of coverage state"
    log "Out of coverage"
    touch "${NO_COV_F}"

  }

else

  [[ -f "${NO_COV_F}" ]] && {

    print "Clearing out of coverage state"
    log "In coverage"
    rm -f "${NO_COV_F}"

  }

fi

if [[ ${blockage} == "true" ]]; then

  [[ -f "${BLOCKAGE_F}" ]] || {

    print "Setting blockage state"
    log "Blockage"
    touch "${BLOCKAGE_F}"

  }

else

  [[ -f "${BLOCKAGE_F}" ]] && {

    print "Clearing blockage state"
    log "Left blockage"
    rm -f "${BLOCKAGE_F}"

  }

fi

exit "${SUCCESS}"
