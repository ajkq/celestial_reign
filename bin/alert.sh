#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

check_deps s-nail jq date

# Functions
check_deps check_bash_ver check_redis ev_log gen_redis_url print \
  sanitize_text verify_vars_set

# mapfile
check_bash_ver 4

NO_COV_F="${NO_COV_F:-"var/out_of_coverage"}"
BLOCKAGE_F="${BLOCKAGE_F:-"var/blockage"}"
ALARM_F="${ALARM_F:-"var/alarm"}"
SVC_ENA_TOL_S="${SVC_ENA_TOL_S:-"180"}"
UPTIME_TOL_S="${UPTIME_TOL_S:-"120"}"

function log() {
  ev_log "${inst-}" "${@}"
}

function email_subject() {
  local subj type site_name inst descr state
  type="${1-}"
  site_name="${2-}"
  inst="${3-}"
  descr="${4-}"
  state="${5-}"

  subj="${type}:"
  [[ -n "${site_name}" ]] && subj+=" ${site_name}:"
  subj+=" Rec. ${inst}"
  [[ -n "${descr}" ]] && subj+=" (${descr}):" || subj+=":"
  subj+=" ${state}"

  echo "${subj}"
}

function email_body() {
  local body site_name type inst descr state date thresh
  site_name="${1-}"
  type="${2-}"
  inst="${3-}"
  descr="${4-}"
  state="${5-}"
  date="${6-}"
  thresh="${7-}"

  body="*** Celestial reign ***\n\n"
  [[ -n "${site_name}" ]] && body+="Site name: ${site_name}\n\n"
  body+="Notification type: ${type}\n"
  body+="Receiver ID: ${inst}\n"
  [[ -n "${descr}" ]] && body+="Rec. description: ${descr}\n\n" || body+="\n"
  body+="State: ${state}\n\n"
  body+="Date/Time: ${date}\n\n"
  [[ "${type,,}" == "alert" ]] &&
    body+="Alert threshold: $(format_interval "${thresh}")\n"

  echo -e "${body}"
}

REDIS_URL="${REDIS_URL:-"$(gen_redis_url)"}"

check_redis
redis_cmd=(redis-cli -u "${REDIS_URL-}" --no-auth-warning)

glb_cfg="$("${redis_cmd[@]}" --raw GET "cfg:i0")"

unset cfg_val
# //"" Return empty string instead of false/null
cfg_elem='.alert_net_thresh//"",.alert_av_thresh//"",.site_name//""'
mapfile -t cfg_val < <(jq -r -c "${cfg_elem}" <<<"${glb_cfg}")
alert_net_thresh="${cfg_val[0]-}"
alert_av_thresh="${cfg_val[1]-}"
site_name="${cfg_val[2]-}"

# sanitize and trim site_name
site_name="$(sanitize_text "${site_name}" 20)"

mapfile -t to_addr < <(jq -r -c ".alert_email[]?" <<<"${glb_cfg}")

cur_epoch="${EPOCHSECONDS:-"$(date -u '+%s')"}"
cur_date="$(date +'%a %b %-d %Y %H:%M %:z')"

read -r -d. uptime </proc/uptime

if ! verify_vars_set to_addr 2>/dev/null; then

  print "alert_email not configured"

else

  for inst in $("${redis_cmd[@]}" --raw SMEMBERS rec_id_list); do

    print "Processing receiver ${inst}"

    cfg="$("${redis_cmd[@]}" --raw GET "cfg:i${inst}")"

    unset cfg_val
    cfg_elem='.ctrl_conn//"",.alert_net//"",.alert_av//"",.vid_analyze//"",'
    cfg_elem+='.descr//""'
    mapfile -t cfg_val < <(jq -r -c "${cfg_elem}" <<<"${cfg}")
    ctrl_conn="${cfg_val[0]-}"
    alert_net="${cfg_val[1]-}"
    alert_av="${cfg_val[2]-}"
    vid_analyze="${cfg_val[3]-}"
    descr="${cfg_val[4]-}"

    descr="$(sanitize_text "${descr}" 20)"

    if verify_vars_set ctrl_conn; then

      if [[ "${alert_net}" == "true" ]] &&
        ping_receiver "${ctrl_conn}" command; then

        # record time of successful network activity check
        "${redis_cmd[@]}" SET "last_net_act_chk:i${inst}" "${cur_epoch}" >&-

      fi

      # get time of last successful network activity check
      last_net_act_chk="$("${redis_cmd[@]}" --raw GET \
        "last_net_act_chk:i${inst}")"
      # default to 0 if key doesn't exist to trigger over threshold
      last_net_act_chk="${last_net_act_chk:-0}"

      # calculate duration in seconds
      last_net_act_chk_dur=$((cur_epoch - last_net_act_chk))

      if [[ "${alert_net}" != "true" ]]; then

        print "Network alert disabled"

        [[ "$("${redis_cmd[@]}" --raw SISMEMBER \
          "alert_notif:i${inst}" net)" == "1" ]] && {
          print "Removed network notified flag"
          "${redis_cmd[@]}" SREM "alert_notif:i${inst}" net >&-
        }

      elif [[ "${alert_net}" == "true" ]] &&
        verify_vars_set alert_net_thresh &&
        ((last_net_act_chk_dur > alert_net_thresh)); then

        print "Last network activity over threshold"

        # Send message once
        if [[ "$("${redis_cmd[@]}" --raw SISMEMBER \
          "alert_notif:i${inst}" net)" == "0" ]]; then

          subj="$(email_subject ALERT "${site_name}" "${inst}" "${descr}" \
            "Network offline")"

          body="$(email_body "${site_name}" ALERT "${inst}" "${descr}" \
            "Network offline" "${cur_date}" "${alert_net_thresh}")"

          # Send email message
          if s-nail -A celestial_reign -s "${subj}" -. "${to_addr[@]}" \
            <<<"${body}"; then

            print "Sending network alert notification to ${#to_addr[@]}" \
              "configured email address(es)"

            log "Notify network alert"

            # Add to set of notified services
            "${redis_cmd[@]}" SADD "alert_notif:i${inst}" net >&-

          fi

        fi

      else

        print "Last network activity within range"

        # Send recovery only if alert was sent
        if [[ "$("${redis_cmd[@]}" --raw SISMEMBER \
          "alert_notif:i${inst}" net)" == "1" ]]; then

          subj="$(email_subject RECOVERY "${site_name}" "${inst}" \
            "${descr}" "Network Ok")"

          body="$(email_body "${site_name}" RECOVERY "${inst}" "${descr}" \
            "Network Ok" "${cur_date}")"

          if s-nail -A celestial_reign -s "${subj}" -. "${to_addr[@]}" \
            <<<"${body}"; then

            print "Sending network recovery notification to ${#to_addr[@]}" \
              "configured email address(es)"

            log "Notify network recovery"

            # Remove from set of notified services
            "${redis_cmd[@]}" SREM "alert_notif:i${inst}" net >&-

          fi

        fi

      fi

    fi

    if [[ "${vid_analyze}" == "true" ]]; then

      # If capture is running, neither loss key and no action window
      # exists, record successful AV activity check
      if [[ "$("${redis_cmd[@]}" --raw EXISTS \
        "video_loss:i${inst}")" == "0" ]] &&
        [[ "$("${redis_cmd[@]}" --raw EXISTS \
          "audio_loss:i${inst}")" == "0" ]] &&
        [[ "$("${redis_cmd[@]}" --raw EXISTS \
          "act_window:i${inst}")" == "0" ]] &&
        [[ "$("${redis_cmd[@]}" --raw EXISTS \
          "capture_src:i${inst}")" == "1" ]]; then

        "${redis_cmd[@]}" SET "last_av_act_chk:i${inst}" "${cur_epoch}" >&-

      fi

      last_av_act_chk="$("${redis_cmd[@]}" --raw GET \
        "last_av_act_chk:i${inst}")"
      last_av_act_chk="${last_av_act_chk:-0}"

      last_av_act_chk_dur=$((cur_epoch - last_av_act_chk))

      svc_ena_time="$("${redis_cmd[@]}" --raw GET \
        "capture_svc_ena:i${inst}")"

      if [[ "${alert_av}" != "true" ]]; then

        print "A/V alert disabled"

        [[ "$("${redis_cmd[@]}" --raw SISMEMBER \
          "alert_notif:i${inst}" av)" == "1" ]] && {

          print "Removed A/V notified flag"
          "${redis_cmd[@]}" SREM "alert_notif:i${inst}" av >&-

        }

      elif [[ "${svc_ena_time}" =~ ^[0-9]+$ ]] &&
        (((cur_epoch - svc_ena_time) < SVC_ENA_TOL_S)); then

        print "Recently enabled capture service, skipping A/V check"

      elif ((uptime < UPTIME_TOL_S)); then

        print "Recently started system, skipping A/V check"

      elif [[ "${alert_av}" == "true" ]] && verify_vars_set alert_av_thresh &&
        ((last_av_act_chk_dur > alert_av_thresh)); then

        print "Last A/V activity over threshold"

        # Alert if not in blockage
        if [[ -f "${NO_COV_F}" || -f "${BLOCKAGE_F}" ]] ||
          [[ -f "${ALARM_F}" ]]; then

          print "Vessel in blockage, skipping alert notification"

        elif [[ "$("${redis_cmd[@]}" --raw SISMEMBER \
          "alert_notif:i${inst}" av)" == "0" ]]; then

          subj="$(email_subject ALERT "${site_name}" "${inst}" "${descr}" \
            "A/V loss")"

          body="$(email_body "${site_name}" ALERT "${inst}" "${descr}" \
            "Audio/Video loss" "${cur_date}" "${alert_av_thresh}")"

          if s-nail -A celestial_reign -s "${subj}" -. "${to_addr[@]}" \
            <<<"${body}"; then

            print "Sending AV alert notification to ${#to_addr[@]}" \
              "configured email address(es)"

            log "Notify A/V alert"

            "${redis_cmd[@]}" SADD "alert_notif:i${inst}" av >&-

          fi
        fi

      else

        print "Last A/V activity within range"

        if [[ "$("${redis_cmd[@]}" --raw SISMEMBER \
          "alert_notif:i${inst}" av)" == "1" ]]; then

          subj="$(email_subject RECOVERY "${site_name}" "${inst}" \
            "${descr}" "A/V Ok")"

          body="$(email_body "${site_name}" RECOVERY "${inst}" "${descr}" \
            "Audio/Video Ok" "${cur_date}")"

          if s-nail -A celestial_reign -s "${subj}" -. "${to_addr[@]}" \
            <<<"${body}"; then

            print "Sending AV recovery notification to ${#to_addr[@]}" \
              "configured email address(es)"

            log "Notify A/V recovery"

            "${redis_cmd[@]}" SREM "alert_notif:i${inst}" av >&-

          fi
        fi

      fi

    else

      print "A/V analysis disabled"

    fi

    print

  done

fi

exit "${SUCCESS}"
