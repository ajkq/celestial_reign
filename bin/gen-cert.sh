#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

KEY_F="${KEY_F:-"/etc/nginx/server.key"}"
CERT_F="${CERT_F:-"/etc/nginx/server.pem"}"
# Exclude standalone setup receiver interface IPs
IP_EXCL="${IP_EXCL-"10.246.147."}"
INCL_LOCAL="${INCL_LOCAL:-"false"}"

trap "exit" INT TERM

check_deps hostname openssl sort uniq grep

# Functions
check_deps check_bash_ver

hostname --version >/dev/null 2>&1 || {
  print "Linux hostname tool required"
  exit "${FAILURE}"
}

# mapfile
check_bash_ver 4

read -r -a ips < <(hostname -I)
read -r -a hostnames < <(hostname -A)
read -r -a fqdn < <(hostname -f)

hostnames+=("${fqdn[@]}" "$(</etc/hostname)")

[[ "${INCL_LOCAL}" == "true" ]] && {
  ips+=(127.0.0.1 ::1)
  hostnames+=(localhost)
}

mapfile -t ips_dedup < <(printf "%s\n" "${ips[@]}" | sort | uniq)
mapfile -t hostnames_dedup < <(printf "%s\n" "${hostnames[@]}" | sort | uniq |
  grep -e "\." -e localhost)

san=""
for entry in "${hostnames_dedup[@]}"; do
  [[ -n "${san}" ]] && san+=","
  san+="DNS:${entry}"
done

for entry in "${ips_dedup[@]}"; do
  [[ "${entry}" == "${IP_EXCL}"* ]] && continue
  [[ -n "${san}" ]] && san+=","
  san+="IP:${entry}"
done

cfg="
[req]
distinguished_name=req
[ext]
basicConstraints=critical,CA:TRUE
subjectKeyIdentifier=hash
authorityKeyIdentifier=keyid:always
keyUsage=critical,digitalSignature,keyEncipherment,keyCertSign
extendedKeyUsage=critical,serverAuth
subjectAltName=${san}
"

openssl req -x509 -newkey rsa:4096 -sha256 -days 3650 -nodes \
  -keyout "${KEY_F}" -out "${CERT_F}" \
  -subj "/O=Celestial reign/CN=${hostnames[0]}" \
  -config <(printf "%s" "${cfg}") -extensions ext
