#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset
set -o pipefail

check_deps curl cut dig getent gssdp-discover head ip jq xidel

# Functions
check_deps check_gnu_grep check_rec_id check_redis check_vars_pos_int \
  check_vars_set gen_redis_url ping_receiver print

check_gnu_grep

INTERVAL_S="${INTERVAL_S:-"60"}"
EXPIRE_S="${EXPIRE_S:-"120"}"
TOTAL_TIMEOUT="${TOTAL_TIMEOUT:-"8"}"
CONN_TIMEOUT="${CONN_TIMEOUT:-"4"}"

check_vars_pos_int INTERVAL_S EXPIRE_S TOTAL_TIMEOUT CONN_TIMEOUT

REDIS_URL="${REDIS_URL:-"$(gen_redis_url)"}"

check_redis
redis_cmd=(redis-cli -u "${REDIS_URL-}" --no-auth-warning)

function cleanup() {
  exit_status=$?
  "${redis_cmd[@]}" DEL "ssdp_url_ctrl:i${inst-}" >&-
  print "Stopped"
  exit "${exit_status}"
}

trap "exit" INT TERM
trap cleanup EXIT

inst="${1-}"

check_rec_id "${inst}"

cfg="$("${redis_cmd[@]}" --raw GET "cfg:i${inst}")"

# //"" Return empty string instead of false/null
ctrl_conn="$(jq -r -c '.ctrl_conn//""' <<<"${cfg}")"

# pick first result. Try next server on failure; filter stdout error messages
ctrl_ip="$(dig -4 +short +fail "${ctrl_conn}" | grep --invert-match ";;" |
  head -n1)"
# try hosts database if dig didn't get a match
ctrl_ip="${ctrl_ip:-"$(getent ahostsv4 "${ctrl_conn}" | head -n1 |
  cut -f1 -d" ")"}"
# fall back to what is probably an IP address if neither methods
# returned anything
ctrl_ip="${ctrl_ip:-${ctrl_conn}}"

[[ "${ctrl_conn}" != "${ctrl_ip}" ]] &&
  print "Resolved '${ctrl_conn}' to IPv4 '${ctrl_ip}'" \
    "for matching SSDP responses"

# appliance setups have each receiver on a dedicated interface
iface_auto="$(ip -o route get "${ctrl_ip}" | grep --only-matching \
  --max-count=1 --perl-regexp "dev\s+\K(\S+)")"
IFACE="${IFACE:-"${iface_auto}"}"

check_vars_set ctrl_ip IFACE

print "Interface used for discovery: ${IFACE}"
print "Reporting changing messages only"

last_loc=
while read -r location; do

  if [[ "${location}" == *"${ctrl_ip}"* ]]; then

    if ! { ping_receiver "${ctrl_ip}" soap &&
      device_type="$(curl --connect-timeout "${CONN_TIMEOUT}" --max-time \
        "${TOTAL_TIMEOUT}" --fail --silent --user-agent "SKYPLUS_skyplus" \
        "${location}" | xidel - --silent \
        --xpath '/root/device/deviceType' 2>/dev/null)"; }; then

      print "Location verification failed (soap API not responding)"
      last_loc=

    fi

    # Sky+HD/SkyHD receivers respond with one message, containing
    # deviceType urn:schemas-nds-com:device:SkyControl:2

    # Sky Q receivers respond with two messages, containing deviceType
    # urn:schemas-nds-com:device:GatewaySkyServe:2 and
    # urn:schemas-nds-com:device:GatewaySkyControl:2
    # Only controlURL in the latter works for serviceType
    # urn:schemas-nds-com:service:SkyPlay:2

    # deviceType query also makes sure the soap API is working correctly
    # on Sky+HD/SkyHD. Rarely, soap API stops responding but command
    # API+TV service still works. Power cycle is needed to recover.
    # Without rejecting unverified response, verification step in
    # analyze service would run into a timeout
    if [[ "${device_type}" == *"SkyControl:2" ]]; then

      "${redis_cmd[@]}" SET "ssdp_url_ctrl:i${inst}" "${location}" \
        EX "${EXPIRE_S}" >&-

      [[ "${last_loc}" != "${location}" ]] && print "${location}"
      last_loc="${location}"

    fi

    systemd-notify WATCHDOG=1

  fi

done < <(gssdp-discover --interface="${IFACE}" \
  --target=urn:schemas-nds-com:service:SkyPlay:2 \
  --rescan-interval="${INTERVAL_S}" --message-type=available |
  grep --line-buffered --only-matching --perl-regexp 'Location:\s\K(\S+)')
