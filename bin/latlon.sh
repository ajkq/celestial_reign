#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset
set -o pipefail

check_deps gpspipe grep jq mv rm systemd-notify truncate

# Functions
check_deps check_bash_ver print write_test_var

# mapfile (4), EPOCHSECONDS (5)
check_bash_ver 5

POS_F="${POS_F:-"latlon"}"
POS_TMP_F="${POS_TMP_F:-".${POS_F}.tmp"}"
POS_LAST_F="${POS_LAST_F:-"${POS_F}_last"}"
POS_LAST_TMP_F="${POS_LAST_TMP_F:-".${POS_LAST_F}.tmp"}"
GPSD_HOST="${GPSD_HOST:-"localhost"}"
GPSD_PORT="${GPSD_PORT:-"2947"}"
NO_LOC_TH="${NO_LOC_TH:-"10"}"

function cleanup() {

  exit_status=$?

  [[ -f "${POS_F-}" ]] && {
    truncate -s 0 "${POS_F}"
    [[ -f "${POS_TMP_F-}" ]] && rm -f "${POS_TMP_F}"
    print "Stopped"
  }

  exit "${exit_status}"

}

trap "exit" INT TERM
trap cleanup EXIT

write_test_var POS_F POS_TMP_F POS_LAST_F

gpsd_conn="${GPSD_HOST}:${GPSD_PORT}"
[[ -n "${GPSD_DEV-}" ]] && gpsd_conn+=":${GPSD_DEV}"

print "Started"

notif_dev="false"
notif_loc=
last_upd=
fail_count=

while read -r line; do

  [[ "${notif_dev}" != "true" ]] && {
    print "GNSS device present"
    notif_dev="true"
  }

  mapfile -t coord < <(jq -c -r '.lat//"",.lon//""' <<<"${line}")
  lat="${coord[0]-}"
  lon="${coord[1]-}"
  # lat, lon is only present if the GNSS device has a fix
  if [[ -n "${lat}" && -n "${lon}" ]]; then

    [[ "${notif_loc}" != "loc" ]] && {
      print "Location data present"
      notif_loc="loc"
    }

    # Do not update file more than once per second
    [[ "${last_upd}" != "${EPOCHSECONDS}" ]] && {

      echo "${lat},${lon}" >"${POS_LAST_TMP_F}"
      echo "${lat},${lon}" >"${POS_TMP_F}"
      # mv is atomic when source and dest. are on the same file system
      mv "${POS_LAST_TMP_F}" "${POS_LAST_F}"
      mv "${POS_TMP_F}" "${POS_F}"
    }

    last_upd="${EPOCHSECONDS}"
    fail_count=0

    # Truncate current coordinates file if no location data is received
    # at the start or if more than NO_LOC_TH consecutive messages are
    # received without location to prevent issues when reception is
    # spotty. Capture+alarm services use the current location for
    # logging. Coverage evaluates based on the last location in case of
    # GNSS blockage
  elif [[ -z "${fail_count}" ]] || ((fail_count > NO_LOC_TH)); then

    [[ "${notif_loc}" != "no_loc" ]] && {
      print "No location data present"
      notif_loc="no_loc"
    }

    [[ -s "${POS_F}" ]] && truncate -s 0 "${POS_F}"
    [[ -f "${POS_TMP_F}" ]] && rm -f "${POS_TMP_F}"

    ((++fail_count))

  else
    ((++fail_count))
  fi

  systemd-notify WATCHDOG=1

  # TPV records are only emitted if a GNSS device is present
done < <(gpspipe -w "${gpsd_conn}" | grep --fixed-strings --line-buffered TPV)
