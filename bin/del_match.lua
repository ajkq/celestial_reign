-- Description: Delete key on value match
-- Parameters: key, value
-- Usage (bash): redis-cli --raw EVAL "$(<del_match.lua)" 1 "${key}" "${value}"
if redis.call("get", KEYS[1]) == ARGV[1] then
    redis.call("del", KEYS[1])
    return 'OK'
else
    return
end
