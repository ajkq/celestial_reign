#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

check_deps rm sleep stty touch

# Functions
check_deps check_bash_ver check_vars_pos_int check_vars_set \
  load_bash_builtin print

# EPOCHSECONDS
check_bash_ver 5

# try to load builtin sleep, ignore failure
load_bash_builtin sleep rm || :

ALARM_F="${ALARM_F:-"var/alarm"}"

function cleanup() {

  exit_status=$?

  if [[ -f "${ALARM_F}" ]]; then
    print "Exit: alarm cleared"
    rm -f "${ALARM_F}"

    [[ "${POS_LOG}" == "true" ]] && [[ -s "${POS_F}" ]] && {
      coord="$(<"${POS_F}")"
      [[ -n "${coord}" ]] &&
        echo "${coord},2,S,0,${EPOCHSECONDS}" >>"${POS_LOG_F}"
    }

  fi
  print "Stopped"

  exit "${exit_status}"

}

trap "exit" INT TERM
trap cleanup EXIT

INTERVAL_S="${INTERVAL_S:-5}"
TIMEOUT_S="${TIMEOUT_S:-2}"
BAUD_RATE="${BAUD_RATE:-"115200"}"
SET_TTY="${SET_TTY:-"true"}"
# record GNSS coordinates of loss start/end events for mapping purposes
POS_LOG="${POS_LOG:-"false"}"
POS_LOG_F="${POS_LOG_F:-"log/detect-coord-alarm.log"}"
POS_F="${POS_F:-"tmp/latlon"}"

check_vars_set SERIAL_DEVICE
check_vars_pos_int BAUD_RATE INTERVAL_S TIMEOUT_S

# check if logs can be written to if enabled
[[ "${POS_LOG}" == "true" ]] && write_test_var POS_LOG_F

if ! [[ -c "${SERIAL_DEVICE-}" ]]; then
  print "Serial device not present"
  exit "${FAILURE}"
fi

[[ "${SET_TTY}" == "true" ]] && stty -F "${SERIAL_DEVICE}" \
  ispeed "${BAUD_RATE}" ospeed "${BAUD_RATE}" -echo crtscts

print "Running"

# get initial state
if [[ -f "${ALARM_F}" ]]; then
  print "Init: alarm state file present"
  prev_state="true"
else
  prev_state="false"
fi

i=0
unexp_th=10

shopt -s nocasematch

while true; do

  # Command disables console messages for 5 minutes. Re-do before status
  # query
  echo "SET ERROR MESSAGES OFF" >"${SERIAL_DEVICE}"
  sleep 1

  echo "SHOW STATUS SYSTEM LOCK" >"${SERIAL_DEVICE}"
  # Prompt starts with >. Response without. Captures request and response
  if ! read -r -t "${TIMEOUT_S}" -d'>' -a response <"${SERIAL_DEVICE}"; then
    print "Timeout"
    state="true"
  elif [[ "${response[*]}" == *"SHOW STATUS SYSTEM LOCK ON"* ]]; then
    state="false"
  elif [[ "${response[*]}" == *"SHOW STATUS SYSTEM LOCK OFF"* ]]; then
    state="true"
  elif [[ "${response[*]}" == *"SHOW STATUS SYSTEM LOCK No valid local data"* ]]; then
    state="true"
  elif [[ "${response[*]}" == *"SHOW STATUS SYSTEM LOCK DISABLED"* ]]; then
    state="false"
  elif ((i > unexp_th)); then
    print "Too many unexpected responses"
    state="false"
  else
    ((++i))
    print "Ignoring unexpected response '${response[*]}'"
    continue
  fi

  i=0

  if [[ "${state}" == "true" && "${prev_state}" != "true" ]]; then

    print "Alarm triggered"
    touch "${ALARM_F}"

    [[ "${POS_LOG}" == "true" ]] && [[ -s "${POS_F}" ]] && {

      coord="$(<"${POS_F}")"
      [[ -n "${coord}" ]] &&
        echo "${coord},0,S,0,${EPOCHSECONDS}" >>"${POS_LOG_F}"
    }

  elif [[ "${state}" != "true" && "${prev_state}" == "true" ]]; then

    print "Alarm cleared"
    rm -f "${ALARM_F}"

    [[ "${POS_LOG}" == "true" ]] && [[ -s "${POS_F}" ]] && {

      coord="$(<"${POS_F}")"
      [[ -n "${coord}" ]] &&
        echo "${coord},1,S,0,${EPOCHSECONDS}" >>"${POS_LOG_F}"
    }

  fi

  prev_state="${state}"

  sleep "${INTERVAL_S}"

done
