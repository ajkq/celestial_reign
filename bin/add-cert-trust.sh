#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

CERT_F="${CERT_F:-"/etc/nginx/server.pem"}"

check_deps certutil cut getent id mkdir sudo whoami

# Functions
check_deps print

trap "exit" INT TERM

function add_cert_trust() {
  local homedir cert_f
  homedir="${1}"
  cert_f="${2}"

  nssdb="${homedir}/.pki/nssdb"

  [[ -d "${nssdb}" ]] || {
    mkdir -p "${nssdb}"
    certutil -N -d sql:"${nssdb}" --empty-password
  }

  # Remove entry if it exists
  certutil -d sql:"${nssdb}" -D -n celestial_reign 2>/dev/null || :

  if certutil -d sql:"${nssdb}" -A -t "CT,," -n celestial_reign \
    -i "${cert_f}"; then

    print "Added certificate trust for user $(whoami)"

  fi
}

function usage() {
  print "Usage: ${0} <USER NAME>"
  exit "${INVALIDARGUMENT}"
}

if ((EUID != 0)); then
  print "Must be run as root"
  exit "${USAGE}"
fi

user="${1-}"

if [[ -z "${user}" ]]; then

  usage

elif id -u "${user}" >/dev/null 2>&1; then

  homedir="$(getent passwd "${user}" | cut -d: -f6)"

  bash_func="$(declare -f check_deps print add_cert_trust);"
  bash_func+="add_cert_trust '${homedir}' '${CERT_F}'"

  sudo -u "${user}" bash -c "${bash_func}"

else

  print "User ${user} does not exist"
  usage

fi
