#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset
set -o pipefail
shopt -s nullglob

check_deps ffmpeg mkdir mv nproc rm

# Functions
check_deps check_gnu_find check_redis check_vars_pos_int

check_gnu_find

REDIS_URL="${REDIS_URL:-"$(gen_redis_url)"}"

check_redis
redis_cmd=(redis-cli -u "${REDIS_URL-}" --no-auth-warning)

LOGLEVEL="${LOGLEVEL:-"panic"}"
LOG_EXEC="${LOG_EXEC:-"false"}"
# Allow empty value to use system time zone
TZ="${TZ-"UTC0"}"
[[ -n "${TZ}" ]] && export TZ

cd tmp || {
  print "Failed to change directory"
  exit "${INVALIDARGUMENT}"
}

mkdir -p preview

(($(nproc) < 4)) && low_power="true" || low_power="false"

PROCS="${PROCS:-"$(nproc)"}"

if [[ "${low_power}" == "true" ]]; then
  FORMAT="${FORMAT:-"jpeg"}"
else
  FORMAT="${FORMAT:-"avif"}"
fi

check_vars_pos_int PROCS

if [[ "${FORMAT}" == "avif" ]]; then
  QUALITY="${QUALITY:-"40"}"
  SPEED="${SPEED:-"4"}"
elif [[ "${FORMAT}" == "webp" ]]; then
  QUALITY="${QUALITY:-"50"}"
elif [[ "${FORMAT}" == "jpeg" ]]; then
  QUALITY="${QUALITY:-"8"}"
else
  print "Invalid FORMAT '${FORMAT}'"
  exit "${CONFIG}"
fi

[[ "${QUALITY}" =~ ^[0-9]+$ ]] || {
  print "Invalid QUALITY '${QUALITY}'"
  exit "${CONFIG}"
}

# delete stale preview files
msg='Deleted stale preview file %f, last modified %TFT%TT%Tz\n'
find preview -maxdepth 1 \
  \( -iname \*.avif -o -iname \*.webp -o -iname \*.jpeg \) \
  -type f -mmin +2 -exec rm -f {} \; -printf "${msg}"

function render() {
  local inst low_power dest dest_tmp ffmpeg_exec cfg type
  inst="${1-}"
  low_power="${2-}"
  verify_vars_pos_int inst || return "${FAILURE}"
  verify_vars_set low_power || return "${FAILURE}"

  dest="preview/${inst}.${FORMAT}"
  dest_tmp="preview/${inst}.tmp"

  ffmpeg_exec=(ffmpeg -nostdin -loglevel "${LOGLEVEL}" -nostats)

  [[ "${low_power}" == "true" ]] && ffmpeg_exec+=(-threads 1
    -filter_threads 1 -sws_flags bilinear)

  ffmpeg_exec+=(-i "${feed}")

  # Stretch image horizontally to a 16:9 aspect ratio in case local
  # capture runs in non-widescreen resolution
  cfg="$("${redis_cmd[@]}" --raw GET "cfg:i${inst}")"
  type="$(jq -r -c '.vid_src_conn.type//""' <<<"${cfg}")"
  [[ "${type}" == "local" ]] && ffmpeg_exec+=(-vf "scale=ceil(ih*16/9):ih")

  ffmpeg_exec+=(-frames:v 1)

  if [[ "${FORMAT}" == "avif" ]]; then
    ffmpeg_exec+=(-crf:v "${QUALITY}")
  elif [[ "${FORMAT}" =~ ^webp|jpeg$ ]]; then
    ffmpeg_exec+=(-q:v "${QUALITY}")
  fi

  if [[ "${FORMAT}" == "avif" ]]; then
    ffmpeg_exec+=(-pix_fmt yuv420p -c:v libaom-av1 -still-picture 1
      -cpu-used "${SPEED}" -usage realtime -f avif)
  elif [[ "${FORMAT}" == "webp" ]]; then
    ffmpeg_exec+=(-pix_fmt yuv420p -c:v libwebp -compression_level 6
      -preset photo -f webp)
  elif [[ "${FORMAT}" == "jpeg" ]]; then
    ffmpeg_exec+=(-pix_fmt yuvj420p -c:v mjpeg -f image2 -update 1)
  fi

  ffmpeg_exec+=("${dest_tmp}" -y)

  [[ "${LOG_EXEC}" == "true" ]] &&
    printf '[%2s]: %s' "${inst}" "${ffmpeg_exec[*]}" >&2

  # Prepend the receiver instance if logging is enabled since multiple
  # processes run in parallel. Do not anchor since loglevel may contain
  # flags
  if [[ ! "${LOGLEVEL}"  =~ quiet|panic ]] && "${ffmpeg_exec[@]}" 2>&1 |
    prepend "$(printf '[%2s]' "${inst}")" >&2; then

    mv "${dest_tmp}" "${dest}"

  elif [[ "${LOGLEVEL}" =~ quiet|panic ]] && "${ffmpeg_exec[@]}"; then
    mv "${dest_tmp}" "${dest}"

  else
    rm -f "${dest}"
  fi

}

i=0
for feed in capture/*/feed.m3u8; do

  inst="${feed%/*}"
  inst="${inst#*/}"

  if ((i % PROCS == 0)); then
    wait
  fi
  ((++i))

  render "${inst}" "${low_power}" &

done

wait
