#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset
set -o pipefail

CARD="${CARD:-0}"
TP="${TP:-11778}"
SR="${SR:-27500}"
POL="${POL:-V}"
PORT="${PORT:-0}"
UNIT="${UNIT:-2}"

check_deps dvbdate_ntp dvbtune head stdbuf systemd-notify xargs

# Functions
check_deps print

trap "exit" INT TERM
trap '' PIPE

device="/dev/dvb/adapter${CARD}"

[[ -e "${device}" ]] || {

  print "Device ${device} validation failed"
  exit "${FAILURE}"

}

# force line buffering so we can update watchdog. restart required after
# interface disconnect/reconnect or signal loss
[[ -n "${INVOCATION_ID-}" ]] && stdbuf -oL dvbdate_ntp -v -c "${UNIT}" \
  -d "${device}/demux0" | xargs -L30 systemd-notify WATCHDOG=1 &

# show initial output only
dvbtune -D "${PORT}" -c "${CARD}" -f "${TP}000" -p "${POL}" \
  -s "${SR}" -m 2>&1 | head -n15 >&2
