#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

check_deps amqp-publish date ffmpeg jo jq timeout xargs

# Functions
check_deps check_bash_ver check_rec_id check_redis check_sw_upd ev_log \
  format_interval gen_amqp_url gen_redis_url get_control_url get_sid \
  get_transport ping_receiver print set_pin

# printf/date
check_bash_ver 4.2

function cleanup() {

  exit_status=$?

  print "Done"

  exit "${exit_status}"

}

trap "exit" INT TERM
trap cleanup EXIT

# Import redis script
del_match="$(<"${BASH_SOURCE%/*}/del_match.lua")"

FEED_ACT_BIN="${FEED_ACT_BIN:-"bin/feed-act.sh"}"
TIMER_FREQ_M="${TIMER_FREQ_M:-4}"
FEED_ACT_RE_S="${FEED_ACT_RE_S:-"120"}"
ACT_WIN_EX_S="${ACT_WIN_EX_S:-"120"}"
BLOCKAGE_F="${BLOCKAGE_F:-"var/blockage"}"
ALARM_F="${ALARM_F:-"var/alarm"}"
# Show set PIN response message on failure. Requires curl ≥7.76.0
SHOW_PIN_ERR="${SHOW_PIN_ERR:-"true"}"
CHECK_SID="${CHECK_SID:-"true"}"
SVC_ENA_TOL_S="${SVC_ENA_TOL_S:-"60"}"
UPTIME_TOL_S="${UPTIME_TOL_S:-"120"}"
CAPT_FAIL_TH="${CAPT_FAIL_TH:-"8"}"
SID_CH_MAP="${SID_CH_MAP:-"true"}"
# Allow empty value to use system time zone
TZ="${TZ-"UTC0"}"
[[ -n "${TZ}" ]] && export TZ

# Maintain a terse event log (syslog tag cr-events)
function log() {
  ev_log "${inst-}" "${@}"
}

inst="${1-}"

check_rec_id "${inst}"

REDIS_URL="${REDIS_URL:-"$(gen_redis_url)"}"

check_redis
redis_cmd=(redis-cli -u "${REDIS_URL-}" --no-auth-warning)

# Support bash <5
cur_epoch="${EPOCHSECONDS:-"$(date -u '+%s')"}"

cfg="$("${redis_cmd[@]}" --raw GET "cfg:i${inst}")"
glb_cfg="$("${redis_cmd[@]}" --raw GET "cfg:i0")"

unset cfg_val
# //"" Return empty string instead of false/null
cfg_elem='.vid_analyze//"",.verify_chan//"",.ctrl_conn//"",.rec_type//""'
mapfile -t cfg_val < <(jq -r -c "${cfg_elem}" <<<"${cfg}")
vid_analyze="${cfg_val[0]-}"
verify_chan="${cfg_val[1]-}"
ctrl_conn="${cfg_val[2]-}"
rec_type="${cfg_val[3]-}"

unset cfg_val
cfg_elem='.alarm//""'
mapfile -t cfg_val < <(jq -r -c "${cfg_elem}" <<<"${glb_cfg}")
alarm="${cfg_val[0]-}"

target_chan="$("${redis_cmd[@]}" --raw GET "cur_chan:i${inst}")"

# Skip execution if user requested suspension to be able to send remote
# control commands without interference. This key expires automatically
ttl="$("${redis_cmd[@]}" --raw TTL "analyze_suspend:i${inst}")"
# -2: key does not exist, -1: key does not expire
((ttl >= 0)) && {

  print "Service suspended; Expires in $(format_interval "${ttl}")"
  verify_chan="false"
  vid_analyze="false"

}

# Skip if vessel is in geographically defined blockage
[[ -f "${BLOCKAGE_F}" ]] && {

  print "Blockage state active, service suspended"
  verify_chan="false"
  vid_analyze="false"

}

# Skip if alarm feature is enabled and state is active. State
# should be flagged if antenna control unit has no signal lock.
[[ "${alarm}" == "true" && -f "${ALARM_F}" ]] && {

  print "Alarm state active, service suspended"
  verify_chan="false"
  vid_analyze="false"

}

# Check if PIN entry is required and SID matches
[[ "${verify_chan}" == "true" ]] && ping_receiver "${ctrl_conn}" soap && {

  print "Running verification"

  # Set by cr-discover@.service
  ssdp_url="$("${redis_cmd[@]}" --raw GET "ssdp_url_ctrl:i${inst}")"

  if [[ -n "${ssdp_url}" ]]; then

    print "Device/model information:"
    get_rec_details "${ssdp_url}" || :

    if ! control_url="$(get_control_url "${ssdp_url}")"; then

      print "No control URL returned"

    fi

  else

    print "SSDP URL not set"

  fi

  [[ -n "${control_url-}" ]] && {

    print "Checking PIN lock"

    if transport="$(get_transport "${ssdp_url}" "${control_url}")"; then

      # Transport will say OK even if device is in standby or stuck on a
      # blue screen.
      print "Transport state: '${transport}'"

    else

      print "No Transport info returned"

    fi

    # Check if PIN entry is required
    if [[ "${transport}" == "ERROR_PIN_REQUIRED" ]]; then

      pin="$(jq -r -c '.pin//""' <<<"${cfg}")"

      if [[ -z "${pin}" ]]; then

        print "WARNING: PIN not configured"

      elif set_pin "${ssdp_url}" "${control_url}" "${pin}"; then

        print "PIN entry successful"

        log "PIN entry"

      else

        print "WARNING: PIN entry failed"

      fi

    fi

    # Check if current service ID matches what's been recorded during a
    # channel change/reset (SkyHD/Sky+HD). Check if current channel
    # matches (Sky Q). If not, request set.
    target_sid="$("${redis_cmd[@]}" --raw GET "target_sid:i${inst}")"

    # SID is available via soap API on SkyHD/Sky+HD/Sky Q. Response is
    # empty if device is in standby mode. Transport: ERROR_SIGNAL
    # returns previous channel (backup channel from reset) SID.
    if [[ "${CHECK_SID}" == "true" ]] &&
      [[ "${transport}" =~ ^(OK|ERROR_PIN_REQUIRED)$ ]] &&
      cur_sid="$(get_sid "${ssdp_url}" "${control_url}")"; then

      print "Checking service ID/channel"

      # Sky Q offers SID-Channel mapping via JSON API
      if [[ "${rec_type}" == "skyq" ]] && [[ "${SID_CH_MAP}" == "true" ]] &&
        ping_receiver "${ctrl_conn}" json &&
        cur_chan="$(get_channel "${ctrl_conn}" "${cur_sid}")"; then

        print "Current Ch: '${cur_chan}', Target Ch: '${target_chan}'"

        [[ "${cur_chan}" != "${target_chan}" ]] && {

          logmsg="Incorrect channel"
          [[ -n "${cur_chan}" ]] && logmsg+=" (${cur_chan})"

          print "${logmsg}"
          action="set"

        }

        # Check for pending update via json API
        if check_sw_upd "${ctrl_conn}"; then
          print "WARNING: Pending software update." \
            "Enter standby mode to finish update (rec_remote_control power)"
        fi

      else

        print "Current SID: '${cur_sid}', Target SID: '${target_sid}'"

        # Control service does not record SID if transport has
        # ERROR_SIGNAL state as it most likely is from previous channel.
        # This triggers mismatch. If A/V loss is detected, the set
        # request is subsequently changed to reset.

        # No or deleted target_sid is considered a mismatch. This
        # ensures that 1) after enabling verify_chan, correct channel is
        # set and SID gets recorded; 2) after a failed channel change
        # due to receiver being unreachable it is set to the correct
        # channel once network connection recovers.
        if [[ -z "${target_sid}" ]]; then

          logmsg="Incorrect service ID (not set)"

          print "${logmsg}"
          # Set correct channel instead of first reset'ing to backup channel
          action="set"

        elif [[ "${cur_sid}" != "${target_sid}" ]]; then

          logmsg="Incorrect service ID"
          [[ -n "${cur_sid}" ]] && logmsg+=" (${cur_sid})"

          print "${logmsg}"
          action="set"

        fi

      fi

    fi

  }

}

# Analyze video for freeze, audio for silence. If detected, request
# reset. Omit video analysis on radio channels (static image). Handle no
# feed/incorrect resolution state for network sources.

# Run vid_analyze even if verify_chan already requested set. Receiver
# may be stuck on a no signal screen requiring reset (change to backup,
# then target channel). Entering same channel does not recover service.
# SkyHD, Sky+HD, and Sky Q receivers behave this way.
[[ "${vid_analyze}" == "true" ]] && {

  print "Running analysis"

  unset cfg_val
  # //"" Return empty string instead of false/null
  cfg_elem='.no_feed_act//"",.res_act//"",.feed_act_group//""'
  mapfile -t cfg_val < <(jq -r -c "${cfg_elem}" <<<"${cfg}")
  no_feed_act="${cfg_val[0]-}"
  res_act="${cfg_val[1]-}"
  cfg_group="${cfg_val[2]-}"

  # Record no feed action group only if not already set. If it doesn't
  # match the config from the database, release possible lock owned by
  # instance.
  group_key="feed_act_group:i${inst}"
  "${redis_cmd[@]}" SET "${group_key}" "${cfg_group}" NX >&-
  group="$("${redis_cmd[@]}" --raw GET "${group_key}")"
  lock_key="feed_act_lock:g${group}"
  recent_key="feed_act_recent:g${group}"

  if [[ "${cfg_group}" != "${group}" ]]; then

    print "Feed action group changed"
    [[ "$("${redis_cmd[@]}" --raw EVAL "${del_match}" 1 "${lock_key}" \
      "${inst}")" == "OK" ]] && {

      print "Releasing lock/delay"
      "${redis_cmd[@]}" --scan --pattern "*:g${group}" |
        xargs -r "${redis_cmd[@]}" DEL
    }

    # Record new group
    "${redis_cmd[@]}" SET "${group_key}" "${cfg_group}" >&-

  fi

  fail_count="$("${redis_cmd[@]}" --raw GET "capture_fail_count:i${inst}")"
  fail_count="${fail_count:-0}"

  capture_src="$("${redis_cmd[@]}" --raw GET capture_src:i"${inst}")"

  if [[ "${capture_src}" =~ [AV] ]]; then

    if ((fail_count == 0)); then

      # Capture running stable. fail_count is reset after sleeping for
      # one WATCHDOG_NOTIFY cycle (default: 60 seconds).
      capture="yes"

    else

      # Capture may be stopping and starting repeatedly
      capture="yes_temp"

    fi

  else

    capture="no_temp"

    svc_ena_time="$("${redis_cmd[@]}" --raw GET "capture_svc_ena:i${inst}")"
    read -r -d. uptime </proc/uptime

    print "WARNING: Capture/detection not running"

    # Cases for ignoring no capture state
    if ((uptime < UPTIME_TOL_S)); then

      print "Recently started system"

    elif [[ "${svc_ena_time}" =~ ^[0-9]+$ ]] &&
      (((cur_epoch - svc_ena_time) < SVC_ENA_TOL_S)); then

      print "Recently enabled capture"

    elif [[ "$("${redis_cmd[@]}" --raw EXISTS "${recent_key}")" == "1" ]]; then

      print "Recently performed feed action"

    elif [[ "$("${redis_cmd[@]}" --raw EXISTS \
      "capture_recent_stop:i${inst}")" == "1" ]] &&
      ((fail_count < CAPT_FAIL_TH)); then

      print "Recently stopped capture"

    elif [[ "$("${redis_cmd[@]}" --raw EXISTS \
      "capture_failure:i${inst}")" == "1" ]]; then

      print "Capture device/dependency failure"

    # Key is updated when capture service starts. If it is missing,
    # something bad happened to the data in redis.
    elif [[ "$("${redis_cmd[@]}" --raw EXISTS \
      "capture_svc_start:i${inst}")" == "0" ]]; then

      print "Configuration failure. Possible data corruption"

    # elif: Do not ignore stopped capture based on SSDP service/channel
    # information which may be present even if receiver is not
    # outputting video.

    else

      capture="no"

    fi

  fi

  [[ "${capture}" == "yes"* && "${res_act}" == "true" ]] && {

    h_res="$("${redis_cmd[@]}" --raw GET "capture_res:i${inst}")"

  }

  # no_feed_act/res_act: database fields
  feed_act="false"
  if [[ "${capture}" == "no" && "${no_feed_act}" == "true" ]]; then

    print "Enabling no feed action"
    feed_act="true"
    act_type="feed"

  # Inexpensive encoders output standard definition after receiver reboot
  elif [[ "${capture}" == "yes"* && "${res_act}" == "true" &&
    "${h_res-}" == "720" ]]; then

    print "Enabling resolution action (horizontal resolution: ${h_res})"

    feed_act="true"
    act_type="res"

  # Release possible lock if capture has recovered or both feed_act
  # features are disabled.
  elif [[ "${capture}" == "yes" ]] ||
    { [[ "${no_feed_act}" != "true" && "${res_act}" != "true" ]]; }; then

    [[ "$("${redis_cmd[@]}" --raw EVAL "${del_match}" 1 "${lock_key}" \
      "${inst}")" == "OK" ]] && {

      print "Feed state cleared. Releasing lock"

      "${redis_cmd[@]}" DEL "${recent_key}" >&-

    }

  fi

  if [[ "${no_feed_act}" == "true" || "${res_act}" == "true" ]] &&
    [[ ! -x "${FEED_ACT_BIN}" ]]; then
    print "FEED_ACT_BIN ${FEED_ACT_BIN} not executable"
    feed_act="false"
  fi

  # If network source is offline and no feed action enabled, or if
  # network source is online but resolution is standard definition, aquire
  # group lock to have this instance reboot e.g., a multi-input encoder
  # without another analyze instance on the same encoder interfering.
  # All instances on one multi-input encoder need to be set to the same
  # group. With lock held, wait for initial and re-execution delays
  # before executing external script.
  feed_act_exec="false"
  if [[ "${feed_act}" == "true" ]]; then

    ini_delay_res="$(jq -r -c .res_act_ini_delay//'""' <<<"${cfg}")"
    ini_delay_feed="$(jq -r -c .feed_act_ini_delay//'""' <<<"${cfg}")"

    rex_delay="$(jq -r -c ."${act_type}"_act_rex_delay//'""' <<<"${cfg}")"

    ini_key="${act_type}_ini_act:g${group}"
    ini_key_res="res_ini_act:g${group}"
    ini_key_feed="feed_ini_act:g${group}"

    rex_key="${act_type}_rex_act:g${group}"

    check_vars_set ini_delay_res ini_delay_feed rex_delay

    if [[ "$("${redis_cmd[@]}" --raw SET \
      "${lock_key}" "${inst}" NX)" == "OK" ]]; then

      print "Lock acquired"

      # Set initial delay for both types. No feed state caused by
      # receiver reboot may seamlessly change to incorrect resolution.
      "${redis_cmd[@]}" --raw SET "${ini_key_res}" 0 EX "${ini_delay_res}" >&-

      print "Initial delay act_type: res set," \
        "expires in $(format_interval "${ini_delay_res}")"

      "${redis_cmd[@]}" --raw SET "${ini_key_feed}" 0 EX "${ini_delay_feed}" >&-

      print "Initial delay act_type: feed set," \
        "expires in $(format_interval "${ini_delay_feed}")"

    elif [[ "$("${redis_cmd[@]}" --raw GET "${lock_key}")" == "${inst}" ]]; then

      print "Active lock. Evaluating delay condition"

      ttl="$("${redis_cmd[@]}" --raw TTL "${ini_key}")"
      # -2: key does not exist, -1: key does not expire
      ((ttl >= 0)) &&
        print "${act_type} initial delay active, expires in" \
          "$(format_interval "${ttl}")"

      ttl="$("${redis_cmd[@]}" --raw TTL "${rex_key}")"
      ((ttl >= 0)) &&
        print "${act_type} re-exec delay active, expires in" \
          "$(format_interval "${ttl}")"

      [[ "$("${redis_cmd[@]}" --raw EXISTS "${ini_key}")" == "0" ]] &&
        [[ "$("${redis_cmd[@]}" --raw EXISTS "${rex_key}")" == "0" ]] && {

        print "Delay condition satisfied"

        print "${act_type} re-exec delay set, expires in" \
          "$(format_interval "${rex_delay}")"
        "${redis_cmd[@]}" SET "${rex_key}" 0 EX "${rex_delay}" >&-

        if [[ "${act_type}" == "feed" ]]; then

          log "No feed action"
          print "No feed action"

        elif [[ "${act_type}" == "res" ]]; then

          log "Incorrect resolution action"
          print "Incorrect resolution action"

        fi

        "${redis_cmd[@]}" SET "${recent_key}" 0 EX "${FEED_ACT_RE_S}" >&-
        feed_act_exec="true"

        print "Exec: '${FEED_ACT_BIN}' '${group}' '${act_type}'"
        timeout 20s "${FEED_ACT_BIN}" "${group}" "${act_type}" || :

      }

    else

      print "Failed to acquire lock"

    fi

  fi

  cur_chan_radio="$("${redis_cmd[@]}" --raw GET cur_chan_radio:i"${inst}")"

  if [[ "${capture}" == "no" && "${feed_act_exec}" != "true" ]]; then

    # Inexpensive encoders output corrupt data on hdmi input loss, causing
    # capture service to shut down. Need to wake receiver.
    print "Assuming input loss"

    logmsg="input"
    action="reset"

  elif [[ "${capture}" == "yes"* && "${cur_chan_radio}" == "true" ]]; then

    # Radio channels have static picture, check for audio only
    print "Radio channel, skipping video check"

    if [[ "${capture_src}" != *"A"* ]]; then

      print "WARNING: No audio information being analyzed by capture service"

    elif [[ "$("${redis_cmd[@]}" --raw EXISTS \
      "audio_loss:i${inst}")" == "1" ]]; then

      print "No audio detected"
      logmsg="audio"
      action="reset"

    fi

  elif [[ "${capture}" == "yes"* ]]; then

    [[ "$("${redis_cmd[@]}" --raw EXISTS "video_loss:i${inst}")" == "1" ]] && {

      print "No video detected"
      logmsg_temp="video"
      action="reset"

    }

    [[ "$("${redis_cmd[@]}" --raw EXISTS "audio_loss:i${inst}")" == "1" ]] && {

      print "No audio detected"
      [[ -n "${logmsg_temp-}" ]] && logmsg_temp+="+"
      logmsg_temp+="audio"
      action="reset"

    }

    [[ -n "${logmsg_temp-}" ]] && logmsg="${logmsg_temp}"

  fi

  [[ "${action-}" == "reset" && -n "${logmsg-}" ]] && logmsg="No ${logmsg}"

}

# Reset rate limiter if action window expired (e.g., due to manual
# shutdown).
if [[ "$("${redis_cmd[@]}" --raw EXISTS "act_window:i${inst}")" == "0" ]]; then

  [[ "$("${redis_cmd[@]}" --raw EXISTS \
    "rate_limit_notif:i${inst}")" == "1" ]] && {

    print "Rate limit reset"
    log "Rate limit reset"

  }

  "${redis_cmd[@]}" DEL "act_count:i${inst}" >&-
  "${redis_cmd[@]}" DEL "rate_limit_stime:i${inst}" >&-
  "${redis_cmd[@]}" DEL "rate_limit_notif:i${inst}" >&-

fi

# Rate limiting for sustained A/V loss
if [[ -n "${action-}" ]]; then

  # Record action window start time, auto-expiring after 120 seconds
  "${redis_cmd[@]}" SET "act_window:i${inst}" "${cur_epoch}" NX \
    EX "${ACT_WIN_EX_S}" >&-
  # Extend lifetime if we are in an action window
  "${redis_cmd[@]}" EXPIRE "act_window:i${inst}" "${ACT_WIN_EX_S}" >&-

fi

act_win_stime="$("${redis_cmd[@]}" --raw GET "act_window:i${inst}")"

if [[ -n "${act_win_stime}" ]]; then

  print "Action window counter: $("${redis_cmd[@]}" INCR "act_count:i${inst}")"

  # Calculate elapsed minutes since action window start
  act_win_dur="$(((cur_epoch - act_win_stime) / 60))"

  act_count="$("${redis_cmd[@]}" --raw GET act_count:i"${inst}")"

  # shellcheck disable=SC2194
  case 1 in
  $((act_win_dur < 5))) factor=1 ;;
  $((act_win_dur < 60))) factor=4 ;;    # Run every min after  5m
  $((act_win_dur < 120))) factor=8 ;;   # Run every  2m after  1h
  $((act_win_dur < 180))) factor=12 ;;  # Run every  3m after  2h
  $((act_win_dur < 720))) factor=20 ;;  # Run every  5m after  3h
  $((act_win_dur < 1440))) factor=40 ;; # Run every 10m after 12h
  *) factor=80 ;;                       # Run every 20m after  1d
  esac

  if [[ "$((act_count % factor))" -ne 0 ]]; then

    # Rate limit

    # Record when rate limiter first kicked in
    "${redis_cmd[@]}" SET "rate_limit_stime:i${inst}" "${cur_epoch}" NX >&-

    # Log when rate limiter kicks in or goes up a notch
    ((factor > 1)) &&
      [[ "$("${redis_cmd[@]}" --raw GET \
        "rate_limit_notif:i${inst}")" != "${factor}" ]] && {

      "${redis_cmd[@]}" SET "rate_limit_notif:i${inst}" "${factor}" >&-
      log "Rate limit: approx. every $((factor / TIMER_FREQ_M)) min"

    }

    rate_limit_stime="$("${redis_cmd[@]}" --raw GET \
      "rate_limit_stime:i${inst}")"
    rate_limit_stime="$(convert_unix_tstamp_iso "${rate_limit_stime}")"

    print "Rate limit active since ${rate_limit_stime}"
    print "Process request every ${factor} runs/approx." \
      "$((factor / TIMER_FREQ_M)) min"
    [[ -n "${action-}" ]] && print "Ignoring request '${action}'"
    # cancel request
    action=

  fi

fi

# Control service requires cur_chan for set/reset. If it is missing,
# something bad happened to the data in redis.
[[ -z "${target_chan}" && "${action-}" =~ ^(reset|set)$ ]] && {
  print "Configuration failure. Possible data corruption. Canceling request"
  action=
}

# Send request to the associated control service instance
[[ "${action-}" =~ ^(reset|set)$ ]] && {

  print "Requesting channel '${action}'"

  [[ -n "${logmsg-}" ]] && log "${logmsg}"

  amqp_url="$(gen_amqp_url)"
  payload="$(jo type=channel action="${action}" instance="${inst}" \
    requestor=_analyze)"

  amqp-publish -u "${amqp_url}" --exchange="celestial_reign" \
    -r "rec_ctrl.${inst}" -p -C json -b "${payload}" || print "AMQP error"

}

exit "${SUCCESS}"
