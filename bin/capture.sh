#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset
set -o pipefail

check_deps date ffmpeg grep jq kill logger mediainfo nc realpath rm sed \
  sleep systemd-notify tsp

# Functions
check_deps check_bash_ver check_ffmpeg_filters check_gnu_stat check_rec_id \
  check_redis check_vars_pos_int gen_redis_url load_bash_builtin print \
  verify_ffmpeg_progress verify_vars_set

check_gnu_stat

# Mapfile (4), EPOCHSECONDS (5)
check_bash_ver 5

# Try to load builtins, ignore failure. sleep supports seconds only
load_bash_builtin sleep realpath rm || :

RECENT_STOP_S="${RECENT_STOP_S:-"60"}"

inst="${1-}"
check_rec_id "${inst}"

REDIS_URL="${REDIS_URL:-"$(gen_redis_url)"}"

check_redis
redis_cmd=(redis-cli -u "${REDIS_URL-}" --no-auth-warning)

function cleanup() {

  exit_status=$?

  [[ -z "${inst-}" ]] && exit "${exit_status}"

  # Remove keys indicating A/V detection is running
  "${redis_cmd[@]}" DEL "capture_src:i${inst}" >&-

  # Flag recent shutdown if not set, expiring automatically
  expire="${RECENT_STOP_S}"
  [[ "$("${redis_cmd[@]}" SET "capture_recent_stop:i${inst}" 0 EX "${expire}" \
    NX)" == "OK" ]] && print "Flag recent shutdown (TTL ${expire})"

  # Add/increment fail counter to detect repeatedly restarting service
  print "Shutdown count:" \
    "$("${redis_cmd[@]}" INCR "capture_fail_count:i${inst}")"

  cur_epoch="${EPOCHSECONDS}"

  # Clear A/V loss keys. Log if deleted
  [[ "$("${redis_cmd[@]}" --raw DEL "audio_loss:i${inst}")" != "0" ]] && {

    log "AUDIO: audio_loss cleared (service exit)"

    [[ "${POS_LOG}" == "true" ]] && [[ ! -f "${NO_COV_F}" ]] &&
      [[ -s "${POS_F}" ]] && {

      coord="$(<"${POS_F}")"
      [[ -n "${coord}" ]] &&
        echo "${coord},2,A,${inst},${cur_epoch}" >>"${POS_LOG_F}"
    }

  }

  [[ "$("${redis_cmd[@]}" --raw DEL "video_loss:i${inst}")" != "0" ]] && {

    log "VIDEO: video_loss cleared (service exit)"

    [[ "${POS_LOG}" == "true" ]] && [[ ! -f "${NO_COV_F}" ]] &&
      [[ -s "${POS_F}" ]] && {

      coord="$(<"${POS_F}")"
      [[ -n "${coord}" ]] &&
        echo "${coord},2,V,${inst},${cur_epoch}" >>"${POS_LOG_F}"
    }

  }

  "${redis_cmd[@]}" DEL "capture_res:i${inst}" >&-

  # Clean up segments
  [[ -d "${target_dir-}" ]] && rm -f "${target_dir}"/feed*

  [[ "${exit_status}" == "${NOTINSTALLED}" ]] &&
    "${redis_cmd[@]}" SET "capture_failure:i${inst}" 0 >&-

  exit "${exit_status}"

}

# Maintain log with detection events (syslog tag cr-detect-events)
function log() {
  printf -v message '%-2s %s\n' "${inst}" "${*}"
  logger --id=$$ -t cr-detect-events "${message}"
}

# Clear existing A/V loss keys in case the trap couldn't (e.g., redis
# connection error)
[[ "$("${redis_cmd[@]}" --raw DEL "audio_loss:i${inst-}")" != "0" ]] && {
  log "AUDIO: audio_loss cleared (unclean shutdown)"
  print "audio_loss cleared (unclean shutdown)"
}
[[ "$("${redis_cmd[@]}" --raw DEL "video_loss:i${inst-}")" != "0" ]] && {
  log "VIDEO: video_loss cleared (unclean shutdown)"
  print "video_loss cleared (unclean shutdown)"
}

trap "exit" INT TERM
trap cleanup EXIT

# Update capture start key. Do not delete on exit. Used to prevent
# incorrect no capture detection by analyze service if data in redis is
# bad.
"${redis_cmd[@]}" SET "capture_svc_start:i${inst}" "${EPOCHSECONDS}" >&-

LOGLEVEL="${LOGLEVEL:-"level+info"}"
SEGMENTS="${SEGMENTS:-"10"}"
NOTIFY_S="${NOTIFY_S:-"60"}"
LOSS_CHK_M="${LOSS_CHK_M:-"15"}"
LOSS_TH_M="${LOSS_TH_M:-"240"}"
UPTIME_RND_S="${UPTIME_RND_S:-"120"}"

# Timestamped detection event log
EVENT_LOG="${EVENT_LOG:-"true"}"

# Record GNSS coordinates of loss start/end events for mapping purposes
POS_LOG="${POS_LOG:-"false"}"
POS_LOG_F="${POS_LOG_F:-"log/detect-coord.log"}"
POS_F="${POS_F:-"tmp/latlon"}"

NO_COV_F="${NO_COV_F:-"var/out_of_coverage"}"

AUD_DUR_S="${AUD_DUR_S:-"25"}"
VID_DUR_S="${VID_DUR_S:-"25"}"
VID_THRESH="${VID_THRESH:-"-60"}"
AUD_THRESH="${AUD_THRESH:-"-80"}"
AUDIO="${AUDIO:-"true"}"
EXIT_ERR="${EXIT_ERR:-"true"}"
PROG_PORT="${PROG_PORT:-"$(progress_bind_port "${inst-}")"}"

export TSDUCK_NO_VERSION_CHECK=1

check_vars_pos_int LOSS_TH_M LOSS_CHK_M NOTIFY_S SEGMENTS AUD_DUR_S \
  VID_DUR_S PROG_PORT

# Check if logs can be written to if enabled
[[ "${POS_LOG}" == "true" ]] && write_test_var POS_LOG_F

check_ffmpeg_filters freezedetect silencedetect

# Randomized start delay on boot
read -r -d. uptime </proc/uptime

if ((uptime < UPTIME_RND_S)); then

  l_sleep $((1 + RANDOM % 10))

fi

cfg="$("${redis_cmd[@]}" --raw GET "cfg:i${inst}")"
# //"" Return empty string instead of false/null
vid_src_conn="$(jq -r -c '.vid_src_conn//""' <<<"${cfg}")"

type="$(jq -r -c '.type//""' <<<"${vid_src_conn}")"

print "Type '${type}'"

target_dir="tmp/capture/${inst}"
mkdir -p "${target_dir}"

fs_type="$(stat -f -c %T "${target_dir}")"

[[ "${fs_type}" != "tmpfs" ]] && {
  print "WARNING: Target directory not on tmpfs"
}

# Mask PIP preview picture
rec_type="$(jq -r -c '.rec_type//""' <<<"${cfg}")"

if [[ "${rec_type}" == "sky+hd" ]]; then

  # Mini TV can be disabled on SkyHD/Sky+HD receivers
  CROP_PIP="${CROP_PIP:-"false"}"

  # Cut off right side
  CROP_H_POS="${CROP_H_POS:-"0"}"
  CROP_WIDTH="${CROP_WIDTH:-"65"}"

elif [[ "${rec_type}" == "skyq" ]]; then

  CROP_PIP="${CROP_PIP:-"true"}"

  # Cut off left side
  CROP_H_POS="${CROP_H_POS:-"out_w"}"
  CROP_WIDTH="${CROP_WIDTH:-"75"}"

else
  CROP_PIP="false"
fi

[[ ${CROP_PIP} == "true" ]] && {

  print "Video detection: Crop PIP mini TV enabled (${rec_type})"
  vf="crop=out_w=${CROP_WIDTH}/100*in_w:out_h=in_h:x=${CROP_H_POS}:y=0,"

}

vid_detect="${target_dir}/vid_detect"
aud_detect="${target_dir}/aud_detect"

[[ -p "${vid_detect}" ]] || mkfifo "${vid_detect}"
[[ -p "${aud_detect}" ]] || mkfifo "${aud_detect}"

playlist="${target_dir}/feed.m3u8"

# Write detected audio/video loss information to named pipes
vf+="freezedetect@1=d=${VID_DUR_S}:n=${VID_THRESH}dB,"
vf+="metadata=mode=print:file=${vid_detect}:direct=1"
af="silencedetect@1=d=${AUD_DUR_S}:n=${AUD_THRESH}dB,"
af+="ametadata=mode=print:file=${aud_detect}:direct=1"

ffmpeg_out_opts=(-vf "${vf}" -af "${af}")

if [[ "${type}" == "local" ]]; then

  elem='.alsa.device//"",.alsa.channels//"",.alsa.sample_rate//"",'
  elem+='.v4l2.device//"",.v4l2.framerate//"",.v4l2.video_size//"",'
  elem+='.v4l2.input_format//""'

  mapfile -t val < <(jq -r -c "${elem}" <<<"${vid_src_conn}")
  alsa_device="${val[0]-}"
  alsa_channels="${val[1]-}"
  alsa_sample_rate="${val[2]-}"
  v4l2_device="${val[3]-}"
  v4l2_framerate="${val[4]-}"
  v4l2_video_size="${val[5]-}"
  v4l2_input_format="${val[6]-}"
  declare sources

  input_idx=0

  verify_vars_set v4l2_device && {

    # Rawvideo segments take a lot of space and likely need a drop-in
    # config for opt-celestial_reign-tmp.mount to create a bigger tmpfs
    # mount, default to off.
    if [[ "${v4l2_input_format}" == "rawvideo" ]]; then
      FEED="${FEED:-"false"}"
    else
      FEED="${FEED:-"true"}"
    fi

    # FFmpeg doesn't like a symlink (possibly set by udev rules) as v4l2
    # input, need to resolve it. Built-in realpath fails and produces
    # empty output if the file does not exist, unlike GNU coreutils
    # realpath, which outputs input.
    v4l2_device_real="$(realpath "${v4l2_device}" 2>/dev/null || :)"

    if [[ -n "${v4l2_device_real}" ]] &&
      [[ "${v4l2_device}" != "${v4l2_device_real}" ]]; then
      print "v4l2 device: ${v4l2_device} => ${v4l2_device_real}"
    elif [[ -z "${v4l2_device_real}" ]]; then
      print "Failed to resolve v4l2 device '${v4l2_device}'"
    fi

    # Check if file is a character special device
    [[ -c "${v4l2_device_real}" ]] || {
      print "v4l2.device validation failed"
      # Used by analyze to ignore not-running capture
      "${redis_cmd[@]}" SET "capture_failure:i${inst-}" 0 >&-
      exit "${FAILURE}"
    }

    ffmpeg_in_opts+=(-f v4l2 -timestamps abs -thread_queue_size 256
      -video_size "${v4l2_video_size}" -input_format "${v4l2_input_format}"
      -framerate "${v4l2_framerate}" -i "${v4l2_device_real}")

    sources+="V"
    # Null output for detect filters needs explicit maps
    ffmpeg_out_opts+=(-map "${input_idx}":v:0 -f null)
    input_idx="$((input_idx + 1))"
  }

  verify_vars_set alsa_device && {

    ffmpeg_in_opts+=(-f alsa -ar "${alsa_sample_rate}" -ac "${alsa_channels}"
      -thread_queue_size 256 -i "${alsa_device}")

    sources+="A"
    ffmpeg_out_opts+=(-map "${input_idx}":a:0 -f null)
    input_idx="$((input_idx + 1))"
  }

  ffmpeg_out_opts+=(out.null)

  # Allow feed generation to be turned off. Rendering preview image
  # directly (fps=1/30 -f image2 -update 1) instead of stream copy &
  # cr-preview.service causes alsa xruns and prevents silencedetect from
  # working
  [[ "${FEED-}" == "true" ]] && {

    segment_wrap="$(calc_int "${SEGMENTS}*2")"

    # Stream_segment muxer/mkv container allows rawvideo, mjpeg, lpcm, etc.
    # Does not play smoothly but good enough for preview extraction
    ffmpeg_out_opts+=(-an -c copy -f stream_segment -segment_time 1
      -segment_list "${playlist}" -segment_list_flags live
      -segment_list_size "${SEGMENTS}" -segment_wrap "${segment_wrap}"
      "${target_dir}/feed-%d.mkv")
  }

elif [[ "${type}" == "network" ]]; then

  elem='.udp.host//"",.udp.port//"",.udp.localaddr//""'

  mapfile -t val < <(jq -r -c "${elem}" <<<"${vid_src_conn}")
  udp_host="${val[0]-}"
  udp_port="${val[1]-}"
  udp_localaddr="${val[2]-}"

  FEED="${FEED:-"true"}"

  tsp_exec=(tsp --receive-timeout 5000 -I ip)

  [[ -n "${udp_localaddr}" ]] && {
    check_localaddr "${udp_localaddr}"
    tsp_exec+=(-l "${udp_localaddr}")
  }

  tsp_exec+=("${udp_host}:${udp_port}")

  sources="V"
  if [[ "${AUDIO}" != "true" ]]; then
    ffmpeg_in_opts+=(-an)
  else
    # Assume network stream contains both video and audio
    sources+="A"
  fi

  # xerror: stop when receiving corrupt data (e.g., HDMI input loss).
  # Freeze start/end doesn't get detected reliably after input recovery
  # without this option on some encoders. Analyze requests channel reset
  # after capture_recent_shutdown flag has expired or capture_fail_count
  # exceeds CAPT_FAIL_TH (default: 8) to wake receiver. Optional for
  # buggy encoders
  [[ "${EXIT_ERR}" == "true" ]] && ffmpeg_in_opts+=(-xerror)
  ffmpeg_in_opts+=(-i pipe:)

  ffmpeg_out_opts+=(-map 0:v:0? -f null -map 0:a:0? -f null out.null)

  [[ "${FEED}" == "true" ]] && {
    # Use hls muxer/ts container for UDP MPEG TS streams
    ffmpeg_out_opts+=(-an -c copy -f hls -hls_time 1 -hls_segment_filename
      "${target_dir}/feed-%d.ts" -hls_flags +delete_segments
      -hls_list_size "${SEGMENTS}" "${playlist}")
  }

else

  print "Unsupported type"
  exit "${CONFIG}"

fi

# Send progress info to UDP address which we'll periodically check to
# update the watchdog
ffmpeg_exec=(ffmpeg -nostdin -loglevel "${LOGLEVEL}" -nostats
  -progress "udp://127.0.0.1:${PROG_PORT}"
  "${ffmpeg_in_opts[@]}" "${ffmpeg_out_opts[@]}")

# Filter continuity errors reported by null output (if loglevel >error)
# spamming log. Do not anchor since loglevel may contain flags
[[ "${LOGLEVEL}" =~ quiet|panic|fatal ]] || sed_filter="/\[null/d;"

if [[ -n "${tsp_exec[*]}" ]]; then

  # Filter ongoing output about each segment written
  [[ "${LOGLEVEL}" =~ quiet|panic|fatal|error|warning &&
    "${FEED}" == "true" ]] || sed_filter+="/\[hls/d;"
  [[ "${LOGLEVEL}" =~ verbose|debug|trace && "${FEED}" == "true" ]] &&
    sed_filter+="/AVIOContext/d;/EXT-X-MEDIA-SEQUENCE/d;"

  print "${tsp_exec[*]-} | ${ffmpeg_exec[*]}"

  [[ -n "${ADDL_SED_SCR-}" ]] && sed_filter+="${ADDL_SED_SCR}"

  if [[ -n "${sed_filter-}" ]]; then

    # Redirect stdout+stderr to sed for filtering log output, then
    # output to stderr. Subshell allows PID of ffmpeg to be captured
    "${tsp_exec[@]}" | "${ffmpeg_exec[@]}" &> >(sed -u "${sed_filter}" >&2) &

  else

    "${tsp_exec[@]}" | "${ffmpeg_exec[@]}" &

  fi

else

  [[ "${LOGLEVEL}" =~ quiet|panic|fatal|error|warning &&
    "${FEED}" == "true" ]] || sed_filter+="/\[stream_segment/d;"
  [[ "${LOGLEVEL}" =~ verbose|debug|trace && "${FEED}" == "true" ]] &&
    sed_filter+="/AVIOContext/d;"

  print "${ffmpeg_exec[*]-}"

  [[ -n "${ADDL_SED_SCR-}" ]] && sed_filter+="${ADDL_SED_SCR}"

  if [[ -n "${sed_filter-}" ]]; then

    "${ffmpeg_exec[@]}" &> >(sed -u "${sed_filter}" >&2) &

  else

    "${ffmpeg_exec[@]}" &

  fi

fi

ffmpeg_pid=$!

# Collect pids of persistent background tasks for monitoring
bg_pids=()
# Read audio/video loss information from named pipes, set keys in redis
while IFS= read -r line; do

  [[ "${LOGLEVEL}" =~ debug|trace|info|verbose ]] || print "VIDEO: ${line}"
  [[ "${EVENT_LOG}" == "true" ]] && log "VIDEO: ${line}"

  cur_epoch="${EPOCHSECONDS}"

  if [[ "${line}" == *"freeze_start"* ]]; then

    "${redis_cmd[@]}" SET "video_loss:i${inst}" "${cur_epoch}" >&-

  # In rare cases ffmpeg emits duration but not end
  elif [[ "${line}" =~ (freeze_end|freeze_duration) ]]; then

    "${redis_cmd[@]}" DEL "video_loss:i${inst}" >&-

  fi

  # Log current coordinates to file for start/end loss events if
  # enabled, not out-of-coverage, and coordinates file not truncated
  [[ "${line}" =~ (freeze_start|freeze_end) ]] &&
    [[ "${POS_LOG}" == "true" ]] && [[ ! -f "${NO_COV_F}" ]] &&
    [[ -s "${POS_F}" ]] && {

    coord="$(<"${POS_F}")"
    if [[ -n "${coord}" ]] && [[ "${line}" == *"freeze_start"* ]]; then
      echo "${coord},0,V,${inst},${cur_epoch}" >>"${POS_LOG_F}"
    elif [[ -n "${coord}" ]] && [[ "${line}" == *"freeze_end"* ]]; then
      echo "${coord},1,V,${inst},${cur_epoch}" >>"${POS_LOG_F}"
    fi
  }

done <"${vid_detect}" &
bg_pids+=($!)

while IFS= read -r line; do

  [[ ! "${LOGLEVEL}" =~ debug|trace|info|verbose ]] && print "AUDIO: ${line}"
  [[ "${EVENT_LOG}" == "true" ]] && log "AUDIO: ${line}"

  cur_epoch="${EPOCHSECONDS}"

  if [[ "${line}" == *"silence_start"* ]]; then

    "${redis_cmd[@]}" SET "audio_loss:i${inst}" "${cur_epoch}" >&-

  elif [[ "${line}" =~ (silence_end|silence_duration) ]]; then

    "${redis_cmd[@]}" DEL "audio_loss:i${inst}" >&-

  fi

  # Log current coordinates to file for start/end loss events if
  # enabled, not out-of-coverage, and coordinates file not truncated
  [[ "${line}" =~ (silence_start|silence_end) ]] &&
    [[ "${POS_LOG}" == "true" ]] && [[ ! -f "${NO_COV_F}" ]] &&
    [[ -s "${POS_F}" ]] && {

    coord="$(<"${POS_F}")"
    if [[ -n "${coord}" ]] && [[ "${line}" == *"silence_start"* ]]; then
      echo "${coord},0,A,${inst},${cur_epoch}" >>"${POS_LOG_F}"
    elif [[ -n "${coord}" ]] && [[ "${line}" == *"silence_end"* ]]; then
      echo "${coord},1,A,${inst},${cur_epoch}" >>"${POS_LOG_F}"
    fi
  }

done <"${aud_detect}" &
bg_pids+=($!)

# Background task exits once done
i=0
err="false"
while true; do

  ((++i))
  sleep 1

  # Exit if no data is coming in
  if ((i > 5)); then

    kill $$

  elif verify_ffmpeg_progress "${PROG_PORT}"; then

    print "Fully started up"

    msg="Detection threshold: Video: ${VID_THRESH}dB"
    [[ "${sources}" == *"A"* ]] && msg+="; Audio: ${AUD_THRESH}dB"
    print "${msg}"

    # Flag that we're processing frames
    "${redis_cmd[@]}" SET "capture_src:i${inst}" "${sources}" >&- || err="true"
    # Remove shutdown flag
    "${redis_cmd[@]}" DEL "capture_recent_stop:i${inst}" >&- || err="true"
    # Remove failure flag (inaccessible v4l2 device)
    "${redis_cmd[@]}" DEL "capture_failure:i${inst-}" >&- || err="true"

    systemd-notify WATCHDOG=1

    [[ "${FEED}" == "true" && "${type}" == "network" ]] && {
      if h_res="$(mediainfo --Inform="Video;%Width%" "${playlist}")"; then
        "${redis_cmd[@]}" SET "capture_res:i${inst}" "${h_res}" >&- ||
          err="true"
        print "Horizontal resolution: ${h_res}"
      fi
    }

    if [[ "${err}" == "true" ]]; then
      print "Startup signaling background task failed, exiting"
      kill $$
    fi

    break

  fi

done &

while true; do

  # Multiple USB capture cards running at too high resolution/framerate
  # can exceed USB bus bandwith, causing ffmpeg to lock up. Subscribing
  # to a stream while the encoder outputs an input loss image with the
  # source recovering afterwards can also cause capture to stall. Check
  # if capture time is changing and update watchdog
  sleep "${NOTIFY_S}"

  verify_ffmpeg_progress "${PROG_PORT}" && systemd-notify WATCHDOG=1

done &
bg_pids+=($!)

while true; do

  # Periodically check if A/V loss persists. If over threshold, stop
  # service as a precaution. The issue is re-detected after service
  # restart if it persists
  sleep "$((LOSS_CHK_M * 60))"
  persist_av_loss_exit="false"

  cur_epoch="${EPOCHSECONDS}"

  video_loss_start="$("${redis_cmd[@]}" --raw GET "video_loss:i${inst}")"
  audio_loss_start="$("${redis_cmd[@]}" --raw GET "audio_loss:i${inst}")"

  cur_chan_radio="$("${redis_cmd[@]}" --raw GET "cur_chan_radio:i${inst}")"

  if [[ "${video_loss_start}" =~ ^[0-9]+$ ]] &&
    [[ "${cur_chan_radio}" != "true" ]]; then

    video_loss_dur=$((cur_epoch - video_loss_start))

    ((video_loss_dur > (LOSS_TH_M * 60))) && {

      print "WARNING: Video loss persists for more than" \
        "${LOSS_CHK_M} minutes"
      log "VIDEO: video_loss >${LOSS_CHK_M} min, exit"
      print "Shutting down service as a precaution"
      print "Condition is re-detected after restart if it persists"

      persist_av_loss_exit="true"

    }

  fi

  if [[ "${audio_loss_start}" =~ ^[0-9]+$ ]]; then

    audio_loss_dur=$((cur_epoch - audio_loss_start))

    ((audio_loss_dur > (LOSS_TH_M * 60))) && {

      print "WARNING: Audio loss persists for more than" \
        "${LOSS_CHK_M} minutes"
      log "AUDIO: audio_loss >${LOSS_CHK_M} min, exit"
      print "Shutting down service as a precaution"
      print "Condition is re-detected after restart if it persists"

      persist_av_loss_exit="true"

    }

  fi

  [[ "${persist_av_loss_exit}" == "true" ]] && kill $$

done &
bg_pids+=($!)

# background task exits once done
{

  sleep "${NOTIFY_S}"

  # Do not remove failed start count immediately. TBS 2603 encoders may
  # output a stream that passes progress check only to immediately
  # become corrupt after if the receiver is off. If we remove failed
  # start count immediately, analyze thinks capture is running and does
  # not try to power up receiver
  "${redis_cmd[@]}" DEL "capture_fail_count:i${inst}" >&- || {

    print "Startup background task failed, exiting"
    kill $$

  }

} &

# Repeatedly check if backgrounded loops are running
while true; do
  for bg_pid in "${bg_pids[@]}"; do
    if ! kill -0 "${bg_pid}" 2>/dev/null; then
      print "A persistent background process failed, exiting"
      kill $$
    fi
  done
  sleep 30
done &

wait "${ffmpeg_pid}"
