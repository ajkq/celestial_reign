#!/usr/bin/env bash

# shellcheck disable=SC2034
readonly SUCCESS=0
readonly FAILURE=1
readonly INVALIDARGUMENT=2
readonly NOTIMPLEMENTED=3
readonly NOTINSTALLED=5
readonly USAGE=64
readonly DATAERR=65
readonly NOINPUT=66
readonly NOPERM=77
readonly CONFIG=78

LC_ALL=en_US.UTF-8
export LC_ALL

# desc: print
# parameters: text(s)
# echoes to stderr
# no return value
function print() {
  echo -e "${*}" >&2
}

# desc: convert unix timestamp to ISO8601 extended format
# parameters: epoch
# echoes to stdout
# no return value
function convert_unix_tstamp_iso() {
  local timestamp offset date_time
  timestamp="${1-}"

  if printf -v offset '%(%z)T' "${timestamp}" &&
    printf -v date_time '%(%FT%T)T' "${timestamp}"; then

    if [[ "${offset}" == "+0000" ]]; then
      echo "${date_time}Z"
    else
      echo "${date_time}${offset:0:3}:${offset:3}"
    fi

  fi
}

# desc: log to syslog (cr-events): script_name instance text
# parameters: instance, text(s)
# no output
# no return value
function ev_log() {
  check_deps logger
  local name inst text
  # get script name without extension/-exec
  name="${0##*/}"
  name="${name%%.*}"
  name="${name%%-exec}"

  inst="${1-}"
  shift
  text="${*}"

  printf -v message '%-10.10s %-2s %s\n' "${name}" "${inst}" "${text}"
  logger --id=$$ -t cr-events "${message}"
}

# desc: check if functions are defined/programs are present and executable
# parameters: commands
# echoes to stderr
# exits script on failed check
function check_deps() {
  local arr cmd
  arr=("${@}")
  for cmd in "${arr[@]}"; do
    if ! type "${cmd}" >/dev/null 2>&1; then
      echo >&2 "${cmd} is missing, not executable, or not defined"
      exit "${NOTINSTALLED}"
    fi
  done
}

# desc: sleep+print message
# parameters: seconds
# echoes to stderr
# no return value
# exits script on failed dependency check
function l_sleep() {
  check_deps print sleep
  local sleep text
  sleep="${1-}"
  if [[ "${sleep}" =~ ^[1-9][0-9]*$ ]]; then
    text="Waiting for ${sleep} second"
    [[ "${sleep}" == "1" ]] || text+="s"
    print "${text}"
    sleep "${sleep}"
  fi
}

# desc: check rec_id
# parameters: rec_id
# echoes to stderr
# exits script on failed check
function check_rec_id() {
  check_deps print
  local inst
  inst="${1-}"
  if ! [[ "${inst}" =~ ^[1-9][0-9]*$ ]]; then
    print "rec_id validation failed"
    exit "${CONFIG}"
  fi
}

# desc: verify if variables are set and not empty
# parameters: variables
# echoes to stderr
# returns FAILURE on failed check
# exits script on failed dependency check
function verify_vars_set() {
  check_deps print
  local arr var ret
  ret="true"
  arr=("${@}")
  for var in "${arr[@]}"; do
    if [[ -z "${!var:+1}" ]]; then
      print "${FUNCNAME[0]}: ${var} empty or not defined"
      ret="false"
    fi
  done
  if [[ "${ret}" != "true" ]]; then
    return "${FAILURE}"
  fi
}

# desc: check if variables are set and not empty
# parameters: variables
# echoes to stderr
# exits script on failed check
function check_vars_set() {
  check_deps verify_vars_set
  verify_vars_set "${@}" || exit "${CONFIG}"
}

# desc: check if variable value is positive integer
# parameters: variable(s)
# echoes to stderr
# returns FAILURE on failed check
# exits script on failed dependency check
function verify_vars_pos_int() {
  check_deps print
  local arr var ret
  ret="true"
  arr=("${@}")
  for var in "${arr[@]}"; do
    if [[ -n "${!var:+1}" ]] && [[ ! "${!var}" =~ ^[1-9][0-9]*$ ]]; then
      print "${FUNCNAME[0]}: Not an integer: ${var}=${!var}"
      ret="false"
    elif [[ -z "${!var:+1}" ]]; then
      print "${FUNCNAME[0]}: ${var} empty or not defined"
      ret="false"
    fi
  done
  if [[ "${ret}" != "true" ]]; then
    return "${FAILURE}"
  fi
}

# desc: check if variable value is positive integer
# parameters: variables
# echoes to stderr
# exits script on failed check
function check_vars_pos_int() {
  check_deps verify_vars_pos_int
  verify_vars_pos_int "${@}" || exit "${CONFIG}"
}

# desc: require minimum bash version
# parameters: decimal_version
# echoes to stderr
# exits script on failed check
function check_bash_ver() {
  check_deps bc print
  local req_ver bash_ver
  req_ver="${1-}"
  [[ "${req_ver}" =~ ^[0-9]+(\.[0-9]+)?$ ]] || {
    print "${FUNCNAME[0]}: Pass decimal version"
    exit "${USAGE}"
  }
  bash_ver="${BASH_VERSINFO[0]}.${BASH_VERSINFO[1]}"
  if (($(bc <<<"${bash_ver} >= ${req_ver}") == 0)); then
    print "Bash >=${req_ver} required"
    exit "${NOTINSTALLED}"
  fi
}

# desc: check if gnu stat is installed
# no parameters
# echoes to stderr
# exits script on failed check
function check_gnu_stat() {
  check_deps grep print stat
  if ! stat --version 2>/dev/null | grep --quiet \
    --fixed-strings coreutils; then
    print "GNU coreutils version of stat required"
    exit "${NOTINSTALLED}"
  fi
}

# desc: check if gnu grep is installed
# no parameters
# echoes to stderr
# no return value
# exits script on failed check
function check_gnu_grep() {
  check_deps grep print
  if ! grep --version 2>/dev/null | grep --quiet \
    --fixed-strings 'GNU grep'; then
    print "GNU grep required"
    exit "${NOTINSTALLED}"
  fi
}

# desc: check if gnu find is installed
# no parameters
# echoes to stderr
# exits script on failed check
function check_gnu_find() {
  check_deps grep print find
  if ! find . --version 2>/dev/null | grep --quiet \
    --fixed-strings findutils; then
    print "GNU findutils version of find required"
    exit "${NOTINSTALLED}"
  fi
}

# desc: get password from file. require private permissions
# parameters: file
# echoes to stderr
# no return value
# exits script on failed dependency check
function get_file_passwd() {
  check_deps id check_gnu_stat print
  check_gnu_stat

  local pass_file pass_file_cont stat
  pass_file="${1-}"

  [[ -f "${pass_file}" ]] &&
    read -ra stat <<<"$(stat -c "%u %a" "${pass_file}")"

  if [[ -f "${pass_file}" ]] &&
    [[ "${stat[1]-}" =~ ^(6|4)00$ ]] &&
    [[ "$(id -u)" =~ ^(${stat[0]-}|0)$ ]]; then

    pass_file_cont="$(<"${pass_file}")"
  else
    print "'${pass_file}' does not exist, isn't owned by user," \
      "or is accessible by others"
  fi

  echo "${pass_file_cont-}"
}

# desc: generate redis connection url
# no parameters
# echoes to stdout
# no return value
# exits script on failed dependency check
function gen_redis_url() {
  check_deps check_vars_set check_gnu_stat id print
  check_gnu_stat

  local redis_url

  REDIS_USER="${REDIS_USER:-"default"}"
  REDIS_PASS_F="${REDIS_PASS_F:-"etc/.redis_pass"}"
  REDIS_PASS="${REDIS_PASS:-$(get_file_passwd "${REDIS_PASS_F}")}"
  REDIS_HOST="${REDIS_HOST:-"localhost"}"
  REDIS_PORT="${REDIS_PORT:-"6379"}"
  REDIS_DB="${REDIS_DB:-"0"}"

  check_vars_set REDIS_PASS

  redis_url="redis://${REDIS_USER}:${REDIS_PASS}@${REDIS_HOST}:${REDIS_PORT}"
  redis_url+="/${REDIS_DB}"

  echo "${redis_url}"
}

# desc: generate amqp connection url
# no parameters
# echoes to stdout
# no return value
# exits script on failed dependency check
function gen_amqp_url() {
  check_deps check_vars_set check_gnu_stat id print
  check_gnu_stat

  local amqp_url

  AMQP_USER="${AMQP_USER:-"celestial_reign"}"
  AMQP_PASS_F="${AMQP_PASS_F:-"etc/.amqp_pass"}"
  AMQP_PASS="${AMQP_PASS:-$(get_file_passwd "${AMQP_PASS_F}")}"
  AMQP_HOST="${AMQP_HOST:-"localhost"}"
  AMQP_PORT="${AMQP_PORT:-"5672"}"
  # default if not set, allow variable to be empty
  AMQP_VHOST="${AMQP_VHOST-"celestial_reign"}"

  check_vars_set AMQP_PASS

  amqp_url="amqp://${AMQP_USER}:${AMQP_PASS}@${AMQP_HOST}:${AMQP_PORT}"
  amqp_url+="/${AMQP_VHOST}"

  echo "${amqp_url}"
}

# desc: check redis
# no parameters
# echoes to stderr
# exits script on failed check
function check_redis() {
  check_deps grep print redis-cli
  redis_cmd=(redis-cli -u "${REDIS_URL-}" --no-auth-warning)
  if ! "${redis_cmd[@]}" ping | grep --quiet --fixed-strings \
    --ignore-case pong; then
    print "Redis error"
    exit "${FAILURE}"
  fi
}

# desc: check if localaddr is present
# parameters: localaddr
# echoes to stderr
# exits script on failed check
function check_localaddr() {
  local sys_ip sys_ips ret localaddr
  check_deps hostname print
  localaddr="${1-}"

  read -r -a sys_ips <<<"$(hostname --all-ip-addresses)"
  ret="false"
  for sys_ip in "${sys_ips[@]}"; do
    [[ "${sys_ip}" == "${localaddr}" ]] && {
      ret="true"
    }
  done

  if [[ "${ret}" != "true" ]]; then
    print "localaddr '${localaddr}' invalid. Link failure or configuration " \
      "error"
    # Return exit code that triggers auto-restart in case of link failure
    exit "${FAILURE}"
  fi
}

# desc: calc rounded to integer
# parameters: bc_math_expr
# echoes to stdout
# no return value
# exits script on failed dependency check
function calc_int() {
  check_deps bc
  printf "%.0f" "$(bc -l <<<"${1-}")"
}

# desc: print+execute command
# parameters: command
# echoes to stderr
# returns exit code from executed command,
# exits script on failed dependency check
function exe() {
  local exec
  check_deps print
  exec=("${@}")

  print "${exec[@]}"
  "${exec[@]}"
}

# desc: check if receiver api is reachable
# parameters: ctrl_conn, api
# echoes to stderr
# returns SUCCESS,
#         FAILURE on incorrect usage/failure
function ping_receiver() {
  local port ctrl_conn api
  ctrl_conn="${1-}"
  api="${2-}"
  check_deps nc verify_vars_set print

  verify_vars_set ctrl_conn api || return "${FAILURE}"

  if [[ "${api}" == "json" ]]; then
    port=9006
  elif [[ "${api}" == "soap" ]]; then
    port=49153
  elif [[ "${api}" == "command" ]]; then
    port=49160
  else
    print "${FUNCNAME[0]}: Incorrect API"
    return "${FAILURE}"
  fi

  # Do not redirect stderr to null to see if name resolution failures
  # are the cause in the logs
  if nc -z -w 5 "${ctrl_conn}" "${port}"; then
    return "${SUCCESS}"
  else
    print "Receiver unreachable (${api} API)"
    return "${FAILURE}"
  fi
}

# desc: get receiver device/model info
# parameters: ssdp_url_SkyPlay
# echoes to stderr+stdout
# returns FAILURE on incorrect usage/failure
# exits script on failed dependency check
function get_rec_details() {
  TOTAL_TIMEOUT="${TOTAL_TIMEOUT:-"8"}"
  CONN_TIMEOUT="${CONN_TIMEOUT:-"4"}"

  local ssdp_url response

  ssdp_url="${1-}"
  check_deps check_vars_pos_int curl verify_vars_set xidel

  check_vars_pos_int CONN_TIMEOUT TOTAL_TIMEOUT

  verify_vars_set ssdp_url || return "${FAILURE}"

  if response="$(curl --connect-timeout "${CONN_TIMEOUT}" --max-time \
    "${TOTAL_TIMEOUT}" --fail --silent --user-agent "SKYPLUS_skyplus" \
    "${ssdp_url}")"; then

    response="$(xidel - --silent --xpath '/root/device/deviceType' \
      --xpath '/root/device/modelNumber' <<<"${response}" 2>/dev/null)"

    if [[ -n "${response}" ]]; then
      echo "${response}"
    else
      print "${FUNCNAME[0]}: Empty response"
      return "${FAILURE}"
    fi

  else
    print "${FUNCNAME[0]}: Request failed"
    return "${FAILURE}"
  fi

}

# desc: get control url SkyPlay
# parameters: ssdp_url_SkyPlay
# echoes to stdout+stderr
# returns FAILURE on failure
# exits script on failed dependency check
function get_control_url() {
  TOTAL_TIMEOUT="${TOTAL_TIMEOUT:-"8"}"
  CONN_TIMEOUT="${CONN_TIMEOUT:-"4"}"

  local ssdp_url response
  ssdp_url="${1-}"

  check_deps check_vars_pos_int curl verify_vars_set xidel

  check_vars_pos_int CONN_TIMEOUT TOTAL_TIMEOUT

  verify_vars_set ssdp_url || return "${FAILURE}"

  # Timeouts are needed to prevent hangs if receiver locks up and fails
  # to perform channel changes
  if response="$(curl --connect-timeout "${CONN_TIMEOUT}" --max-time \
    "${TOTAL_TIMEOUT}" --fail --silent \
    --write-out "%{stderr}${FUNCNAME[0]}: HTTP %{http_code}\n" \
    --user-agent "SKYPLUS_skyplus" "${ssdp_url}")"; then

    # xidel shows a huge backtrace if there is no root element if, e.g.,
    # receiver is not fully booted up; hide it
    response="$(xidel - --silent --xpath \
      '/root/device/serviceList/service[serviceId =
      "urn:nds-com:serviceId:SkyPlay"]/controlURL' \
      <<<"${response}" 2>/dev/null)"

    if [[ -n "${response}" ]]; then
      echo "${response}"
    else
      print "${FUNCNAME[0]}: Empty response"
      return "${FAILURE}"
    fi

  else
    print "${FUNCNAME[0]}: Request failed"
    return "${FAILURE}"
  fi

}

# desc: get current service id
# parameters: ssdp_url_SkyPlay, control_url_SkyPlay
# echoes to stderr+stdout
# returns FAILURE on incorrect usage/failure
# exits script on failed dependency check
function get_sid() {
  TOTAL_TIMEOUT="${TOTAL_TIMEOUT:-"8"}"
  CONN_TIMEOUT="${CONN_TIMEOUT:-"4"}"

  local request response orig_response ssdp_url control_url headers
  ssdp_url="${1-}"
  control_url="${2-}"

  check_deps check_vars_pos_int curl verify_vars_set xidel

  check_vars_pos_int TOTAL_TIMEOUT CONN_TIMEOUT

  verify_vars_set ssdp_url control_url || return "${FAILURE}"

  read -r -d '' request <<"EOF" || :
<?xml version="1.0" encoding="utf-8"?>
<s:Envelope s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
  <s:Body>
    <u:GetMediaInfo xmlns:u="urn:schemas-nds-com:service:SkyPlay:2">
      <InstanceID>0</InstanceID>
    </u:GetMediaInfo>
  </s:Body>
</s:Envelope>
EOF

  headers=(--header "Content-Length: ${#request}" --header
    "SOAPACTION: \"urn:schemas-nds-com:service:SkyPlay:2#GetMediaInfo\""
    --header "Content-Type: text/xml;charset=utf-8")

  if response="$(curl --connect-timeout "${CONN_TIMEOUT}" --max-time \
    "${TOTAL_TIMEOUT}" --fail --silent \
    --write-out "%{stderr}${FUNCNAME[0]}: HTTP %{http_code}\n" \
    --request POST --data "${request}" "${headers[@]}" \
    "${ssdp_url%/*}${control_url}")"; then

    response="$(xidel - --silent --xpath \
      '/s:Envelope/s:Body/u:GetMediaInfoResponse/CurrentURI' \
      <<<"${response}" 2>/dev/null)"

    orig_response="${response}"
    response="${response##*/}"

    if [[ -n "${response}" ]]; then
      if [[ "${orig_response}" == "xsi://"* ]]; then
        # Convert hex to decimal (needed for Sky Q Service ID-Channel lookup)
        echo "$((16#${response}))"
      else
        # Manually tuned returns dvb://. Since those are only selectable
        # via the menu and do not get a regular channel number we can't
        # support them anyway
        print "${FUNCNAME[0]}: Unsupported channel type: '${orig_response}'"
        return "${FAILURE}"
      fi
    else
      # An empty response is usually a proper response body with an
      # empty CurrentURI tag, which typically happens after a power
      # cycle/firmware update
      print "${FUNCNAME[0]}: Empty response"
      return "${FAILURE}"
    fi

  else
    print "${FUNCNAME[0]}: Request failed"
    return "${FAILURE}"
  fi

}

# desc: check if software update is pending (Sky Q)
# parameters: ctrl_conn
# echoes to stderr+stdout
# returns SUCCESS on pending update,
#         FAILURE on incorrect usage/failure
# exits script on failed dependency check
function check_sw_upd() {
  local ctrl_conn response

  ctrl_conn="${1-}"
  check_deps jq unbuffer verify_vars_set websocat

  verify_vars_set ctrl_conn || return "${FAILURE}"

  # websocat exits 0 on failure and outputs error messages to stdout.
  # It does not work as a service without unbuffer fake tty
  if response="$(unbuffer -p websocat -1E \
    "ws://${ctrl_conn}:9006/as/system/status" |
    jq -r -c '.swupdate.state' 2>/dev/null)"; then

    if [[ "${response}" == "available" ]]; then
      return "${SUCCESS}"
    else
      return "${FAILURE}"
    fi

  else
    print "${FUNCNAME[0]}: Request failed"
    return "${FAILURE}"
  fi

}

# desc: get channel from service ID (Sky Q)
# parameters: ctrl_conn, sid
# echoes to stderr+stdout
# returns FAILURE on incorrect usage/failure
# exits script on failed dependency check
function get_channel() {
  TOTAL_TIMEOUT="${TOTAL_TIMEOUT:-"8"}"
  CONN_TIMEOUT="${CONN_TIMEOUT:-"4"}"

  local ctrl_conn sid response

  ctrl_conn="${1-}"
  sid="${2-}"

  check_deps check_vars_pos_int curl verify_vars_set jq

  check_vars_pos_int TOTAL_TIMEOUT CONN_TIMEOUT

  verify_vars_set ctrl_conn sid || return "${FAILURE}"

  if response="$(curl --connect-timeout "${CONN_TIMEOUT}" --max-time \
    "${TOTAL_TIMEOUT}" --fail --silent --write-out \
    "%{stderr}${FUNCNAME[0]}: HTTP %{http_code}\n" \
    "http://${ctrl_conn}:9006/as/services")"; then

    response="$(jq -r -c ".services[]? | select(.sid == \"${sid}\") | .c" \
      <<<"${response}")"

    if [[ -n "${response}" ]]; then
      echo "${response}"
    else
      print "${FUNCNAME[0]}: Channel lookup failed"
      return "${FAILURE}"
    fi

  else
    print "${FUNCNAME[0]}: Request failed"
    return "${FAILURE}"
  fi

}

# desc: get transport state
# parameters: ssdp_url_SkyPlay, control_url_SkyPlay
# echoes to stdout+stderr
# returns FAILURE on incorrect usage
# exits script on failed dependency check
function get_transport() {
  TOTAL_TIMEOUT="${TOTAL_TIMEOUT:-"8"}"
  CONN_TIMEOUT="${CONN_TIMEOUT:-"4"}"

  local ssdp_url control_url request response headers
  ssdp_url="${1-}"
  control_url="${2-}"

  check_deps check_vars_pos_int curl verify_vars_set xidel

  check_vars_pos_int TOTAL_TIMEOUT CONN_TIMEOUT

  verify_vars_set ssdp_url control_url || return "${FAILURE}"

  read -r -d '' request <<"EOF" || :
<?xml version="1.0" encoding="utf-8"?>
<s:Envelope s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
  <s:Body>
    <u:GetTransportInfo xmlns:u="urn:schemas-nds-com:service:SkyPlay:2">
      <InstanceID>0</InstanceID>
    </u:GetTransportInfo>
  </s:Body>
</s:Envelope>
EOF

  headers=(--header "Content-Length: ${#request}" --header
    "SOAPACTION: \"urn:schemas-nds-com:service:SkyPlay:2#GetTransportInfo\""
    --header "Content-Type: text/xml;charset=utf-8")

  if response="$(curl --connect-timeout "${CONN_TIMEOUT}" --max-time \
    "${TOTAL_TIMEOUT}" --fail --silent \
    --write-out "%{stderr}${FUNCNAME[0]}: HTTP %{http_code}\n" \
    --request POST --data "${request}" "${headers[@]}" \
    "${ssdp_url%/*}${control_url}")"; then

    response="$(xidel - --silent --xpath \
      '/s:Envelope/s:Body/u:GetTransportInfoResponse/CurrentTransportStatus' \
      <<<"${response}" 2>/dev/null)"

    if [[ -n "${response}" ]]; then
      echo "${response}"
    else
      print "${FUNCNAME[0]}: Empty response"
      return "${FAILURE}"
    fi

  else
    print "${FUNCNAME[0]}: Request failed"
    return "${FAILURE}"
  fi

}

# desc: transmit pin
# parameters: ssdp_url_SkyPlay, control_url_SkyPlay, pin
# echoes to stderr
# returns FAILURE on incorrect usage/failure
# exits script on failed dependency check
function set_pin() {
  TOTAL_TIMEOUT="${TOTAL_TIMEOUT:-"8"}"
  CONN_TIMEOUT="${CONN_TIMEOUT:-"4"}"

  local ssdp_url control_url pin request response headers fail_param
  ssdp_url="${1-}"
  control_url="${2-}"
  pin="${3-}"

  check_deps check_vars_pos_int curl grep verify_vars_set

  check_vars_pos_int TOTAL_TIMEOUT CONN_TIMEOUT

  verify_vars_set ssdp_url control_url pin || return "${FAILURE}"

  # Show API response on failure if requested. Requires curl ≥7.76.0
  if [[ "${SHOW_PIN_ERR-}" == "true" ]]; then
    fail_param=(--fail-with-body)
  else
    fail_param=(--fail)
  fi

  read -r -d '' request <<EOF || :
<?xml version="1.0" encoding="utf-8"?>
<s:Envelope s:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/"
  xmlns:s="http://schemas.xmlsoap.org/soap/envelope/">
  <s:Body>
    <u:X_NDS_SetUserPIN xmlns:u="urn:schemas-nds-com:service:SkyPlay:2">
      <InstanceID>0</InstanceID>
      <UserPIN>${pin}</UserPIN>
    </u:X_NDS_SetUserPIN>
  </s:Body>
</s:Envelope>
EOF

  headers=(--header "Content-Length: ${#request}" --header
    "SOAPACTION: \"urn:schemas-nds-com:service:SkyPlay:2#X_NDS_SetUserPIN\""
    --header "Content-Type: text/xml;charset=utf-8")

  if response="$(curl --connect-timeout "${CONN_TIMEOUT}" \
    --max-time "${TOTAL_TIMEOUT}" "${fail_param[@]}" --silent \
    --write-out "%{stderr}${FUNCNAME[0]}: HTTP %{http_code}\n" \
    --request POST --data "${request}" "${headers[@]}" \
    "${ssdp_url%/*}${control_url}")"; then

    return "${SUCCESS}"

  else

    print "${FUNCNAME[0]}: Request failed"

    [[ "${SHOW_PIN_ERR-}" == "true" ]] && grep --fixed-strings --ignore-case \
      -e errorcode -e errordescription <<<"${response}" >&2

    return "${FAILURE}"

  fi

}

# desc: generate default bind port for progress stats
# parameters: ID
# echoes to stdout
# no return value
# exits script on failed dependency check
function progress_bind_port() {
  check_deps check_rec_id
  check_rec_id "${1-}"
  echo $((49200 + ${1}))
}

# desc: verify ffmpeg progress
# parameters: progress_port
# echoes to stderr
# returns: SUCCESS,
#          FAILURE if output time is not increasing, or if no progress
#          data returned in 5 seconds,
#          USAGE if no/empty progress_port is passed as argument
# exits script on failed dependency check
function verify_ffmpeg_progress() {
  check_deps check_gnu_grep print nc timeout verify_vars_set
  check_gnu_grep
  local progress progress_a progress_b progress_port
  progress_port="${1-}"
  verify_vars_set progress_port || return "${USAGE}"

  # fetch value of two consecutive records of out_time_us
  mapfile -t progress < <(timeout 5s nc -lu -p "${progress_port}" |
    grep --max-count=2 --only-matching --perl-regexp "out_time_us=\K[0-9]+")

  verify_vars_set progress 2>/dev/null || {
    print "No progress data returned"
    return "${FAILURE}"
  }
  progress_a="${progress[0]-}"
  progress_b="${progress[1]-}"

  if ! { [[ "${progress_a}" =~ [0-9]+ ]] &&
    [[ "${progress_b}" =~ [0-9]+ ]]; }; then
    print "Invalid progress data returned"
    return "${FAILURE}"
  fi

  if ((progress_b == 0)); then
    print "No progress"
    return "${FAILURE}"
  elif ((progress_b > progress_a)); then
    return "${SUCCESS}"
  else
    print "Progress stalled"
    return "${FAILURE}"
  fi
}

# desc: check ffmpeg filter support
# parameters: filters
# echoes to stderr
# exits script on failed check
function check_ffmpeg_filters() (
  local filters filter installed
  check_deps ffmpeg print grep
  filters=("${@}")
  # subshell to keep IFS local
  IFS= installed="$(ffmpeg -filters 2>&1)"
  for filter in "${filters[@]}"; do
    if ! grep --quiet --fixed-strings "${filter}" <<<"${installed}"; then
      print "ffmpeg ${filter} filter missing"
      exit "${NOTINSTALLED}"
    fi
  done
)

# desc: test write access for file assigned to variable
# parameters: variable(s)
# echoes to stderr
# no return value; exits script on failed check
function write_test_var() {
  check_deps print touch
  local vars var test_fail
  test_fail=false
  vars=("${@}")
  for var in "${vars[@]}"; do
    if [[ -z "${!var:+1}" ]]; then
      print "${FUNCNAME[0]}: ${var} empty or not defined"
      test_fail=true
    # touch access time only
    elif ! touch -a "${!var}" >/dev/null 2>&1; then
      print "Write test for ${var} '${!var}' failed"
      test_fail=true
    fi
  done
  if [[ "${test_fail}" == true ]]; then
    exit "${NOPERM}"
  fi
}

# desc: format interval as N day(s) N hour(s) N minute(s) N second(s)
# with precision as needed
# parameters: interval_seconds
# echoes to stdout
# returns FAILURE on invalid input
function format_interval() {
  check_deps print
  local interval d h m s
  declare -a text
  interval="${1-}"
  [[ "${interval}" =~ ^[0-9]+$ ]] || {
    print "${FUNCNAME[0]}: Invalid interval"
    return "${FAILURE}"
  }

  d=$((interval / 60 / 60 / 24))
  h=$((interval / 60 / 60 % 24))
  m=$((interval / 60 % 60))
  s=$((interval % 60))

  if [[ "${d}" -gt 0 ]]; then
    text+=("${d}")
    [[ "${d}" == 1 ]] && text+=("day") || text+=("days")
  fi
  if [[ "${h}" -gt 0 ]]; then
    text+=("${h}")
    [[ "${h}" == 1 ]] && text+=("hour") || text+=("hours")
  fi
  if [[ "${m}" -gt 0 ]]; then
    text+=("${m}")
    [[ "${m}" == 1 ]] && text+=("minute") || text+=("minutes")
  fi
  if [[ "${s}" -gt 0 ]]; then
    text+=("${s}")
    [[ "${s}" == 1 ]] && text+=("second") || text+=("seconds")
  fi

  echo "${text[*]}"
}

# desc: allow alphanumeric characters, hyphen; optionally trim length;
# strip leading/trailing spaces;
# parameters: text, trim_length
# echoes to stdout
# exits script on failed dependency check
function sanitize_text() {
  check_deps xargs
  local text trim_len
  text="${1-}"
  trim_len="${2-}"
  shopt -s extglob
  # remove non-alphanumeric characters
  text="${text//+([^ A-Za-z0-9-])/}"
  shopt -u extglob
  [[ "${trim_len}" =~ ^[1-9][0-9]*$ ]] && text="${text:0:${trim_len}}"
  # strip leading/trailing whitespace
  text="$(xargs <<<"${text}")"
  echo "${text}"
}

# desc: load bash builtin
# parameters: builtin_command_name(s)
# echoes to stderr
# returns: SUCCESS, FAILURE
# exits script on failed dependency check
function load_bash_builtin() {
  check_deps print
  local cmd cmd_path arr err bash_lib_fallback
  arr=("${@}")

  if [[ -d "/usr/lib/bash" ]]; then
    bash_lib_fallback="/usr/lib/bash"
  elif [[ -d "/usr/local/lib/bash" ]]; then
    bash_lib_fallback="/usr/local/lib/bash"
  fi
  BASH_LIB="${BASH_LIB:-"${bash_lib_fallback-}"}"
  if [[ -z "${BASH_LIB}" ]]; then
    print "Failed to load bash builtin command(s) ${arr[*]}"
    print "Install package 'bash-builtins' (apt) or export BASH_LIB"
    return "${FAILURE}"
  fi

  err="false"
  for cmd in "${arr[@]}"; do
    cmd_path="${BASH_LIB}/${cmd}"
    if ! enable -f "${cmd_path}" "${cmd}" 2>/dev/null; then
      print "Failed to load bash builtin command ${cmd_path}"
      err="true"
    fi
  done
  if [[ "${err}" == "true" ]]; then
    return "${FAILURE}"
  fi
}

# desc: prepend text to output
# parameters: none
# reads from stdin
# echoes to stdout
# no return value
function prepend() {
  local line
  while read -r line; do
    echo "${*}: ${line}"
  done
}
