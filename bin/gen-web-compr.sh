#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

WEB_ROOT_D="${WEB_ROOT_D:-"/var/www/html/cr"}"

check_deps grep nginx rm

# Functions
check_deps print

[[ -d "${WEB_ROOT_D}" ]] || {
  print "Web root directory '${WEB_ROOT_D}' does not exist"
  exit "${FAILURE}"
}

if nginx -T 2>/dev/null | grep --quiet --fixed-string 'brotli_static on'; then

  check_deps brotli

  print "brotli_static on, generating brotli compressed files"

  for file in "${WEB_ROOT_D}"/*; do
    [[ "${file##*.}" == "br" ]] && continue
    [[ -d "${file}" ]] && continue
    [[ "${file##*.}" == "inc" ]] && continue
    brotli -f "${file}"
  done
  for file in "${WEB_ROOT_D}"/doc/*; do
    [[ "${file##*.}" == "br" ]] && continue
    [[ -d "${file}" ]] && continue
    brotli -f "${file}"
  done

else

  print "brotli_static off or not defined, deleting brotli compressed files"

  rm -f "${WEB_ROOT_D}"/*.br
  rm -f "${WEB_ROOT_D}"/doc/*.br

fi
