#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

exec_bin="${BASH_SOURCE%/*}/control-exec.sh"

check_deps check_rec_id "${exec_bin}" gen_amqp_url rabbitmq-cli-consumer

inst="${1-}"

check_rec_id "${inst}"

AMQP_URL="${AMQP_URL:-"$(gen_amqp_url)"}"
export AMQP_URL

rabbitmq-cli-consumer --pipe --no-declare --executable "${exec_bin}" \
  --strict-exit-code --queue-name "rec_ctrl.${inst}" --verbose --output \
  --no-datetime
