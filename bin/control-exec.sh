#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

REJECT=3

function cleanup() {

  exit_status=$?

  # Clear activity file which skips analyze service as long as we're busy
  [[ -f "${act_file-}" ]] && rm -f "${act_file}"

  exit "${exit_status}"

}

trap "exit" INT TERM
trap cleanup EXIT

# Maintain a terse event log (syslog tag cr-events)
function log() {
  ev_log "${inst-}" "${@}"
}

# desc: transmit commands to receiver
# parameters: rec_type (skyq|sky+hd),
#             host, rc_data (channel/space delimited commands),
#             rc_type (channel|commands), (optional) wake (true|false)
# echoes to stderr
# returns error on incorrect usage/failure if unreachable
function rec_tx_rc() {

  check_deps grep nc sky-remote-cli timeout

  # Functions
  check_deps l_sleep print verify_vars_set

  local rec_type ctrl_conn rc_type wake
  declare -a rc_data lcmds cmds tcmds # local scope

  rec_type="${1-}"
  ctrl_conn="${2-}"
  read -r -a rc_data <<<"${3-}"
  rc_type="${4-}"
  wake="${5:-false}"

  verify_vars_set ctrl_conn rc_data || return "${FAILURE}"

  if [[ "${rc_type}" == "channel" ]]; then

    timeout="10"

    if [[ "${rec_type}" == "skyq" ]]; then
      # home dismiss to exit possibly open menu (opens menu in top
      # level, then closes it). second dismiss to remove possible PIN
      # entry prompt
      lcmds=(home dismiss dismiss)
      tcmds=()
      # waking is necessary in some cases (power cycle)
      wake_cmds=(home dismiss)
    elif [[ "${rec_type}" == "sky+hd" ]]; then
      # sky closes possibly open menu and also removes PIN entry prompt
      lcmds=(sky)
      # do not perform select backup in case option "back-up to channel
      # guide" is enabled. OSD disappears fairly quickly anyway
      tcmds=(select)
      # waking is necessary if device is in standby. backup clears
      # message after auto-update
      wake_cmds=(backup sky)
    else
      print "invalid rec_type"
      return "${FAILURE}"
    fi

    # Split channel into individual commands
    mapfile -t cmds < <(grep --only-matching . <<<"${rc_data[0]}")

  elif [[ "${rc_type}" == "commands" ]]; then
    cmds=("${rc_data[@]}")
    timeout="20"
  else
    print "invalid rc_type"
    return "${FAILURE}"
  fi

  # Check if receiver is reachable
  if ping_receiver "${ctrl_conn}" command; then

    [[ "${wake}" == "true" ]] && {

      print "Waking: TX: ${wake_cmds[*]}"

      timeout --verbose 10s sky-remote-cli "${ctrl_conn}" \
        "${wake_cmds[@]}" || return "${FAILURE}"

      # l_sleep echoes to stderr that we're sleeping
      l_sleep 8
    }

    cmds=("${lcmds[@]}" "${cmds[@]}" "${tcmds[@]}")

    print "TX: ${cmds[*]}"
    # sky-remote-cli always exits 0. catching timeout is not reliable as
    # e.g., 8 successful commands take ~4s. if receiver is unreachable,
    # initial timeout is long, but subsequent timeouts for 8 commands is
    # ~7s
    timeout --verbose "${timeout}s" sky-remote-cli "${ctrl_conn}" \
      "${cmds[@]}" || return "${FAILURE}"

  else
    return "${FAILURE}"
  fi
}

check_deps cat jo jq rm snmpset ssh sshpass timeout touch wget

# Functions
check_deps check_bash_ver check_rec_id check_redis check_vars_pos_int \
  ev_log gen_redis_url get_control_url get_sid get_transport l_sleep \
  load_bash_builtin ping_receiver print set_pin verify_vars_set

# mapfile
check_bash_ver 4

# try to load builtins, ignore failure
load_bash_builtin rm sleep || :

# Timeout for wget/ssh to avoid blocking if a device hangs
TIMEOUT="${TIMEOUT:-"5"}"
# Show set PIN response message on failure. Requires curl ≥7.76.0
SHOW_PIN_ERR="${SHOW_PIN_ERR:-"true"}"

check_vars_pos_int TIMEOUT

payload="$(timeout 2s cat /dev/stdin)" || {
  print "stdin data input timed out"
  exit "${FAILURE}"
}

# Touch file for a skip condition on the analyze service
act_file="${act_file:-"${RUNTIME_DIRECTORY}/active"}"
touch "${act_file}"

REDIS_URL="${REDIS_URL:-"$(gen_redis_url)"}"

check_redis
redis_cmd=(redis-cli -u "${REDIS_URL-}" --no-auth-warning)

# Retrieve .data[] as space separated elements
# //"" Return empty string instead of false/null/nothing
pl_elem='.type//"",.action//"",.instance//"",(.data|join(" ")?)//"",'
pl_elem+='.requestor//""'
mapfile -t pl_val < <(jq -r -c "${pl_elem}" <<<"${payload}")

type="${pl_val[0]-}"
action="${pl_val[1]-}"
inst="${pl_val[2]-}"
data="${pl_val[3]-}"
requestor="${pl_val[4]-}"

check_rec_id "${inst}"

cfg="$("${redis_cmd[@]}" --raw GET "cfg:i${inst}")"
# //"" Return empty string instead of false/null
ctrl_conn="$(jq -r -c '.ctrl_conn//""' <<<"${cfg}")"
rec_type="$(jq -r -c '.rec_type//""' <<<"${cfg}")"

check_vars_set rec_type

rc_failure="false"
del_sid="false"

if [[ "${type}" == "remote" ]]; then

  check_vars_set ctrl_conn

  print "Event: '${type}'; Instance: '${inst}'; Data: '${data}';" \
    "Requestor: '${requestor}'"

  # multiple commands are space delimited
  [[ "${data}" == *" "* ]] && cmdtext="commands" || cmdtext="command"

  if rec_tx_rc "${rec_type}" "${ctrl_conn}" "${data}" commands; then

    log "Remote ${cmdtext}: ${data}"

  else

    log "Remote ${cmdtext} failed"
    rc_failure="true"

  fi

elif [[ "${type}" == "reset" ]]; then

  check_vars_set ctrl_conn

  print "Event: '${type}'; Instance: '${inst}'; Requestor: '${requestor}'"

  cfg_elem='.pdu_ctrl//"",.pdu_model//"",.pdu_outlet//"",.pdu_password//"",'
  cfg_elem+='.pdu_protocol//"",.pdu_host//"",.pdu_user//"",'
  cfg_elem+='.startup_time//""'
  mapfile -t cfg_val < <(jq -r -c "${cfg_elem}" <<<"${cfg}")

  pdu_ctrl="${cfg_val[0]-}"
  pdu_model="${cfg_val[1]-}"
  pdu_outlet="${cfg_val[2]-}"
  pdu_password="${cfg_val[3]-}"
  pdu_protocol="${cfg_val[4]-}"
  pdu_host="${cfg_val[5]-}"
  pdu_user="${cfg_val[6]-}"
  startup_time="${cfg_val[7]-}"
  cur_chan="$("${redis_cmd[@]}" --raw GET "cur_chan:i${inst}")"

  verify_vars_set cur_chan pdu_ctrl pdu_model pdu_outlet pdu_password \
    pdu_protocol pdu_host pdu_user startup_time || {
    print "Configuration failure. Possible data corruption"
    exit "${REJECT}"
  }

  [[ "${pdu_ctrl}" != "true" ]] && {
    print "pdu_ctrl is not enabled"
    exit "${REJECT}"
  }

  pdu_act="false"
  if [[ "${pdu_model}" == "netio" && "${pdu_protocol}" == "json" ]]; then

    # Action 2: Short OFF delay (restart)
    # https://www.netio-products.com/files/NETIO-M2M-API-Protocol-JSON.pdf
    req="$(jo "Outputs=[$(jo "ID=${pdu_outlet}" Action=2 Delay=5000)]")"

    # curl: (35) OpenSSL SSL_connect: SSL_ERROR_SYSCALL; hence wget.
    # Ignore cert errors since PDU can only generate self-signed certs
    # valid for one year
    if wget --timeout="${TIMEOUT}" --no-verbose -O/dev/null --tries=1 \
      --no-check-certificate --post-data="${req}" \
      --header="Content-Type: application/json" --user="${pdu_user}" \
      --password="${pdu_password}" "https://${pdu_host}/netio.json"; then

      pdu_act="true"

    fi

  elif [[ "${pdu_model}" == "netio" && "${pdu_protocol}" == "snmpv3" ]]; then

    # Integer value 2: Short OFF delay (restart).
    # Delay must be configured using web interface
    # https://www.netio-products.com/files/NETIO-M2M-API-Protocol-SNMP.pdf

    # netioOutputAction
    cmd="1.3.6.1.4.1.47952.1.1.1.5.${pdu_outlet}"

    if snmpset -v3 -l authPriv -u "${pdu_user}" \
      -a SHA -A "${pdu_password}" -x AES -X "${pdu_password}" \
      "${pdu_host}" "${cmd}" i 2; then

      pdu_act="true"

    fi

  elif [[ "${pdu_model}" == "apc-nmc" && "${pdu_protocol}" == "ssh" ]]; then

    # Set 5 seconds delay, then power cycle
    cmd="olRbootTime ${pdu_outlet} 5
    olReboot ${pdu_outlet}
    quit"

    SSHPASS="${pdu_password}"
    export SSHPASS

    # Command always returns exit code 255
    timeout --verbose 10s sshpass -e ssh -T -l"${pdu_user}" -c aes256-cbc \
      -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no \
      -o ConnectTimeout="${TIMEOUT}" "${pdu_host}" <<<"${cmd}" || :

    pdu_act="true"

  else

    print "Model/protocol not implemented"
    exit "${REJECT}"

  fi

  # On success, wait for the receiver to accept input, then change
  # channel
  if [[ "${pdu_act}" == "true" ]]; then

    print "Power cycle PDU outlet '${pdu_outlet}'"
    log "Power cycle"

    l_sleep "${startup_time}"

    if rec_tx_rc "${rec_type}" "${ctrl_conn}" "${cur_chan}" channel \
      true; then

      log "Channel set: ${cur_chan}"

    else

      log "Channel set failed"
      rc_failure="true"
      del_sid="true"

    fi

  else
    print "Power cycle failed"
  fi

elif [[ "${type}" == "channel" ]]; then

  [[ "${action}" != "store" ]] && check_vars_set ctrl_conn

  if [[ "${action}" =~ ^commit|store$ ]]; then

    print "Event: '${type}'; Instance: '${inst}'; Action: '${action}';" \
      "Data: '${data[0]}'; Requestor: '${requestor}'"
    key="cur_chan:i${inst}"

    print "SET '${key}' '${data[0]}'"

    "${redis_cmd[@]}" SET "${key}" "${data[0]}" >&-

    # 4 digit channel with leading zero is a radio channel.
    # Needed for analyze service
    chan_radio="false"
    [[ "${data[0]}" == "0"* ]] && chan_radio="true"

    key="cur_chan_radio:i${inst}"
    print "SET '${key}' '${chan_radio}'"

    "${redis_cmd[@]}" SET "${key}" "${chan_radio}" >&-

  fi

  if [[ "${action}" == "commit" ]]; then

    if rec_tx_rc "${rec_type}" "${ctrl_conn}" "${data[0]}" channel \
      true; then

      msg="Channel change"
      [[ "${requestor}" == "cr_scheduler" ]] && msg+=" (scheduled)"
      msg+=": ${data[0]}"

      log "${msg}"

    else

      log "Channel change failed"
      rc_failure="true"
      del_sid="true"

    fi

  elif [[ "${action}" == "reset" ]]; then

    print "Event: '${type}'; Instance: '${inst}'; Action: '${action}';" \
      "Requestor: '${requestor}'"

    backup_chan="$(jq -r -c '.backup_chan//""' <<<"${cfg}")"
    cur_chan="$("${redis_cmd[@]}" --raw GET "cur_chan:i${inst}")"

    verify_vars_set backup_chan cur_chan || {
      print "Configuration failure. Possible data corruption"
      exit "${REJECT}"
    }

    err="false"

    if rec_tx_rc "${rec_type}" "${ctrl_conn}" "${backup_chan}" channel \
      true; then

      msg="Channel reset"
      [[ "${requestor}" != "_analyze" ]] && msg+=" (manual)"
      msg+=": (${backup_chan}) ${cur_chan}"

      log "${msg}"

      l_sleep 5
      rec_tx_rc "${rec_type}" "${ctrl_conn}" "${cur_chan}" channel ||
        err="true"

    else
      err="true"
    fi

    if [[ "${err}" == "true" ]]; then

      log "Channel reset failed"

      rc_failure="true"
      del_sid="true"

    fi

  elif [[ "${action}" == "set" ]]; then

    print "Event: '${type}'; Instance: '${inst}'; Action: '${action}'" \
      "Requestor: '${requestor}'"

    cur_chan="$("${redis_cmd[@]}" --raw GET "cur_chan:i${inst}")"

    verify_vars_set cur_chan || {
      print "Configuration failure. Possible data corruption"
      exit "${REJECT}"
    }

    if rec_tx_rc "${rec_type}" "${ctrl_conn}" "${cur_chan}" channel \
      true; then

      log "Channel set: ${cur_chan}"

    else

      log "Channel set failed"
      rc_failure="true"
      del_sid="true"

    fi

  elif [[ "${action}" != "store" ]]; then

    print "Event: '${type}'; Invalid action '${action}';" \
      "Requestor: '${requestor}'"
    exit "${REJECT}"

  fi

else

  print "Invalid event type '${type}'"
  exit "${REJECT}"

fi

verify_chan="$(jq -r -c '.verify_chan//""' <<<"${cfg}")"

if [[ "${rc_failure}" == "true" ]]; then

  print "Remote control command failed"

elif [[ "${verify_chan}" == "true" ]] && ping_receiver "${ctrl_conn}" soap &&
  { [[ "${type}" == "reset" ]] ||
    [[ "${type}" == "channel" && "${action}" =~ ^commit|reset|set$ ]]; }; then

  if [[ "${type}" == "reset" ]]; then
    # SID needs a bit more time to update correctly after a power cycle
    l_sleep 10
  else
    # Less than 3 seconds has a high chance of recording the SID of the
    # previous (backup) channel
    l_sleep 5
  fi

  ssdp_url="$("${redis_cmd[@]}" --raw GET "ssdp_url_ctrl:i${inst}")"

  if [[ -n "${ssdp_url}" ]]; then

    print "Device/model information:"
    get_rec_details "${ssdp_url}" || :

    if ! control_url="$(get_control_url "${ssdp_url}")"; then

      print "No control URL returned"

    fi

  else

    print "SSDP URL not set"

  fi

  [[ -n "${control_url-}" ]] && {

    print "Checking PIN lock"

    if transport="$(get_transport "${ssdp_url}" "${control_url}")"; then

      print "Transport state: '${transport}'"

    else

      print "No Transport info returned"

    fi

    # Transport state for a PIN locked channel is OK if checking
    # immediately after channel change, must wait 5 seconds (Sky+HD)
    if [[ "${transport}" == "ERROR_PIN_REQUIRED" ]]; then

      pin="$(jq -r -c '.pin//""' <<<"${cfg}")"

      if [[ -z "${pin}" ]]; then

        print "WARNING: PIN not configured"

      elif set_pin "${ssdp_url}" "${control_url}" "${pin}"; then

        print "PIN entry successful"

        log "PIN entry"

      else

        print "WARNING: PIN entry failed"

      fi

    fi

    # Transport: ERROR_SIGNAL on target channel but not backup channel
    # returns backup channel SID
    if [[ "${transport}" =~ ^(OK|ERROR_PIN_REQUIRED)$ ]] &&
      cur_sid="$(get_sid "${ssdp_url}" "${control_url}")"; then

      print "SET 'target_sid:i${inst}' '${cur_sid}'"

      "${redis_cmd[@]}" SET "target_sid:i${inst}" "${cur_sid}" >&-

    fi

  }

  [[ -z "${cur_sid-}" ]] && del_sid="true"

fi

# Don't keep previous SID if channel command failed or no SID was
# returned
if { [[ "${verify_chan}" != "true" ]] || [[ "${del_sid}" == "true" ]]; } &&
  [[ "$("${redis_cmd[@]}" --raw EXISTS "target_sid:i${inst}")" == "1" ]]; then

  print "DEL 'target_sid:i${inst}'"
  "${redis_cmd[@]}" DEL "target_sid:i${inst}" >&-

fi

exit "${SUCCESS}"
