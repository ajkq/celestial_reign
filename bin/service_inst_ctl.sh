#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

check_deps systemctl xargs

# Functions
check_deps exe print

function usage() {
  cat <<EOF >&2
Usage:
${0} <Action> <Component> <Instance>
Action:     start|stop|restart
Component:  control|analyze|capture|discover
Instance:   stop:           *|N
            start|restart:  N
EOF
}

if ((EUID != 0)); then
  print "Must be run as root"
  exit "${CONFIG}"
fi

action="${1-}"
component="${2-}"
instance="${3-}"

UNIT_ROOT="${UNIT_ROOT:-"${PWD}/systemd"}"

[[ "${action}" =~ ^(start|stop|restart)$ ]] || {
  print "Action validation failed\n"
  usage
  exit "${FAILURE}"
}

[[ "${component}" =~ ^(control|analyze|capture|discover)$ ]] || {
  print "Component validation failed\n"
  usage
  exit "${FAILURE}"
}

{ [[ "${instance}" == "*" && "${action}" == "stop" ]] ||
  [[ "${instance}" =~ ^[1-9][0-9]*$ ]]; } || {
  print "Instance validation failed\n"
  usage
  exit "${FAILURE}"
}

if [[ "${action}" == "stop" ]]; then

  if [[ "${instance}" == "*" ]]; then

    # match timer and service units
    unit="cr-${component}@*"
    # systemctl instance globbing doesn't work for disable
    exec=(systemctl show "${unit}" -p Id --value \|
      xargs -r systemctl disable --now)
    print "${exec[*]}"
    eval "${exec[*]}"

  else

    [[ "${component}" == "analyze" ]] && {
      unit="cr-${component}@${instance}.timer"
      exe systemctl disable --now "${unit}"
    }

    unit="cr-${component}@${instance}.service"
    exe systemctl disable --now "${unit}"

  fi

elif [[ "${action}" == "start" ]]; then

  [[ "${component}" == "analyze" ]] && {
    unit="${UNIT_ROOT}/cr-${component}@.timer"
    exe systemctl link "${unit}"
  }

  unit="${UNIT_ROOT}/cr-${component}@.service"
  exe systemctl link "${unit}"

  if [[ "${component}" == "analyze" ]]; then
    unit="cr-${component}@${instance}.timer"
  else
    unit="cr-${component}@${instance}.service"
  fi

  exe systemctl enable --now "${unit}"

elif [[ "${action}" == "restart" ]]; then

  unit="cr-${component}@${instance}.service"
  exe systemctl restart "${unit}"

fi

exit "${SUCCESS}"
