#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

check_deps print journalctl

unit="${1-}"

if [[ -z "${1-}" ]]; then
  print "Helper to show journal for specific unit. Includes unit name when" \
    "instance name glob detected"
  print "Usage: ${0} <Unit>"
  exit "${INVALIDARGUMENT}"
fi

args=(--pager-end --no-hostname)
[[ "${unit}" == *"@*"* ]] && args+=(--output=with-unit)

journalctl "${args[@]}" --unit="${unit}"
