#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

check_deps cat mktemp rm zip

# Functions
check_deps check_gnu_grep convert_unix_tstamp_iso print

check_gnu_grep

function cleanup() {

  exit_status=$?

  [[ -d "${tmp_dir-}" ]] && rm -rf "${tmp_dir}"

  exit "${exit_status}"

}

trap "exit" INT TERM
trap cleanup EXIT

if ! tmp_dir="$(mktemp -d)"; then
  print "Failed to create temporary directory"
  exit "${FAILURE}"
fi

source_file="${1-}"

# strip path
tmp_file="${source_file##*/}"
# strip extension
tmp_file="${tmp_file%%.*}"
name="$(sanitize_text "${tmp_file}")"
dest_file="${tmp_file}.kmz"

tmp_file="${tmp_file}.kml"
tmp_file="${tmp_dir}/${tmp_file}"

[[ -f "${source_file}" ]] || {
  print "Usage: ${0} <File>"
  print "kmz output file saved in working directory\n"
  exit "${USAGE}"
}

cat >"${tmp_file}" <<EOF
<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
<Document>
<name>${name}</name>
<Style id="red"><IconStyle><color>ff0000ff</color></IconStyle></Style>
<Style id="green"><IconStyle><color>ff00ff00</color></IconStyle></Style>
<Style id="orange"><IconStyle><color>ff00a5ff</color></IconStyle></Style>
EOF

# -90 to 90 with optional decimals
regex="(?# lat )(?:-[1-9]|-?[1-8][0-9]|-?90|[0-9])(?:\.[0-9]+)?,"
# -180 to 180 with optional decimals
regex+="(?# lon )(?:-[1-9]|-?[1-9][0-9]|-?1[0-7][0-9]|-?180|[0-9])"
regex+="(?:\.[0-9]+)?,"
regex+="(?# state )(?:0|1|2),"
regex+="(?# event )(?:A|V|S),"
regex+="(?# rec )(?:[0-9]+),"
regex+="(?# epoch )(?:[0-9]+)"

i=0

while IFS= read -r record; do

  ((++i))

  IFS=',' read -r lat lon state event rec epoch <<<"${record}"
  datetime="$(TZ="UTC0" convert_unix_tstamp_iso "${epoch}")"

  record="<Placemark>"

  if [[ "${event}" == "A" ]]; then
    event_long="audio"
  elif [[ "${event}" == "V" ]]; then
    event_long="video"
  elif [[ "${event}" == "S" ]]; then
    event_long="signal"
  else
    event_long="${event}"
  fi

  if [[ "${rec}" == "0" ]]; then
    rec_short=
    rec_long=
  else
    rec_short=" ${event} ${rec}"
    rec_long=" ${event_long} rec. ${rec}"
  fi

  if [[ "${state}" == "0" ]]; then
    record+="<name>L${rec_short}</name>"
    record+="<styleUrl>#red</styleUrl>"
    record+="<description>loss${rec_long}</description>"
  elif [[ "${state}" == "1" ]]; then
    record+="<name>R${rec_short}</name>"
    record+="<styleUrl>#green</styleUrl>"
    record+="<description>recover${rec_long}</description>"
  elif [[ "${state}" == "2" ]]; then
    record+="<name>C${rec_short}</name>"
    record+="<styleUrl>#orange</styleUrl>"
    record+="<description>clear${rec_long}</description>"
  fi

  record+="<Point><coordinates>${lon},${lat}</coordinates></Point>"
  record+="<TimeStamp><when>${datetime}</when></TimeStamp>"
  record+="</Placemark>"

  echo "${record}" >>"${tmp_file}"

done < <(grep --only-matching --perl-regexp "^${regex}$" "${source_file}")

cat >>"${tmp_file}" <<"EOF"
</Document>
</kml>
EOF

if ((i == 0)); then

  print "Source file contains no valid input"

else

  zip -q -9 -j "${dest_file}" "${tmp_file}"

fi
