#!/usr/bin/env python3
import os
import errno
import sys
import signal
import time
import distutils.util

if sys.version_info < (3, 8):
  print("Python ≥3.8 required")
  sys.exit(5)


def rm_f(file_path):
  try:
    os.remove(file_path)
  except OSError as e:
    # Re-raise errors except no such file or directory
    if e.errno != errno.ENOENT:
      raise


alarm_f = os.environ.get('ALARM_F', 'var/alarm')
mode = os.environ.get('MODE', 'nc')
pin = os.environ.get('BOARD_PIN', 'D4')
interval = os.environ.get('INTERVAL_S', 5)
pos_log = os.environ.get('POS_LOG', 'false')
pos_log_f = os.environ.get('POS_LOG_F', 'log/detect-coord-alarm.log')
pos_f = os.environ.get('POS_F', 'tmp/latlon')

try:
  import digitalio
  import board
except:
  if os.path.isfile(alarm_f):
    rm_f(alarm_f)
    print('Exit: alarm cleared')
  raise


def s_f(file_path):
  return os.path.exists(file_path) and os.stat(file_path).st_size > 0


def log_pos(pos_f, pos_log_f, state):
  with open(pos_f, 'r') as file:
    coord = file.read().replace('\n', '')
    record = coord + ',' + state + ',S,0,' + str(int(time.time()))
  if coord:
    with open(pos_log_f, "a") as file:
      file.write(record + '\n')


def cleanup(sig, frame):
  if os.path.isfile(alarm_f):
    rm_f(alarm_f)
    print('Exit: alarm cleared')

    if pos_log and s_f(pos_f):
      log_pos(pos_f, pos_log_f, '2')

  print('Stopped')
  sys.exit(0)


signal.signal(signal.SIGINT, cleanup)
signal.signal(signal.SIGTERM, cleanup)

vars = [alarm_f, mode, pin, interval, pos_log, pos_log_f, pos_f]
for var in vars:
  if not bool(var):
    print(f"{var=} cannot be empty")
    sys.exit(78)

try:
  pos_log = bool(distutils.util.strtobool(pos_log))
except ValueError:
  print("POS_LOG not boolean")
  sys.exit(78)

try:
  interval = float(interval)
except ValueError:
  print("INTERVAL not an integer")
  sys.exit(78)

if mode.lower() == "nc":
  logic = False
elif mode.lower() == "no":
  logic = True
else:
  print('Invalid MODE')
  sys.exit(78)

gpio = digitalio.DigitalInOut(getattr(board, pin))
gpio.direction = digitalio.Direction.INPUT

print('Running')

if os.path.isfile(alarm_f):
  print("Init: alarm state file present")
  prev_state = False
else:
  prev_state = True

while True:

  if logic:
    state = gpio.value
  else:
    state = not gpio.value

  if state and not prev_state:
    rm_f(alarm_f)
    print("Alarm cleared")

    if pos_log and s_f(pos_f):
      log_pos(pos_f, pos_log_f, '1')

  elif prev_state and not state:
    open(alarm_f, 'a').close()
    print("Alarm triggered")

    if pos_log and s_f(pos_f):
      log_pos(pos_f, pos_log_f, '0')

  prev_state = state

  time.sleep(interval)
