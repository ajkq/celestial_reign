#!/usr/bin/env bash

# shellcheck source=shared-functions.sh
source "${BASH_SOURCE%/*}/shared-functions.sh"

set -o errexit
set -o nounset

check_deps grep psql

# Functions
check_deps print

PGDATABASE="${PGDATABASE:-"celestial_reign"}"
PGUSER="${PGUSER:-"cr_scheduler"}"
export PGDATABASE PGUSER

# show psql output only if changes were performed, or on error
if IFS= result="$(psql --no-psqlrc --no-password \
  --command='SELECT * FROM app.f_process_schedule();' 2>&1)"; then

  if ! grep --ignore-case --quiet \
    --fixed-strings "(0 rows)" <<<"${result}"; then
    print "${result}"
  fi

else
  print "${result}"
fi
