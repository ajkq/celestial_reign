# Celestial reign

Remote management and monitoring solution for Sky UK receivers, designed
for River vessels.

IMPORTANT: Maintainer needed.

This solution was developed for a defunct luxury River cruise line. Due
to the bankruptcy, I changed jobs in 2022 and haven't had professional
exposure to Sky UK IPTV headends since. I am looking for a maintainer.

## Features

* Scheduled and immediate channel changes
* Global list of assignable channels
* Per-receiver channel denylist
* Lock receiver to a single channel
* Automatically enter PIN for PIN-locked programs
* Ensure the correct channel is tuned
* Automatic channel reset after A/V loss, standby, software update, with
  rate-limiting if the issue persists. Skip based on geofencing rules or
  antenna control unit's signal state
* Trigger script on no stream or stream in standard definition for
  network sources using separate initial and re-execution delays for
  both conditions
* Manually issued power cycle with timed channel reset using a
  compatible Power Distribution Unit
* Email alerts on A/V and network connectivity loss
* Remote control commands
* Preview images updated twice per minute
* User, operator, and admin permission levels

## Documentation

HTML files for on-screen reading and PDF files for printing are located
[here](share/html/doc), generated from Markdown [sources](documentation).

## License

Copyright © 2021-2024 Andreas Kohout

Licensed under the EUPL-1.2 or later
