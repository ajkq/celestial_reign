\set ON_ERROR_STOP on

\connect celestial_reign

SET ROLE cr_app;

DO $$
BEGIN
  PERFORM app.f_config_defaults();
  PERFORM app.f_catalog_def_chan();
END
$$;
