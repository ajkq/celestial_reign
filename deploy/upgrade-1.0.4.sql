\set ON_ERROR_STOP on

\connect celestial_reign

SET ROLE cr_app;

SELECT app.f_version() >= 10001 AS incompatible
\gset
\if :incompatible
  \echo 'Incompatible version'
  \quit
\endif

CREATE OR REPLACE FUNCTION app.f_catalog_def_chan()
  RETURNS TABLE (LIKE app.chan_catalog)
  LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  INSERT INTO app.chan_catalog (channel)
  VALUES (UNNEST(ARRAY[
    401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 415, 416, 418,
    421, 422, 425, 426, 427, 433, 437, 858, 859, 860, 861, 862, 863, 864, 865,
    866, 867, 868, 869, 872, 873, 875, 877
  ]))
  ON CONFLICT (channel) DO NOTHING
  RETURNING chan_catalog_id, channel;
END;
$$;

COMMENT ON FUNCTION app.f_catalog_def_chan IS
'Add default channels to catalog';

CREATE OR REPLACE FUNCTION app.f_version()
  RETURNS integer
  LANGUAGE sql
  IMMUTABLE PARALLEL SAFE
  AS 'SELECT 10001';

COMMENT ON FUNCTION app.f_version IS 'Version';
