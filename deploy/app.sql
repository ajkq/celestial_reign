\set ON_ERROR_STOP on

-- JSONPath queries require PostgreSQL 12
SELECT :SERVER_VERSION_NUM < 120000 AS incompatible
\gset
\if :incompatible
  \echo 'PostgreSQL 12 or later required'
  \quit
\endif

-- Verify amqp extension is installed and loaded
SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_available_extensions
                    WHERE name = 'amqp') AS missing_ext
\gset
\if :missing_ext
  \echo 'amqp extension not available'
  \quit
\endif

SELECT EXISTS ( SELECT FROM  pg_catalog.pg_database
                WHERE datname='celestial_reign') AS db_exists
\gset
\if :db_exists
  \echo 'Database already exists'
  \quit
\endif

SET search_path = pg_catalog;

DROP ROLE IF EXISTS cr;
CREATE ROLE cr NOINHERIT;

CREATE DATABASE celestial_reign WITH ENCODING = 'UTF8'
LC_COLLATE = 'en_US.UTF-8' LC_CTYPE = 'en_US.UTF-8' OWNER cr;
COMMENT ON DATABASE celestial_reign IS 'Celestial reign';

REVOKE TEMPORARY ON DATABASE celestial_reign FROM PUBLIC;

ALTER DATABASE celestial_reign
SET timezone TO 'Europe/London';

\connect celestial_reign

DROP ROLE IF EXISTS cr_app;
CREATE ROLE cr_app NOINHERIT;
ALTER DEFAULT PRIVILEGES FOR USER cr_app REVOKE EXECUTE ON ROUTINES FROM PUBLIC;

CREATE EXTENSION amqp;
REVOKE EXECUTE ON ALL ROUTINES IN SCHEMA amqp FROM PUBLIC;
CREATE EXTENSION plperlu;

CREATE SCHEMA app AUTHORIZATION cr_app;
COMMENT ON SCHEMA app IS 'Application';

DROP SCHEMA public;

SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_roles
                    WHERE rolname = 'cr_g_super' ) AS user_not_exists
\gset
\if :user_not_exists
  CREATE ROLE cr_g_super INHERIT;
\endif

GRANT cr to cr_g_super;
GRANT cr_app to cr_g_super;

GRANT USAGE ON SCHEMA amqp TO cr_g_super;
GRANT USAGE ON SCHEMA amqp TO cr_app;

GRANT EXECUTE ON FUNCTION amqp.publish TO cr_app;
GRANT SELECT ON TABLE amqp.broker TO cr_app;
GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA amqp TO cr_g_super;
GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA amqp TO cr_g_super;
GRANT ALL PRIVILEGES ON ALL FUNCTIONS IN SCHEMA amqp TO cr_g_super;

CREATE OR REPLACE FUNCTION app.f_check_email(_email text)
  RETURNS boolean
  LANGUAGE plperlu
  RETURNS NULL ON NULL INPUT
  IMMUTABLE
  LEAKPROOF
AS $$
  use Email::Valid;
  Email::Valid->address($_[0]) ? return true : return false;
$$;

COMMENT ON FUNCTION app.f_check_email IS 'Validate email address';

ALTER FUNCTION app.f_check_email OWNER TO cr_app;

GRANT EXECUTE ON FUNCTION app.f_check_email TO PUBLIC;

CREATE OR REPLACE FUNCTION app.f_check_hostname(_host text)
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
  IMMUTABLE
  LEAKPROOF
AS $$
DECLARE
  _labels TEXT[];
  _label TEXT;
BEGIN
  IF RIGHT(_host, 1) = '.' THEN
    _host = left(_host, (LENGTH(_host) - 1));
  END IF;

  IF LENGTH(_host) > 253 THEN
    RETURN FALSE;
  END IF;

  _labels = STRING_TO_ARRAY(_host, '.');

  IF _labels[ARRAY_LENGTH(_labels, 1)] ~ '^[0-9]+$' THEN
    RETURN FALSE;
  END IF;

  FOREACH _label IN ARRAY _labels
  LOOP
    IF _label !~* '^(?!-)[a-z0-9-]{1,63}(?<!-)$' THEN
      RETURN FALSE;
    END IF;
  END LOOP;

  RETURN TRUE;
END;
$$;

COMMENT ON FUNCTION app.f_check_hostname IS 'Validate hostname/FQDN';

ALTER FUNCTION app.f_check_hostname OWNER TO cr_app;

GRANT EXECUTE ON FUNCTION app.f_check_hostname TO PUBLIC;

SET ROLE cr_app;

CREATE OR REPLACE FUNCTION app.f_check_type(_type text, _value text)
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
  STABLE
AS $$
DECLARE
  _i integer;
BEGIN
  EXECUTE format('SELECT CAST(%L AS %s)', _value, _type);
  GET DIAGNOSTICS _i = ROW_COUNT;
  IF _i > 0 THEN
    RETURN TRUE;
  END IF;
  -- Do not catch undefined_object, e.g., non-existent type
  EXCEPTION WHEN integrity_constraint_violation OR data_exception THEN

  RETURN FALSE;
END;
$$;

COMMENT ON FUNCTION app.f_check_type IS 'Validate data type (accept NULL)';

GRANT EXECUTE ON FUNCTION app.f_check_type TO PUBLIC;

CREATE OR REPLACE FUNCTION app.f_require_type(_type text, _value text)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
AS $$
DECLARE
  _i integer;
BEGIN
  IF _value IS NULL THEN
    RETURN FALSE;
  END IF;
  EXECUTE format('SELECT CAST(%L AS %s)', _value, _type);
  GET DIAGNOSTICS _i = ROW_COUNT;
  IF _i > 0 THEN
    RETURN TRUE;
  END IF;
  EXCEPTION WHEN integrity_constraint_violation OR data_exception THEN

  RETURN FALSE;
END;
$$;

COMMENT ON FUNCTION app.f_require_type IS 'Validate data type (reject NULL)';

GRANT EXECUTE ON FUNCTION app.f_require_type TO PUBLIC;

CREATE OR REPLACE FUNCTION app.f_convert_interval_min(_interval interval)
  RETURNS real
  LANGUAGE sql
  RETURNS NULL ON NULL INPUT
  IMMUTABLE
AS $$
  -- Division produces double precision, ROUND with precision parameter
  -- requires numeric; real removes trailing decimal zero
  SELECT ROUND( (EXTRACT(EPOCH FROM _interval)/60)::numeric, 1 );
$$;

COMMENT ON FUNCTION app.f_convert_interval_min IS
'Convert interval to minutes';

GRANT EXECUTE ON FUNCTION app.f_convert_interval_min TO PUBLIC;

CREATE DOMAIN app.email AS text
CHECK (app.f_check_email(VALUE));

COMMENT ON DOMAIN app.email IS
'Email address';

CREATE DOMAIN app.channel_nr AS TEXT
CHECK (VALUE ~ '^0?([1-8][0-9]{2}|9[0-8][0-9]|99[0-9])$');

COMMENT ON DOMAIN app.channel_nr IS
'Channel numbers 100-999 with one optional leading zero';

CREATE DOMAIN app.pos_interval AS interval(0)
CHECK (VALUE >='0'::interval);

COMMENT ON DOMAIN app.pos_interval IS 'Positive interval(0)';

-- inet accepts CIDR mask
CREATE DOMAIN app.ip AS text
CHECK (VALUE = host(VALUE::inet));

COMMENT ON DOMAIN app.ip IS 'IP address';

CREATE DOMAIN app.ipv4 AS app.ip
CHECK (family(VALUE::inet) = 4);

COMMENT ON DOMAIN app.ipv4 IS 'IPv4 address';

CREATE DOMAIN app.host AS text
CHECK (app.f_check_type('app.ip',VALUE) OR app.f_check_hostname(VALUE));

COMMENT ON DOMAIN app.host IS 'IP/hostname/FQDN';

CREATE DOMAIN app.host_ipv4 AS text
CHECK (app.f_check_type('app.ipv4',VALUE) OR app.f_check_hostname(VALUE));

COMMENT ON DOMAIN app.host_ipv4 IS 'IPv4/hostname/FQDN';

CREATE DOMAIN app.unpriv_port AS integer
CHECK ((VALUE > 1023) AND (VALUE <= 65535));

COMMENT ON DOMAIN app.unpriv_port IS 'Unprivileged TCP/UDP port';

CREATE DOMAIN app.file_path AS text
CHECK (VALUE ~ '[^\0\n]+');

COMMENT ON DOMAIN app.file_path IS 'UNIX file path (disallowing newlines)';

CREATE DOMAIN app.pos_integer AS integer
CHECK (VALUE > 0);

COMMENT ON DOMAIN app.pos_integer IS 'Positive integer';

CREATE DOMAIN app.pos_smallint AS smallint
CHECK (VALUE > 0);

COMMENT ON DOMAIN app.pos_smallint IS 'Positive smallint';

CREATE DOMAIN app.pin AS text
CHECK (VALUE ~ '^\d{4}$');

COMMENT ON DOMAIN app.pin IS '4 digit PIN';

CREATE DOMAIN app.vid_format AS text
CHECK (VALUE ~ '^[a-z0-9_]+$');

COMMENT ON DOMAIN app.vid_format IS 'ffmpeg video format';

CREATE DOMAIN app.vid_size AS text
CHECK (VALUE ~ '^\d+x\d+|[a-z0-9]+$');

COMMENT ON DOMAIN app.vid_size IS 'ffmpeg video size';

CREATE DOMAIN app.vid_framerate AS text
CHECK (VALUE ~ '^[a-z-]+|\d+(\/\d+)?$');

COMMENT ON DOMAIN app.vid_framerate IS 'ffmpeg video framerate';

CREATE DOMAIN app.vid_src_conn AS jsonb CHECK (
  (
    VALUE ?& array['type','udp']
    AND NOT VALUE ?| array['alsa', 'v4l2']
    AND (VALUE->>'type') = 'network'
    -- localaddr is optional, validate if present
    AND app.f_check_type('app.ip',VALUE->'udp'->>'localaddr')
    AND app.f_require_type('app.host',VALUE->'udp'->>'host')
    AND app.f_require_type('app.unpriv_port',VALUE->'udp'->>'port')
  )
  OR
  (
    -- alsa or v4l2 section is required
    (
      NOT VALUE ? 'udp'
      AND (VALUE->>'type') = 'local'
    ) AND (
      VALUE ?& array['type', 'alsa']
      AND app.f_require_type('app.file_path',VALUE->'alsa'->>'device')
      AND app.f_require_type('app.pos_integer',VALUE->'alsa'->>'sample_rate')
      AND app.f_require_type('app.pos_integer',VALUE->'alsa'->>'channels')
    ) OR (
      VALUE ?& array['type', 'v4l2']
      AND app.f_require_type('app.file_path',VALUE->'v4l2'->>'device')
      AND app.f_require_type('app.vid_format',VALUE->'v4l2'->>'input_format')
      AND app.f_require_type('app.vid_framerate',VALUE->'v4l2'->>'framerate')
      AND app.f_require_type('app.vid_size',VALUE->'v4l2'->>'video_size')
    )
  )
  OR VALUE IS NULL
);

COMMENT ON DOMAIN app.vid_src_conn IS 'Video source connection';

-- SECURITY DEFINER avoids necessity of SELECT privileges on table amqp.broker
-- for regular users
CREATE OR REPLACE FUNCTION app.f_amqp_pub(_routing_key text, _payload jsonb)
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
  IMMUTABLE
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _broker_id integer := 1;
  _exchange text := 'celestial_reign';
  -- persistent
  _delivery_mode integer := 2;
  _content_type text := 'json';
BEGIN
  _payload := JSONB_SET(_payload, '{requestor}', TO_JSONB(SESSION_USER), true);

  RETURN amqp.publish(_broker_id, _exchange, _routing_key, _payload::text,
  _delivery_mode, _content_type);
END;
$$;

COMMENT ON FUNCTION app.f_amqp_pub IS 'Publish persistent JSON '
'AMQP message to broker 1, exchange celestial_reign';

CREATE OR REPLACE FUNCTION app.trf_sync_comp_mgmt_svc()
  RETURNS TRIGGER
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _svc text;
  _payload jsonb;
  _stop text[] DEFAULT '{}';
  _start text[] DEFAULT '{}';
BEGIN

  IF (TG_OP = 'TRUNCATE') THEN
    _stop := ARRAY ['alert', 'coverage', 'preview'];

    FOREACH _svc IN ARRAY _stop
    LOOP
      SELECT JSONB_BUILD_OBJECT('type', 'svc_sync', 'action', 'stop',
                                'data', _svc)
      INTO _payload;
      IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
        RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
      END IF;
    END LOOP;

  ELSE
      IF (TG_OP = 'INSERT') THEN

        IF (NEW.alert) THEN
          _start := array_append(_start, 'alert');
        END IF;

        IF (NEW.coverage) THEN
          _start := array_append(_start, 'coverage');
        END IF;

        IF (NEW.preview) THEN
          _start := array_append(_start, 'preview');
        END IF;

      ELSIF (TG_OP = 'UPDATE') THEN

        IF ( (NEW.alert) AND (NOT OLD.alert) ) THEN
          _start := array_append(_start, 'alert');
        END IF;

        IF ( (NEW.coverage) AND (NOT OLD.coverage) ) THEN
          _start := array_append(_start, 'coverage');
        END IF;

        IF ( (NEW.preview) AND (NOT OLD.preview) ) THEN
          _start := array_append(_start, 'preview');
        END IF;

        IF ( (NOT NEW.alert) AND (OLD.alert) ) THEN
          _stop := array_append(_stop, 'alert');
        END IF;

        IF ( (NOT NEW.coverage) AND (OLD.coverage) ) THEN
          _stop := array_append(_stop, 'coverage');
        END IF;

        IF ( (NOT NEW.preview) AND (OLD.preview) ) THEN
          _stop := array_append(_stop, 'preview');
        END IF;

      END IF;

      IF (TG_OP = 'DELETE') THEN

        IF (OLD.alert) THEN
          _stop := array_append(_stop, 'alert');
        END IF;

        IF (OLD.coverage) THEN
          _stop := array_append(_stop, 'coverage');
        END IF;

        IF (OLD.preview) THEN
          _stop := array_append(_stop, 'preview');
        END IF;

      END IF;

      FOREACH _svc IN ARRAY _stop
      LOOP
        SELECT JSONB_BUILD_OBJECT('type', 'svc_sync', 'action', 'stop',
                                  'data', _svc)
        INTO _payload;
        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;
      END LOOP;

      FOREACH _svc IN ARRAY _start
      LOOP
        SELECT JSONB_BUILD_OBJECT('type', 'svc_sync', 'action', 'start',
                                  'data', _svc)
        INTO _payload;
        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;
      END LOOP;

  END IF;

  RETURN NULL;
END $$;

COMMENT ON FUNCTION app.trf_sync_comp_mgmt_svc IS
'Trigger: AMQP comp_mgmt svc_sync (cr-comp-mgmt.service)';

CREATE OR REPLACE FUNCTION app.trf_sync_comp_mgmt_svc_inst()
  RETURNS TRIGGER
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _svc text;
  _payload jsonb;
  _stop text[] DEFAULT '{}';
  _start text[] DEFAULT '{}';
  _restart text[] DEFAULT '{}';
BEGIN

  IF (TG_OP = 'TRUNCATE') THEN
    _stop := ARRAY ['control', 'capture', 'analyze', 'discover'];

    FOREACH _svc IN ARRAY _stop
    LOOP
      SELECT JSONB_BUILD_OBJECT('type', 'svc_inst_sync', 'action', 'stop',
                                'instance', '*', 'data', _svc)
      INTO _payload;
      IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
        RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
      END IF;
    END LOOP;

  ELSE
      IF (TG_OP = 'INSERT') THEN

        _start := array_append(_start, 'control');

        IF (NEW.vid_analyze) THEN
          _start := array_append(_start, 'capture');
        END IF;

        IF (NEW.verify_chan) THEN
          _start := array_append(_start, 'discover');
        END IF;

        IF (NEW.vid_analyze OR NEW.verify_chan) THEN
          _start := array_append(_start, 'analyze');
        END IF;

      ELSIF (TG_OP = 'UPDATE') THEN

        IF (OLD.rec_id <> NEW.rec_id) THEN

          _start := array_append(_start, 'control');

          IF (NEW.vid_analyze) THEN
            _start := array_append(_start, 'capture');
          END IF;

          IF (NEW.verify_chan) THEN
            _start := array_append(_start, 'discover');
          END IF;

          IF ( (NEW.vid_analyze) OR (NEW.verify_chan) ) THEN
            _start := array_append(_start, 'analyze');
          END IF;

        ELSE

          IF ( (NEW.vid_analyze) AND (NOT OLD.vid_analyze) ) THEN
            _start := array_append(_start, 'capture');
          ELSIF ( (NEW.vid_analyze)
                  AND (OLD.vid_src_conn <> NEW.vid_src_conn) ) THEN
            _restart := array_append(_restart, 'capture');
          END IF;

          IF ( (NEW.verify_chan) AND (NOT OLD.verify_chan) ) THEN
            _start := array_append(_start, 'discover');
          ELSIF ( (NEW.verify_chan)
                  AND (OLD.ctrl_conn <> NEW.ctrl_conn) ) THEN
            _restart := array_append(_restart, 'discover');
          END IF;

          IF  ( (     (NEW.vid_analyze) AND (NOT OLD.vid_analyze)
                  AND (NOT NEW.verify_chan)
                ) OR
                (     (NEW.verify_chan) AND (NOT OLD.verify_chan)
                  AND (NOT NEW.vid_analyze)
                ) OR
                (     (NEW.verify_chan) AND (NOT OLD.verify_chan)
                  AND (NEW.vid_analyze) AND (NOT OLD.vid_analyze)
                ) ) THEN
            _start := array_append(_start, 'analyze');
          END IF;

          IF ( (NOT NEW.vid_analyze) AND (OLD.vid_analyze) ) THEN
            _stop := array_append(_stop, 'capture');
          END IF;

          IF ( (NOT NEW.verify_chan) AND (OLD.verify_chan) ) THEN
            _stop := array_append(_stop, 'discover');
          END IF;

          IF (  ( (NOT NEW.vid_analyze) AND (NOT NEW.verify_chan) )
                AND ( (OLD.vid_analyze) OR (OLD.verify_chan) ) ) THEN
            _stop := array_append(_stop, 'analyze');
          END IF;

        END IF;

      END IF;

      IF (  (TG_OP = 'DELETE') OR
            ( (TG_OP = 'UPDATE') AND (OLD.rec_id <> NEW.rec_id) ) ) THEN

        _stop := array_append(_stop, 'control');

        IF (OLD.vid_analyze) THEN
          _stop := array_append(_stop, 'capture');
        END IF;

        IF (OLD.verify_chan) THEN
          _stop := array_append(_stop, 'discover');
        END IF;

        IF (OLD.vid_analyze OR OLD.verify_chan) THEN
          _stop := array_append(_stop, 'analyze');
        END IF;

      END IF;

      FOREACH _svc IN ARRAY _stop
      LOOP
        SELECT JSONB_BUILD_OBJECT('type', 'svc_inst_sync', 'action', 'stop',
                                  'instance', OLD.rec_id, 'data', _svc)
        INTO _payload;
        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;
      END LOOP;

      FOREACH _svc IN ARRAY _start
      LOOP
        SELECT JSONB_BUILD_OBJECT('type', 'svc_inst_sync', 'action', 'start',
                                  'instance', NEW.rec_id, 'data', _svc)
        INTO _payload;
        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;
      END LOOP;

      FOREACH _svc IN ARRAY _restart
      LOOP
        SELECT JSONB_BUILD_OBJECT('type', 'svc_inst_sync', 'action', 'restart',
                                  'instance', NEW.rec_id, 'data', _svc)
        INTO _payload;
        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;
      END LOOP;
  END IF;

  RETURN NULL;
END $$;

COMMENT ON FUNCTION app.trf_sync_comp_mgmt_svc_inst IS
'Trigger: AMQP comp_mgmt svc_inst_sync (cr-comp-mgmt.service)';

CREATE OR REPLACE FUNCTION app.trf_sync_rec_ctrl_chan()
  RETURNS TRIGGER
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _payload jsonb;
  _action text;
  _message text;
BEGIN
    IF (TG_OP = 'INSERT') THEN

      IF (NEW.proc_chan_chg) THEN
        _action := 'commit';
      ELSIF (NOT NEW.proc_chan_chg) THEN
        _action := 'store';
      END IF;

    ELSIF (TG_OP = 'UPDATE') THEN

      IF (  (NOT OLD.proc_chan_chg AND NEW.proc_chan_chg)
            OR (NEW.channel <> OLD.channel) ) THEN

        _action := 'commit';

      ELSIF ( (NEW.rec_id <> OLD.rec_id)
              AND (NEW.channel = OLD.channel) ) THEN

        IF (NEW.verify_chan) THEN
          _action := 'commit';
        ELSE
          _action := 'store';
        END IF;

      END IF;

    END IF;

    IF (_action in ('commit', 'store')) THEN

      SELECT JSONB_BUILD_OBJECT('type', 'channel', 'action', _action,
                                'instance', NEW.rec_id,
                                'data', JSON_AGG(NEW.channel))
      INTO _payload;

      IF NOT app.f_amqp_pub('rec_ctrl.' || NEW.rec_id, _payload) THEN
        RAISE EXCEPTION 'Failed to publish to receiver control (AMQP error)';
      END IF;

    END IF;

  RETURN NULL;
END
$$;

COMMENT ON FUNCTION app.trf_sync_rec_ctrl_chan IS
'Trigger: AMQP rec_ctrl.N channel (cr-control@.service)';

CREATE OR REPLACE FUNCTION app.trf_sync_comp_mgmt_cfg()
  RETURNS TRIGGER
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _payload jsonb;
BEGIN
  IF ((TG_OP = 'TRUNCATE') AND (TG_TABLE_NAME != 'glb_rec_cfg')) THEN

    SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'clear')
    INTO _payload;

    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;

  ELSE

    IF (TG_TABLE_NAME = 'rec_cfg') THEN

      IF (TG_OP in ('INSERT', 'UPDATE')) then

        SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'write',
                                  'instance', rec_id, 'data', data)
        INTO _payload
        FROM  ( SELECT rec_id, data
                FROM app.comp_mgmt_cfg_sync
                WHERE rec_id = NEW.rec_id
              ) r ;

        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;

      END IF;

      IF ((TG_OP = 'UPDATE') AND (OLD.rec_id <> NEW.rec_id)) OR
        (TG_OP = 'DELETE') THEN

        SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'remove',
                                  'instance', OLD.rec_id)
        INTO _payload;

        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;

      END IF;

    ELSIF (TG_TABLE_NAME = 'pdu_conn') THEN

      FOR _payload IN
        SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'write',
                                  'instance', rec_id, 'data', data)
        FROM  ( SELECT rec_id, data
                FROM app.pdu_conn
                INNER JOIN app.rec_cfg USING (pdu_conn)
                INNER JOIN app.comp_mgmt_cfg_sync USING (rec_id)
                WHERE pdu_conn = NEW.pdu_conn
              ) r
      LOOP
        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;
      END LOOP;

    ELSIF (TG_TABLE_NAME = 'rec_gen') THEN

      FOR _payload IN
        SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'write',
                                  'instance', rec_id, 'data', data)
        FROM  ( SELECT rec_id, data
                FROM app.rec_gen
                INNER JOIN app.rec_cfg USING (rec_type)
                INNER JOIN app.comp_mgmt_cfg_sync USING (rec_id)
                WHERE rec_type = NEW.rec_type
              ) r
      LOOP
        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;
      END LOOP;

    ELSIF (TG_TABLE_NAME = 'feed_act') THEN
      FOR _payload IN
        SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'write',
                                  'instance', rec_id, 'data', data)
        FROM  ( SELECT rec_id, data
                FROM app.feed_act
                INNER JOIN app.rec_cfg USING (feed_act_group)
                INNER JOIN app.comp_mgmt_cfg_sync USING (rec_id)
                WHERE feed_act_group = NEW.feed_act_group
              ) r
      LOOP
        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;
      END LOOP;

    -- Single-row table glb_rec_cfg used for global config
    ELSIF (TG_TABLE_NAME = 'glb_rec_cfg') THEN

      -- TRUNCATE on app.rec_cfg clears all configuration synced via
      -- amqp, including data sourced from here. Performed on all user
      -- editable tables when executing function app.f_config_clear.
      IF ((TG_OP = 'DELETE') OR (TG_OP = 'TRUNCATE')) THEN

        SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'remove',
                                  'instance', 0)
        INTO _payload;

        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;

      ELSE

        SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'write',
                                  'instance', 0, 'data', data)
        INTO _payload
        FROM  ( SELECT data
                FROM app.comp_mgmt_cfg_sync
                WHERE rec_id = 0
              ) r;

        IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
          RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
        END IF;

      END IF;

    END IF;

  END IF;

  RETURN NULL;
END $$;

COMMENT ON FUNCTION app.trf_sync_comp_mgmt_cfg IS
'Trigger: AMQP comp_mgmt cfg_sync (cr-comp-mgmt.service; syncs to redis)';

CREATE OR REPLACE FUNCTION app.f_manual_sync_stop()
  RETURNS void
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _svc text;
  _payload jsonb;
  _stop text[] DEFAULT '{}';
BEGIN

  PERFORM TRUE FROM amqp.broker WHERE broker_id = 1;
  IF NOT FOUND THEN
    RAISE EXCEPTION 'Broker not configured';
  END IF;

  -- Stop all instantiated services
  _stop := ARRAY ['capture', 'analyze', 'discover', 'control'];

  FOREACH _svc IN ARRAY _stop
  LOOP
    SELECT jsonb_build_object('type', 'svc_inst_sync', 'action', 'stop',
                              'instance', '*', 'data', _svc)
    INTO _payload;

    RAISE INFO 'Stop all % service instances', _svc;
    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;
  END LOOP;

  -- Stop all non-instantiated services
  _stop := ARRAY ['alert', 'coverage', 'preview'];

  FOREACH _svc IN ARRAY _stop
  LOOP
    SELECT JSONB_BUILD_OBJECT('type', 'svc_sync', 'action', 'stop',
                              'data', _svc)
    INTO _payload;
    RAISE INFO 'Stop % service', _svc;
    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;
  END LOOP;

  -- Clear configuration
  SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'clear')
  INTO _payload;

  RAISE INFO 'Clear configuration';
  IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
    RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
  END IF;

END
$$;

COMMENT ON FUNCTION app.f_manual_sync_stop is
'AMQP manual worker sync stop/clear';

CREATE OR REPLACE FUNCTION app.f_manual_sync_start()
  RETURNS void
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _svc text;
  _payload jsonb;
  _start text[] DEFAULT '{}';
  _rec smallint;
BEGIN

  PERFORM TRUE FROM amqp.broker WHERE broker_id = 1;
  IF NOT FOUND THEN
    RAISE EXCEPTION 'Broker not configured';
  END IF;

  -- Sync configuration for existing receivers
  FOR _payload IN
    SELECT JSONB_BUILD_OBJECT('type', 'cfg_sync', 'action', 'write',
                              'instance', rec_id, 'data', data)
    FROM  ( SELECT rec_id, data
            FROM app.comp_mgmt_cfg_sync
          ) r
    ORDER BY rec_id
  LOOP
    _rec := _payload->'instance';
    RAISE INFO 'Sync configuration receiver %', _rec;

    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;
  END LOOP;

  -- Start non-instantiated services
  IF (SELECT TRUE FROM app.glb_rec_cfg
      WHERE alert AND glb_rec_cfg_id = 0) THEN

    _start := array_append(_start, 'alert');

  END IF;

  IF (SELECT TRUE FROM app.glb_rec_cfg
      WHERE coverage AND glb_rec_cfg_id = 0) THEN

    _start := array_append(_start, 'coverage');

  END IF;

  IF (SELECT TRUE FROM app.glb_rec_cfg
      WHERE preview AND glb_rec_cfg_id = 0) THEN

    _start := array_append(_start, 'preview');

  END IF;

  FOREACH _svc IN ARRAY _start
  LOOP
    SELECT JSONB_BUILD_OBJECT('type', 'svc_sync', 'action', 'start',
                              'data', _svc)
    INTO _payload;
    RAISE INFO 'Start service %', _svc;

    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;
  END LOOP;

  -- Start control service for existing receivers
  FOR _payload IN
    SELECT JSONB_BUILD_OBJECT('type', 'svc_inst_sync', 'action', 'start',
                              'instance', rec_id, 'data', 'control')
    FROM  ( SELECT rec_id
            FROM app.rec_cfg
          ) r
    ORDER BY rec_id
  LOOP
    _rec := _payload->'instance';
    RAISE INFO 'Start control service receiver %', _rec;

    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;
  END LOOP;

  -- Sync channel
  FOR _payload IN
  SELECT JSONB_BUILD_OBJECT('type', 'channel', 'action', 'commit',
                            'instance', rec_id, 'data', JSON_AGG(channel))
    FROM  ( SELECT rec_id, channel
            FROM app.rec_cfg
            WHERE proc_chan_chg
          ) r
    GROUP BY rec_id
    ORDER BY rec_id
  LOOP
    _rec := _payload->'instance';
    RAISE INFO 'Commit channel receiver %', _rec;

    IF NOT app.f_amqp_pub('rec_ctrl.' || _rec, _payload) THEN
      RAISE EXCEPTION 'Failed to publish to receiver control (AMQP error)';
    END IF;
  END LOOP;

  FOR _payload IN
  SELECT JSONB_BUILD_OBJECT('type', 'channel', 'action', 'store',
                            'instance', rec_id, 'data', JSON_AGG(channel))
    FROM  ( SELECT rec_id, channel
            FROM app.rec_cfg
            WHERE NOT proc_chan_chg
          ) r
    GROUP BY rec_id
    ORDER BY rec_id
  LOOP
    _rec := _payload->'instance';
    RAISE INFO 'Store channel receiver %', _rec;

    IF NOT app.f_amqp_pub('rec_ctrl.' || _rec, _payload) THEN
      RAISE EXCEPTION 'Failed to publish to receiver control (AMQP error)';
    END IF;
  END LOOP;

  -- Start configured services for existing receivers
  FOR _payload IN
    SELECT JSONB_BUILD_OBJECT('type', 'svc_inst_sync', 'action', 'start',
                              'instance', rec_id, 'data', 'capture')
    FROM  ( SELECT rec_id
            FROM app.rec_cfg
            WHERE vid_analyze
          ) r
    ORDER BY rec_id
  LOOP
    _rec := _payload->'instance';
    RAISE INFO 'Start capture service receiver %', _rec;

    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;
  END LOOP;

  FOR _payload IN
    SELECT JSONB_BUILD_OBJECT('type', 'svc_inst_sync', 'action', 'start',
                              'instance', rec_id, 'data', 'discover')
    FROM  ( SELECT rec_id
            FROM app.rec_cfg
            WHERE verify_chan
          ) r
    ORDER BY rec_id
  LOOP
    _rec := _payload->'instance';
    RAISE INFO 'Start discover service receiver %', _rec;

    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;
  END LOOP;

  FOR _payload IN
    SELECT JSONB_BUILD_OBJECT('type', 'svc_inst_sync', 'action', 'start',
                              'instance', rec_id, 'data', 'analyze')
    FROM  ( SELECT rec_id
            FROM app.rec_cfg
            WHERE vid_analyze OR verify_chan
          ) r
    ORDER BY rec_id
  LOOP
    _rec := _payload->'instance';
    RAISE INFO 'Start analyze service receiver %', _rec;

    IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
      RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
    END IF;
  END LOOP;

END
$$;

COMMENT ON FUNCTION app.f_manual_sync_start IS
'AMQP manual worker sync start/config';

CREATE OR REPLACE FUNCTION app.trf_update_last_mod()
  RETURNS TRIGGER
  LANGUAGE plpgsql
AS $$
BEGIN
  NEW.last_modified := NOW();

  RETURN NEW;
END
$$;

COMMENT ON FUNCTION app.trf_update_last_mod IS 'Trigger: Update last_modified';

CREATE OR REPLACE FUNCTION app.trf_check_chan_restr()
  RETURNS TRIGGER
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _ch_col_name app.channel_nr;
BEGIN
  IF (TG_TABLE_NAME = 'rec_cfg') THEN
    _ch_col_name = NEW.channel;

    IF (NOT NEW.proc_chan_chg AND NEW.channel <> OLD.channel) THEN

      RAISE EXCEPTION 'Update on table % violates restriction',
        TG_TABLE_NAME
        USING DETAIL = 'Channel may not be changed'
          ' for receiver ' || NEW.rec_id,
        HINT = 'Processing administratively disabled',
        ERRCODE = 'restrict_violation';

      RETURN NULL;

    END IF;

  -- Strip seconds off timestamp, schedule is only processed once a minute
  ELSIF (TG_TABLE_NAME = 'chan_schedule') THEN

    NEW.datetime := date_trunc('minute', NEW.datetime);
    _ch_col_name = NEW.sched_channel;

    IF (  SELECT TRUE FROM app.rec_cfg
          WHERE NOT proc_chan_chg
          AND rec_id = NEW.rec_id ) THEN

      RAISE EXCEPTION '% on table % violates restriction',
        INITCAP(TG_OP), TG_TABLE_NAME
        USING DETAIL = 'Schedule may not be added to/changed'
          ' for receiver ' || NEW.rec_id,
        HINT = 'Processing administratively disabled',
        ERRCODE = 'restrict_violation';

      RETURN NULL;

    END IF;

  ELSE

    RAISE EXCEPTION 'TRIGGER % failed to execute. '
      'Channel column name for table % not defined', TG_NAME, TG_TABLE_NAME
    USING ERRCODE = 'triggered_action_exception';
    RETURN NULL;

  END IF;

  BEGIN
  -- Only evaluate denylist for regular users, schedule processing must be
  -- able to apply a scheduled channel change that was added to the denylist
  -- afterwards; SESSION_USER required due to SECURITY_DEFINER
  IF (  SELECT pg_has_role(SESSION_USER,'cr_g_api_users','USAGE') ) THEN

    IF (SELECT TRUE FROM app.chan_denylist
        WHERE rec_id = NEW.rec_id AND deny_channel = _ch_col_name) THEN

      RAISE EXCEPTION '% on table % violates restriction',
        INITCAP(TG_OP), TG_TABLE_NAME
        USING DETAIL = 'Channel ' || _ch_col_name
          || ' is not allowed on receiver ' || NEW.rec_id,
        HINT = 'Channel administratively denylisted',
        ERRCODE = 'restrict_violation';

      RETURN NULL;

    END IF;

  END IF;

  EXCEPTION WHEN UNDEFINED_OBJECT THEN
  END;

  RETURN NEW;

END
$$;

COMMENT ON FUNCTION app.trf_check_chan_restr IS
'Trigger: Enforce channel restrictions (Denylist; processing disabled)';

-- Receiver ID rename is supported by sync_comp_mgmt_svc_inst logic.
-- However, on the services side it is akin to delete/insert. A change
-- while an operation is running might be disruptive, hence require
-- features to be switched off before allowing change
CREATE OR REPLACE FUNCTION app.trf_check_rec_id_restr()
  RETURNS TRIGGER
  LANGUAGE plpgsql
AS $$
DECLARE
  _reset_delay interval := '5 minutes';
  _mod_delay interval := '30 seconds';
  _reset_delay_text text;
  _mod_delay_text text;
BEGIN

  IF  (NEW.rec_id <> OLD.rec_id)
      AND ( NEW.proc_chan_chg OR NEW.verify_chan OR NEW.vid_analyze
            OR NEW.remote_ctrl
            OR (OLD.pdu_last_reset + _reset_delay > transaction_timestamp() )
            OR (OLD.last_modified + _mod_delay > transaction_timestamp() )
          ) THEN

    _mod_delay_text := app.f_convert_interval_min(_mod_delay) || ' minutes';
    _reset_delay_text := app.f_convert_interval_min(_reset_delay) || ' minutes';

    RAISE EXCEPTION 'Update on table % violates restriction', TG_TABLE_NAME
      USING DETAIL = 'Receiver ID change ' || OLD.rec_id || ' => ' ||
      NEW.rec_id || ' permissible if the following conditions are met: ' ||
      'proc_chan_chg/verify_chan/vid_analyze/remote_ctrl: disabled, ' ||
      'Time elapsed since last update: ' || _mod_delay_text || ', ' ||
      'Time elapsed since last reset: ' || _reset_delay_text,
      ERRCODE = 'restrict_violation';

    RETURN NULL;

  END IF;

  RETURN NEW;

END
$$;

COMMENT ON FUNCTION app.trf_check_rec_id_restr IS
'Trigger: Allow rec_id change only if all processing is off and time has '
'passed';

-- To recover a receiver stuck on a blue screen, switching to a
-- different channel is needed. We need to ensure that current and
-- backup channel cannot be the same
CREATE OR REPLACE FUNCTION app.trf_check_backup_chan_restr()
  RETURNS TRIGGER
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  IF (TG_TABLE_NAME = 'rec_cfg') THEN

    IF  (  SELECT TRUE FROM app.chan_catalog
           WHERE channel = NEW.backup_chan
        )  THEN

      RAISE EXCEPTION '% on table % violates restriction',
        INITCAP(TG_OP), TG_TABLE_NAME
        USING DETAIL = 'Backup channel ' || NEW.backup_chan
        || ' present in channel catalog',
        HINT = 'Assignable channels cannot be used as backup channel',
        ERRCODE = 'restrict_violation';

      RETURN NULL;

    END IF;

  ELSIF (TG_TABLE_NAME = 'chan_catalog') THEN

    IF  (  SELECT EXISTS (  SELECT TRUE FROM app.rec_cfg
                            WHERE backup_chan = NEW.channel )
        )  THEN

      RAISE EXCEPTION '% on table % violates restriction',
        INITCAP(TG_OP), TG_TABLE_NAME
        USING DETAIL = 'Channel ' || NEW.channel
        || ' in use as backup channel',
        HINT = 'Backup channel cannot be used as assignable channel',
        ERRCODE = 'restrict_violation';

      RETURN NULL;

    END IF;

  END IF;

  RETURN NEW;

END
$$;

COMMENT ON FUNCTION app.trf_check_backup_chan_restr IS
'Trigger: Prevent backup channel matching channel available for assignment';

CREATE TABLE app.rec_gen (
  rec_gen_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  rec_type text NOT NULL UNIQUE,
  remote_cmds jsonb NOT NULL,
  startup_time interval(0) NOT NULL
);

CREATE TRIGGER sync_comp_mgmt_cfg_ar_u
AFTER UPDATE OF startup_time ON app.rec_gen
FOR EACH ROW
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_cfg();

COMMENT ON TABLE app.rec_gen IS 'Supported receiver types';
COMMENT ON COLUMN app.rec_gen.rec_gen_id IS 'Receiver Gen record ID';
COMMENT ON COLUMN app.rec_gen.rec_type IS 'Receiver type';
COMMENT ON COLUMN app.rec_gen.remote_cmds IS 'Remote key name/cmd grouped';
COMMENT ON COLUMN app.rec_gen.startup_time IS 'Startup time';
COMMENT ON TRIGGER sync_comp_mgmt_cfg_ar_u ON app.rec_gen IS
'AMQP comp_mgmt cfg_sync';

CREATE TABLE app.chan_catalog (
  chan_catalog_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  channel app.channel_nr NOT NULL UNIQUE
);

COMMENT ON TABLE app.chan_catalog IS 'Available channels';
COMMENT ON COLUMN app.chan_catalog.chan_catalog_id IS
'Channel catalog record ID';
COMMENT ON COLUMN app.chan_catalog.channel IS 'Channel';

CREATE TRIGGER check_backup_chan_restr_br_iu
BEFORE INSERT OR UPDATE OF channel ON app.chan_catalog
FOR EACH ROW
EXECUTE PROCEDURE app.trf_check_backup_chan_restr();

COMMENT ON TRIGGER check_backup_chan_restr_br_iu ON app.chan_catalog IS
'Backup channel cannot be used as assignable channel';

CREATE TABLE app.pdu_type (
  pdu_type_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  pdu_model text NOT NULL,
  pdu_protocol text NOT NULL,
  UNIQUE (pdu_model, pdu_protocol)
);

COMMENT ON TABLE app.pdu_type IS 'Supported PDU models/protocols';
COMMENT ON COLUMN app.pdu_type.pdu_type_id IS 'PDU type record ID';
COMMENT ON COLUMN app.pdu_type.pdu_model IS 'PDU model';
COMMENT ON COLUMN app.pdu_type.pdu_protocol IS 'PDU protocol';

CREATE TABLE app.pdu_conn (
  pdu_conn_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  pdu_conn app.pos_smallint NOT NULL UNIQUE,
  pdu_model text NOT NULL,
  pdu_protocol text NOT NULL,
  pdu_host app.host NOT NULL,
  pdu_user text,
  pdu_password text,
  UNIQUE (pdu_model, pdu_protocol, pdu_host, pdu_user, pdu_password),
  FOREIGN KEY (pdu_model, pdu_protocol)
  REFERENCES app.pdu_type (pdu_model, pdu_protocol)
  ON UPDATE CASCADE ON DELETE RESTRICT
);

CREATE TRIGGER sync_comp_mgmt_cfg_ar_u
AFTER UPDATE ON app.pdu_conn
FOR EACH ROW
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_cfg();

COMMENT ON TABLE app.pdu_conn IS 'PDU connection';
COMMENT ON COLUMN app.pdu_conn.pdu_conn_id IS 'PDU connection record ID';
COMMENT ON COLUMN app.pdu_conn.pdu_conn IS 'PDU connection';
COMMENT ON COLUMN app.pdu_conn.pdu_model IS 'PDU model';
COMMENT ON COLUMN app.pdu_conn.pdu_protocol IS 'PDU API protocol';
COMMENT ON COLUMN app.pdu_conn.pdu_host IS 'PDU host';
COMMENT ON COLUMN app.pdu_conn.pdu_user IS 'PDU API username';
COMMENT ON COLUMN app.pdu_conn.pdu_password IS 'PDU API password';
COMMENT ON TRIGGER sync_comp_mgmt_cfg_ar_u ON app.pdu_conn IS
'AMQP comp_mgmt cfg_sync';

CREATE TABLE app.feed_act (
  feed_act_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  feed_act_group app.pos_smallint NOT NULL UNIQUE,
  feed_act_ini_delay app.pos_interval NOT NULL CHECK (
    (feed_act_ini_delay >= '00:01:00'::interval)
    AND (feed_act_ini_delay <= '06:00:00'::interval)
  ),
  feed_act_rex_delay app.pos_interval NOT NULL CHECK (
    (feed_act_rex_delay >= '00:05:00'::interval)
    AND (feed_act_rex_delay <= '1 mon'::interval)
  ),
  res_act_ini_delay app.pos_interval NOT NULL CHECK (
    (res_act_ini_delay >= '00:01:00'::interval)
    AND (res_act_ini_delay <= '06:00:00'::interval)
  ),
  res_act_rex_delay app.pos_interval NOT NULL CHECK (
    (res_act_rex_delay >= '00:05:00'::interval)
    AND (res_act_rex_delay <= '1 mon'::interval)
  )
);

COMMENT ON TABLE app.feed_act IS 'Feed action settings';
COMMENT ON COLUMN app.feed_act.feed_act_id IS 'Feed action id';
COMMENT ON COLUMN app.feed_act.feed_act_group IS 'Feed action group';
COMMENT ON COLUMN app.feed_act.feed_act_ini_delay IS
'No feed action initial delay';
COMMENT ON COLUMN app.feed_act.feed_act_rex_delay IS
'No feed action re-exec delay';
COMMENT ON COLUMN app.feed_act.res_act_ini_delay IS
'Incorrect resolution action initial delay';
COMMENT ON COLUMN app.feed_act.res_act_rex_delay IS
'Incorrect resolution action re-exec delay';

CREATE TABLE app.rec_cfg (
  -- rec_id 0 is used for global receiver configuration by view
  -- app.comp_mgmt_cfg_sync / trigger function app.trf_sync_comp_mgmt_cfg
  rec_id smallint NOT NULL PRIMARY KEY CHECK (rec_id > 0),
  -- Arbitrary limit, needs corresponding rabbitmq queues
  CONSTRAINT rec_cfg_rec_id_max_check CHECK (rec_id <= 16),
  descr text NULL,
  channel app.channel_nr NOT NULL,
  proc_chan_chg boolean DEFAULT TRUE NOT NULL,
  rec_type text NOT NULL,
  ctrl_conn app.host_ipv4 UNIQUE NULL
  CONSTRAINT rec_cfg_ctrl_conn_check CHECK (
    NOT (proc_chan_chg AND ctrl_conn IS NULL)
    AND
    NOT (verify_chan AND ctrl_conn IS NULL)
    AND
    NOT (vid_analyze AND ctrl_conn IS NULL)
    AND
    NOT (remote_ctrl AND ctrl_conn IS NULL)
  ),
  verify_chan boolean NOT NULL DEFAULT FALSE,
  vid_src_conn app.vid_src_conn,
  CONSTRAINT rec_cfg_vid_src_conn_alsa_device_v4l2_device_check CHECK (
    (vid_src_conn->'alsa'->>'device') <> (vid_src_conn->'v4l2'->>'device')
  ),
  vid_analyze boolean NOT NULL DEFAULT FALSE,
  CONSTRAINT rec_cfg_vid_analyze_vid_src_conn_check CHECK (
    NOT (vid_analyze AND (vid_src_conn IS NULL))
  ),
  no_feed_act boolean NOT NULL DEFAULT FALSE,
  res_act boolean NOT NULL DEFAULT FALSE,
  CONSTRAINT rec_cfg_feed_act_vid_src_conn_check CHECK (
    NOT ( res_act AND (NOT (vid_src_conn->>'type' = 'network')) )
    AND
    NOT ( no_feed_act AND (NOT (vid_src_conn->>'type' = 'network')) )
  ),
  CONSTRAINT rec_cfg_feed_act_check CHECK (
    NOT (res_act AND feed_act_group IS NULL)
    AND
    NOT (no_feed_act AND feed_act_group IS NULL)
  ),
  feed_act_group app.pos_smallint NULL,
  backup_chan app.channel_nr NOT NULL DEFAULT 501,
  pin app.pin NULL,
  remote_ctrl boolean NOT NULL DEFAULT FALSE,
  pdu_ctrl boolean NOT NULL DEFAULT FALSE,
  pdu_conn app.pos_smallint NULL,
  pdu_outlet smallint UNIQUE NULL CHECK (pdu_outlet >= 0),
  CONSTRAINT rec_cfg_pdu_ctrl_check CHECK (
    NOT (pdu_ctrl AND pdu_conn IS NULL)
    AND
    NOT (pdu_ctrl AND pdu_outlet IS NULL)
  ),
  alert_net boolean NOT NULL DEFAULT FALSE,
  alert_av boolean NOT NULL DEFAULT FALSE,
  pdu_last_reset timestamptz(0) NULL,
  last_modified timestamptz(0) NULL,
  FOREIGN KEY (feed_act_group) REFERENCES app.feed_act (feed_act_group)
  ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (rec_type) REFERENCES app.rec_gen (rec_type)
  ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (channel) REFERENCES app.chan_catalog (channel)
  ON UPDATE RESTRICT ON DELETE RESTRICT,
  FOREIGN KEY (pdu_conn) REFERENCES app.pdu_conn (pdu_conn)
  ON UPDATE RESTRICT ON DELETE RESTRICT
);

CREATE UNIQUE INDEX rec_cfg_vid_src_conn_udp_host_udp_port_idx
ON app.rec_cfg  (
                    (vid_src_conn->'udp'->>'host'),
                    (vid_src_conn->'udp'->>'port')
                  );
CREATE UNIQUE INDEX rec_cfg_vid_src_conn_alsa_device_idx
ON app.rec_cfg  (
                    (vid_src_conn->'alsa'->>'device')
                  );
CREATE UNIQUE INDEX rec_cfg_vid_src_conn_v4l2_device_idx
ON app.rec_cfg  (
                    (vid_src_conn->'v4l2'->>'device')
                  );

CREATE TRIGGER check_chan_restr_br_u
BEFORE UPDATE OF channel ON app.rec_cfg
FOR EACH ROW
EXECUTE PROCEDURE app.trf_check_chan_restr();

CREATE TRIGGER check_backup_chan_restr_br_iu
BEFORE INSERT OR UPDATE OF backup_chan ON app.rec_cfg
FOR EACH ROW
EXECUTE PROCEDURE app.trf_check_backup_chan_restr();

CREATE TRIGGER sync_comp_mgmt_svc_inst_ar_iud
AFTER INSERT OR UPDATE OF rec_id, vid_analyze, verify_chan, vid_src_conn,
ctrl_conn OR DELETE ON app.rec_cfg
FOR EACH ROW
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_svc_inst();

CREATE TRIGGER sync_comp_mgmt_svc_inst_as_t
AFTER TRUNCATE ON app.rec_cfg
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_svc_inst();

CREATE TRIGGER sync_rec_ctrl_chan_ar_iu
AFTER INSERT OR UPDATE OF rec_id, proc_chan_chg, channel
ON app.rec_cfg
FOR EACH ROW
EXECUTE FUNCTION app.trf_sync_rec_ctrl_chan();

CREATE TRIGGER sync_comp_mgmt_cfg_ar_iud
AFTER INSERT OR UPDATE OF rec_id, rec_type, ctrl_conn, verify_chan,
vid_src_conn, vid_analyze, backup_chan, pin, pdu_ctrl, pdu_conn,
pdu_outlet, no_feed_act, res_act, feed_act_group
OR DELETE ON app.rec_cfg
FOR EACH ROW
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_cfg();

CREATE TRIGGER sync_comp_mgmt_cfg_as_t
AFTER TRUNCATE ON app.rec_cfg
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_cfg();

CREATE TRIGGER sync_comp_mgmt_cfg_ar_u
AFTER UPDATE OF feed_act_ini_delay, feed_act_rex_delay, res_act_ini_delay,
res_act_rex_delay, feed_act_group
ON app.feed_act
FOR EACH ROW
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_cfg();

CREATE TRIGGER update_last_mod_br_iu
BEFORE INSERT OR UPDATE ON app.rec_cfg
FOR EACH ROW
EXECUTE FUNCTION app.trf_update_last_mod();

CREATE TRIGGER check_rec_id_restr_br_u
BEFORE UPDATE OF rec_id ON app.rec_cfg
FOR EACH ROW
EXECUTE PROCEDURE app.trf_check_rec_id_restr();

COMMENT ON TABLE app.rec_cfg IS 'Receiver configuration';
COMMENT ON COLUMN app.rec_cfg.rec_id IS 'Receiver No.';
COMMENT ON COLUMN app.rec_cfg.descr IS 'Receiver description';
COMMENT ON COLUMN app.rec_cfg.channel IS 'Target channel';
COMMENT ON COLUMN app.rec_cfg.proc_chan_chg IS
'Process/allow channel changes';
COMMENT ON COLUMN app.rec_cfg.rec_type IS 'Receiver type';
COMMENT ON COLUMN app.rec_cfg.ctrl_conn IS 'Control host/IPv4';
COMMENT ON COLUMN app.rec_cfg.remote_ctrl IS 'Remote control function';
COMMENT ON COLUMN app.rec_cfg.vid_analyze IS
'Channel reset when no signal detected';
COMMENT ON COLUMN app.rec_cfg.no_feed_act IS
'No feed action (Network video source)';
COMMENT ON COLUMN app.rec_cfg.res_act IS
'Incorrect resolution action (Network video source)';
COMMENT ON COLUMN app.rec_cfg.feed_act_group IS
'Feed action group';
COMMENT ON COLUMN app.rec_cfg.vid_src_conn IS 'Video source connection';
COMMENT ON COLUMN app.rec_cfg.verify_chan IS
'Verify tuned channel matches current setting';
COMMENT ON COLUMN app.rec_cfg.backup_chan IS
'Channel briefly switched to during reset sequence';
COMMENT ON COLUMN app.rec_cfg.pin IS 'Parental control PIN';
COMMENT ON COLUMN app.rec_cfg.alert_av IS 'Alert A/V loss';
COMMENT ON COLUMN app.rec_cfg.alert_net IS 'Alert network connection loss';
COMMENT ON COLUMN app.rec_cfg.pdu_ctrl IS 'Power control';
COMMENT ON COLUMN app.rec_cfg.pdu_outlet IS 'PDU Outlet';
COMMENT ON COLUMN app.rec_cfg.pdu_conn IS 'PDU connection';
COMMENT ON COLUMN app.rec_cfg.pdu_last_reset IS 'PDU last reset';
COMMENT ON COLUMN app.rec_cfg.last_modified IS 'row last modified';
COMMENT ON TRIGGER check_chan_restr_br_u ON app.rec_cfg IS
'Enforce channel restrictions (Denylist; processing disabled)';
COMMENT ON TRIGGER check_backup_chan_restr_br_iu ON app.rec_cfg IS
'Assignable channels cannot be used as backup channel';
COMMENT ON TRIGGER sync_comp_mgmt_svc_inst_ar_iud ON app.rec_cfg IS
'AMQP comp_mgmt svc_inst_sync';
COMMENT ON TRIGGER sync_comp_mgmt_svc_inst_as_t ON app.rec_cfg IS
'AMQP comp_mgmt svc_inst_sync';
COMMENT ON TRIGGER sync_rec_ctrl_chan_ar_iu ON app.rec_cfg IS
'AMQP rec_ctrl.N channel';
COMMENT ON TRIGGER sync_comp_mgmt_cfg_ar_iud ON app.rec_cfg IS
'AMQP comp_mgmt cfg_sync';
COMMENT ON TRIGGER sync_comp_mgmt_cfg_as_t ON app.rec_cfg IS
'AMQP comp_mgmt cfg_sync';
COMMENT ON TRIGGER update_last_mod_br_iu ON app.rec_cfg IS
'Update last_modified';
COMMENT ON TRIGGER check_rec_id_restr_br_u ON app.rec_cfg IS
'Enforce restrictions for renaming rec_id';

CREATE TABLE app.chan_denylist (
  chan_denylist_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  rec_id smallint NOT NULL,
  deny_channel app.channel_nr NOT NULL,
  UNIQUE (rec_id, deny_channel),
  FOREIGN KEY (rec_id) REFERENCES app.rec_cfg (rec_id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (deny_channel) REFERENCES app.chan_catalog (channel)
  ON UPDATE RESTRICT ON DELETE RESTRICT
);

COMMENT ON TABLE app.chan_denylist IS 'Channel denylist';
COMMENT ON COLUMN app.chan_denylist.chan_denylist_id IS
'Channel denylist record ID';
COMMENT ON COLUMN app.chan_denylist.rec_id IS 'Receiver ID';
COMMENT ON COLUMN app.chan_denylist.deny_channel IS 'denylisted channel';

CREATE TABLE app.chan_schedule (
  chan_schedule_id bigint GENERATED ALWAYS AS IDENTITY PRIMARY KEY,
  rec_id smallint NOT NULL,
  sched_channel app.channel_nr NOT NULL,
  datetime timestamptz(0) NOT NULL CHECK (datetime > NOW()),
  UNIQUE (rec_id, datetime),
  FOREIGN KEY (rec_id) REFERENCES app.rec_cfg (rec_id)
  ON UPDATE CASCADE ON DELETE CASCADE,
  FOREIGN KEY (sched_channel) REFERENCES app.chan_catalog (channel)
  ON UPDATE RESTRICT ON DELETE RESTRICT
);

CREATE TRIGGER check_chan_restr_br_iu
BEFORE INSERT
OR UPDATE ON app.chan_schedule
FOR EACH ROW
EXECUTE PROCEDURE app.trf_check_chan_restr();

COMMENT ON TABLE app.chan_schedule IS 'Scheduled channel changes';
COMMENT ON COLUMN app.chan_schedule.rec_id IS 'Receiver ID';
COMMENT ON COLUMN app.chan_schedule.chan_schedule_id IS 'Schedule record ID';
COMMENT ON COLUMN app.chan_schedule.sched_channel IS 'Target channel';
COMMENT ON COLUMN app.chan_schedule.datetime IS 'Timestamp';

CREATE TABLE app.glb_rec_cfg (
  glb_rec_cfg_id smallint NOT NULL PRIMARY KEY DEFAULT 0 CHECK (
    glb_rec_cfg_id = 0),
  site_name text,
  analyze_suspend app.pos_interval NOT NULL CHECK (
    (analyze_suspend >= '00:00:30'::interval)
    AND (analyze_suspend <= '00:30:00'::interval)
  ),
  alert_net_thresh app.pos_interval NOT NULL CHECK (
    (alert_net_thresh >= '00:15:00'::interval)
    AND (alert_net_thresh <= '48:00:00'::interval)
  ),
  alert_av_thresh app.pos_interval NOT NULL CHECK (
    (alert_av_thresh >= '00:15:00'::interval)
    AND (alert_av_thresh <= '48:00:00'::interval)
  ),
  alert_email app.email ARRAY,
  CONSTRAINT glb_rec_cfg_alert_email_check CHECK (
    NOT (alert AND alert_email IS NULL)
  ),
  alert boolean DEFAULT FALSE NOT NULL,
  coverage boolean DEFAULT FALSE NOT NULL,
  preview boolean DEFAULT FALSE NOT NULL,
  alarm boolean DEFAULT FALSE NOT NULL
);

CREATE TRIGGER sync_comp_mgmt_svc_ar_iud
AFTER INSERT OR UPDATE OF alert, coverage, preview
OR DELETE ON app.glb_rec_cfg
FOR EACH ROW
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_svc();

CREATE TRIGGER sync_comp_mgmt_svc_as_t
AFTER TRUNCATE ON app.glb_rec_cfg
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_svc();

CREATE TRIGGER sync_comp_mgmt_cfg_ar_iud
AFTER INSERT OR UPDATE OF site_name, analyze_suspend, alert_net_thresh,
alert_av_thresh, alert_email, alarm, glb_rec_cfg_id
OR DELETE ON app.glb_rec_cfg
FOR EACH ROW
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_cfg();

CREATE TRIGGER sync_comp_mgmt_cfg_as_t
AFTER TRUNCATE ON app.glb_rec_cfg
EXECUTE PROCEDURE app.trf_sync_comp_mgmt_cfg();

COMMENT ON TABLE app.glb_rec_cfg IS 'Global receiver config';
COMMENT ON COLUMN app.glb_rec_cfg.glb_rec_cfg_id IS
'Global config record ID';
COMMENT ON COLUMN app.glb_rec_cfg.site_name IS
'Vessel name';
COMMENT ON COLUMN app.glb_rec_cfg.analyze_suspend IS
'Suspend analyze service duration';
COMMENT ON COLUMN app.glb_rec_cfg.alert_net_thresh IS
'Network alert threshold';
COMMENT ON COLUMN app.glb_rec_cfg.alert_av_thresh IS
'A/V alert threshold';
COMMENT ON COLUMN app.glb_rec_cfg.alert_email IS
'Alert email address(es)';
COMMENT ON COLUMN app.glb_rec_cfg.alert IS
'Alert feature';
COMMENT ON COLUMN app.glb_rec_cfg.coverage IS
'Geofencing feature';
COMMENT ON COLUMN app.glb_rec_cfg.preview IS
'Preview feature';
COMMENT ON COLUMN app.glb_rec_cfg.alarm IS
'Alarm feature';
COMMENT ON TRIGGER sync_comp_mgmt_cfg_ar_iud ON app.glb_rec_cfg IS
'AMQP comp_mgmt cfg_sync';
COMMENT ON TRIGGER sync_comp_mgmt_cfg_as_t ON app.glb_rec_cfg IS
'AMQP comp_mgmt cfg_sync';
COMMENT ON TRIGGER sync_comp_mgmt_svc_ar_iud ON app.glb_rec_cfg IS
'AMQP comp_mgmt svc_sync';
COMMENT ON TRIGGER sync_comp_mgmt_svc_as_t ON app.glb_rec_cfg IS
'AMQP comp_mgmt svc_sync';

CREATE OR REPLACE VIEW app.comp_mgmt_cfg_sync AS
SELECT r.rec_id,
  ( SELECT JSONB_STRIP_NULLS(TO_JSONB(q)) AS data
    FROM  ( SELECT  t.alert_av, t.alert_net, t.backup_chan, t.ctrl_conn,
                    t.descr, t.pin, t.verify_chan, t.vid_analyze,
                    t.vid_src_conn,

                    g.rec_type,
                    EXTRACT(epoch FROM g.startup_time) AS startup_time,

                    t.no_feed_act, t.res_act, t.feed_act_group,
                    EXTRACT(epoch FROM n.feed_act_ini_delay)
                    AS feed_act_ini_delay,
                    EXTRACT(epoch FROM n.feed_act_rex_delay)
                    AS feed_act_rex_delay,
                    EXTRACT(epoch FROM n.res_act_ini_delay)
                    AS res_act_ini_delay,
                    EXTRACT(epoch FROM n.res_act_rex_delay)
                    AS res_act_rex_delay,

                    t.pdu_ctrl, t.pdu_outlet, p.pdu_model,
                    p.pdu_protocol, p.pdu_host, p.pdu_user, p.pdu_password

            FROM app.rec_cfg t
            LEFT OUTER JOIN app.rec_gen g USING (rec_type)
            LEFT OUTER JOIN app.pdu_conn p USING (pdu_conn)
            LEFT OUTER JOIN app.feed_act n USING (feed_act_group)
            WHERE t.rec_id = r.rec_id
          ) q
  )
FROM app.rec_cfg r
UNION
SELECT r.glb_rec_cfg_id AS rec_id,
  ( SELECT JSONB_STRIP_NULLS(TO_JSONB(q)) AS data
    FROM  ( SELECT  site_name, alert_email, alarm,
                    EXTRACT(epoch FROM analyze_suspend) AS analyze_suspend,
                    EXTRACT(epoch FROM alert_net_thresh) AS alert_net_thresh,
                    EXTRACT(epoch FROM alert_av_thresh) AS alert_av_thresh
            FROM app.glb_rec_cfg t
            WHERE t.glb_rec_cfg_id = r.glb_rec_cfg_id
          ) q
  )
FROM app.glb_rec_cfg r
WHERE glb_rec_cfg_id = 0
ORDER BY rec_id;

COMMENT ON VIEW app.comp_mgmt_cfg_sync IS
'Aggregated config for trf_sync_comp_mgmt_cfg';

INSERT INTO app.rec_gen (rec_type, remote_cmds, startup_time)
VALUES ('sky+hd', '[[{"name": "TV", "key_code": "tv"},
{"name": "Sky", "key_code": "sky"},
{"name": "Standby", "key_code": "power"}],
[{"name": "TV guide", "key_code": "tvguide"},
{"name": "Box office", "key_code": "boxoffice"},
{"name": "Services", "key_code": "services"},
{"name": "Interactive", "key_code": "interactive"}],
[{"name": "Up", "key_code": "up"},
{"name": "Down", "key_code": "down"},
{"name": "Select", "key_code": "select"},
{"name": "Left", "key_code": "left"},
{"name": "Right", "key_code": "right"}],
[{"name": "Channel up", "key_code": "channelup"},
{"name": "Channel down", "key_code": "channeldown"},
{"name": "Info", "key_code": "i"}],
[{"name": "Text", "key_code": "text"},
{"name": "Back up", "key_code": "backup"},
{"name": "Help", "key_code": "help"}],
[{"name": "Play", "key_code": "play"},
{"name": "Pause", "key_code": "pause"},
{"name": "Stop", "key_code": "stop"},
{"name": "Record", "key_code": "record"},
{"name": "Rewind", "key_code": "rewind"},
{"name": "Fast forward", "key_code": "fastforward"}],
[{"name": "Red", "key_code": "red"},
{"name": "Green", "key_code": "green"},
{"name": "Yellow", "key_code": "yellow"},
{"name": "Blue", "key_code": "blue"}],
[{"name": "1", "key_code": "1"}, {"name": "2", "key_code": "2"},
{"name": "3", "key_code": "3"}],[ {"name": "4", "key_code": "4"},
{"name": "5", "key_code": "5"}, {"name": "6", "key_code": "6"}],
[ {"name": "7", "key_code": "7"}, {"name": "8", "key_code": "8"},
{"name": "9", "key_code": "9"}], [{"name": "0", "key_code": "0"}]]',
'00:02:20');

INSERT INTO app.rec_gen (rec_type, remote_cmds, startup_time)
VALUES ('skyq', '[[{"name": "Standby", "key_code": "power"},
{"name": "Sky", "key_code": "sky"},
{"name": "Search", "key_code": "search"}],
[{"name": "Play", "key_code": "play"},
{"name": "Pause", "key_code": "pause"},
{"name": "Stop", "key_code": "stop"},
{"name": "Record", "key_code": "record"},
{"name": "Rewind", "key_code": "rewind"},
{"name": "Fast forward", "key_code": "fastforward"}],
[{"name": "Up", "key_code": "up"},
{"name": "Down", "key_code": "down"},
{"name": "Select", "key_code": "select"},
{"name": "Left", "key_code": "left"},
{"name": "Right", "key_code": "right"}],
[{"name": "Dismiss", "key_code": "dismiss"},
{"name": "Home", "key_code": "home"},
{"name": "Side bar", "key_code": "sidebar"}],
[{"name": "Channel up", "key_code": "channelup"},
{"name": "Channel down", "key_code": "channeldown"},
{"name": "Info", "key_code": "i"}],
[{"name": "Red", "key_code": "red"},
{"name": "Green", "key_code": "green"},
{"name": "Yellow", "key_code": "yellow"},
{"name": "Blue", "key_code": "blue"}],
[{"name": "1", "key_code": "1"}, {"name": "2", "key_code": "2"},
{"name": "3", "key_code": "3"}],[{"name": "4", "key_code": "4"},
{"name": "5", "key_code": "5"}, {"name": "6", "key_code": "6"}],
[{"name": "7", "key_code": "7"}, {"name": "8", "key_code": "8"},
{"name": "9", "key_code": "9"}], [{"name": "Input", "key_code": "input"},
{"name": "0", "key_code": "0"}, {"name": "Help", "key_code": "help"}]]',
'00:01:50');

INSERT INTO app.pdu_type (pdu_model, pdu_protocol) VALUES ('netio', 'json');
INSERT INTO app.pdu_type (pdu_model, pdu_protocol) VALUES ('netio', 'snmpv3');
INSERT INTO app.pdu_type (pdu_model, pdu_protocol) VALUES ('apc-nmc', 'ssh');

CREATE OR REPLACE FUNCTION app.f_process_schedule()
  RETURNS TABLE ( activity text, table_name text, chan_schedule_id bigint,
                  rec_id smallint, proc_chan_chg boolean,
                  channel app.channel_nr, datetime timestamptz)
  LANGUAGE plpgsql
AS $$
BEGIN

  PERFORM TRUE FROM app.rec_cfg;
  IF NOT FOUND THEN
    RAISE INFO 'No receivers configured';
  ELSE

    -- Receivers with proc_chan_chg enabled:
    RAISE INFO 'Processing schedule for enabled receivers:';

    -- First, update rec_cfg with most recent records
    RETURN QUERY
    WITH sch AS (
      SELECT DISTINCT ON (s.rec_id) s.chan_schedule_id, s.rec_id,
                                      s.sched_channel, s.datetime
      FROM app.chan_schedule s
      WHERE s.datetime <= transaction_timestamp()
      ORDER BY s.rec_id, s.datetime DESC
    ), act AS (
      SELECT 'update' activity, 'rec_cfg' table_name
    )
    UPDATE app.rec_cfg t
    SET channel = s.sched_channel
    FROM sch s, act a
    WHERE t.rec_id = s.rec_id
    AND t.proc_chan_chg
    RETURNING a.activity, a.table_name, s.chan_schedule_id, s.rec_id,
              t.proc_chan_chg, s.sched_channel, s.datetime;
    IF FOUND THEN
      RAISE NOTICE 'Updated rec_cfg.channel with most recent records';
    ELSE
      RAISE INFO 'No recent records found to update rec_cfg.channel with';
    END IF;

    -- Second, delete past records
    RETURN QUERY
    WITH act AS (
      SELECT 'delete' activity, 'chan_schedule' table_name
    )
    DELETE
    FROM app.chan_schedule s
    USING app.rec_cfg t, act a
    WHERE (s.rec_id = t.rec_id)
    AND s.datetime <= transaction_timestamp()
    AND t.proc_chan_chg
    RETURNING a.activity, a.table_name, s.chan_schedule_id, s.rec_id,
              t.proc_chan_chg, s.sched_channel, s.datetime;
    IF FOUND THEN
      RAISE NOTICE 'Deleted past records';
    ELSE
      RAISE INFO 'No past records found to delete';
    END IF;

    -- Receivers with proc_chan_chg disabled:
    RAISE INFO 'Processing schedule for disabled receivers:';

    -- Delete past records except most recent
    RETURN QUERY
    WITH act AS (
      SELECT 'delete' activity, 'chan_schedule' table_name
    ), que AS (
      SELECT  s.chan_schedule_id, s.rec_id, s.proc_chan_chg,
              s.sched_channel, s.datetime
      FROM  ( SELECT  v.chan_schedule_id, v.rec_id, t.proc_chan_chg,
                      v.sched_channel, v.datetime,
                      COUNT(v.chan_schedule_id)
                      OVER (PARTITION BY v.rec_id
                            ORDER BY v.datetime DESC) recent
              FROM app.chan_schedule v
              INNER JOIN app.rec_cfg t USING (rec_id)
              WHERE v.datetime <= transaction_timestamp()
              AND NOT t.proc_chan_chg
            ) s
      WHERE s.recent > 1
    )
    DELETE
    FROM app.chan_schedule s
    USING que q, act a
    WHERE (s.chan_schedule_id = q.chan_schedule_id)
    RETURNING a.activity, a.table_name, s.chan_schedule_id, s.rec_id,
              q.proc_chan_chg, s.sched_channel, s.datetime;
    IF FOUND THEN
      RAISE NOTICE E'Deleted past records (excluding most recent)\n';
    ELSE
      RAISE INFO E'No past records (excluding most recent) found to delete\n';
    END IF;

  END IF;
END;
$$;

COMMENT ON FUNCTION app.f_process_schedule IS 'Process channel schedule';

CREATE OR REPLACE FUNCTION app.f_config_clear()
  RETURNS void
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  TRUNCATE TABLE app.rec_cfg, app.chan_schedule, app.chan_denylist,
    app.chan_catalog, app.pdu_conn, app.feed_act, app.glb_rec_cfg
  RESTART IDENTITY CASCADE;
END;
$$;

COMMENT ON FUNCTION app.f_config_clear IS 'Clear configuration';

CREATE OR REPLACE FUNCTION app.f_config_defaults()
  RETURNS void
  LANGUAGE plpgsql
AS $$
BEGIN
  INSERT INTO app.glb_rec_cfg (glb_rec_cfg_id, analyze_suspend,
    alert_net_thresh, alert_av_thresh, preview)
  VALUES (0, '15 min', '15 min', '1 hour', true)
  ON CONFLICT (glb_rec_cfg_id) DO UPDATE
  SET glb_rec_cfg_id = EXCLUDED.glb_rec_cfg_id,
    analyze_suspend = EXCLUDED.analyze_suspend,
    alert_net_thresh = EXCLUDED.alert_net_thresh,
    alert_av_thresh = EXCLUDED.alert_av_thresh;

  INSERT INTO app.feed_act (feed_act_group, feed_act_ini_delay,
    feed_act_rex_delay, res_act_ini_delay, res_act_rex_delay)
    VALUES (UNNEST(ARRAY[1,2,3,4]), '10 min', '7 days', '2 min', '1 hour')
  ON CONFLICT (feed_act_group) DO NOTHING;
END;
$$;

COMMENT ON FUNCTION app.f_config_defaults IS 'User configurable defaults';

CREATE OR REPLACE FUNCTION app.f_catalog_def_chan()
  RETURNS TABLE (LIKE app.chan_catalog)
  LANGUAGE plpgsql
AS $$
BEGIN
  RETURN QUERY
  INSERT INTO app.chan_catalog (channel)
  VALUES (UNNEST(ARRAY[
    401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 415, 416, 418,
    419, 420, 423, 424, 425, 427, 858, 859, 860, 861, 862, 863, 864, 865, 866,
    867, 868, 869, 872, 873, 875, 877
  ]))
  ON CONFLICT (channel) DO NOTHING
  RETURNING chan_catalog_id, channel;
END;
$$;

COMMENT ON FUNCTION app.f_catalog_def_chan IS
'Add default channels to catalog';

CREATE OR REPLACE FUNCTION app.f_rec_remote_control(_rec_id integer,
_commands text[])
 RETURNS boolean
 LANGUAGE plpgsql
 STABLE
 RETURNS NULL ON NULL INPUT
AS $$
DECLARE
  _cmd text;
  _payload jsonb;
BEGIN

PERFORM TRUE FROM app.rec_cfg
WHERE rec_id = _rec_id AND remote_ctrl;
IF NOT FOUND THEN
  RAISE WARNING 'remote_ctrl not enabled for receiver %,'
    ' or receiver does not exist', _rec_id;
  RETURN FALSE;
END IF;

-- check for empty array
IF  (CARDINALITY(_commands) = 0)
    OR
    NOT( SELECT ARRAY_AGG(key_code) @> _commands AS valid_cmds
      FROM ( SELECT JSONB_PATH_QUERY(remote_cmds, '$[*][*]') ->> 'key_code'
              AS key_code
              FROM app.rec_gen
              INNER JOIN app.rec_cfg USING (rec_type)
              WHERE rec_id = _rec_id
            ) cmds
    ) THEN
  RAISE WARNING 'One or more commands not valid for receiver %',
    _rec_id;
  RETURN FALSE;
END IF;

SELECT JSONB_BUILD_OBJECT('type', 'remote', 'instance', _rec_id,
                          'data', ARRAY_TO_JSON(_commands))
INTO _payload;
IF NOT app.f_amqp_pub('rec_ctrl.' || _rec_id, _payload) THEN
  RAISE EXCEPTION 'Failed to publish to receiver control (AMQP error)';
END IF;

RETURN TRUE;
END $$;

COMMENT ON FUNCTION app.f_rec_remote_control IS
'Transmit remote control command(s) for specified receiver';

CREATE OR REPLACE FUNCTION app.f_rec_reset_power(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
AS $$
DECLARE
  _payload jsonb;
  _delay interval := '5 minutes';
BEGIN

PERFORM TRUE FROM app.rec_cfg
WHERE rec_id = _rec_id
AND pdu_ctrl;
IF NOT FOUND THEN
  RAISE WARNING 'pdu_ctrl not enabled for receiver %, '
    'or receiver does not exist', _rec_id;
  RETURN FALSE;
END IF;

PERFORM TRUE FROM app.rec_cfg
WHERE rec_id = _rec_id
AND pdu_last_reset + _delay > transaction_timestamp();
IF FOUND THEN
  RAISE WARNING 'Last reset for receiver % was less than % minutes ago',
    _rec_id, app.f_convert_interval_min(_delay);
  RETURN FALSE;
END IF;

UPDATE app.rec_cfg
SET pdu_last_reset = transaction_timestamp()
WHERE rec_id = _rec_id;

SELECT JSONB_BUILD_OBJECT('type', 'reset', 'instance', _rec_id)
INTO _payload;
IF NOT app.f_amqp_pub('rec_ctrl.' || _rec_id, _payload) THEN
  RAISE EXCEPTION 'Failed to publish to receiver control (AMQP error)';
END IF;

RETURN TRUE;
END $$;

COMMENT ON FUNCTION app.f_rec_reset_power IS
'Power cycle specified receiver, then set channel';

CREATE OR REPLACE FUNCTION app.f_rec_reset_channel(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
AS $$
DECLARE
  _payload jsonb;
  _rec_ids integer[] DEFAULT '{}';
BEGIN

IF _rec_id = 0 THEN

  FOR _rec_id IN
    SELECT rec_id
    FROM app.rec_cfg
    WHERE ctrl_conn IS NOT NULL
    ORDER BY rec_id
  LOOP
    _rec_ids := array_append(_rec_ids, _rec_id);
  END LOOP;

  IF NOT FOUND THEN
    RAISE WARNING 'No receivers configured';
    RETURN FALSE;
  END IF;

  RAISE INFO 'Requesting channel reset for receiver(s) %',
              ARRAY_TO_STRING(_rec_ids,', ');

ELSE

  PERFORM TRUE FROM app.rec_cfg
  WHERE rec_id = _rec_id
  AND ctrl_conn IS NOT NULL;
  IF NOT FOUND THEN
    RAISE WARNING 'ctrl_conn not configured for receiver %,'
      ' or receiver does not exist', _rec_id;
    RETURN FALSE;
  END IF;

  _rec_ids := array_agg(_rec_id);

END IF;

FOREACH _rec_id IN ARRAY _rec_ids
LOOP
  SELECT JSONB_BUILD_OBJECT('type', 'channel', 'action', 'reset',
                            'instance', _rec_id)
  INTO _payload;
  IF NOT app.f_amqp_pub('rec_ctrl.' || _rec_id, _payload) THEN
    RAISE EXCEPTION 'Failed to publish to receiver control (AMQP error)';
END IF;
END LOOP;

RETURN TRUE;
END $$;

COMMENT ON FUNCTION app.f_rec_reset_channel IS
'Manually reset channel on (all) receiver(s)';

CREATE OR REPLACE FUNCTION app.f_analyze_suspend(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
AS $$
DECLARE
  _payload jsonb;
  _suspend_time interval;
BEGIN

PERFORM TRUE FROM app.rec_cfg
WHERE rec_id = _rec_id
AND remote_ctrl
AND (vid_analyze OR verify_chan);
IF NOT FOUND THEN
  RAISE WARNING 'remote_ctrl and/or vid_analyze/verify_chan not enabled for '
    'receiver %, or receiver does not exist', _rec_id;
  RETURN FALSE;
END IF;

SELECT analyze_suspend FROM app.glb_rec_cfg WHERE glb_rec_cfg_id = 0
INTO _suspend_time;
IF NOT FOUND THEN
  RAISE WARNING 'analyze_suspend not configured (glb_rec_cfg)';
  RETURN FALSE;
END IF;

RAISE NOTICE 'Receiver % analyze service suspended until %', _rec_id,
  TO_CHAR(CURRENT_TIMESTAMP(0)+_suspend_time, 'YYYY-MM-DD HH24:MI OF');

SELECT JSONB_BUILD_OBJECT('type', 'svc_misc', 'data', 'analyze',
                          'action', 'suspend', 'instance', _rec_id)
INTO _payload;
IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
  RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
END IF;

RETURN TRUE;
END $$;

COMMENT ON FUNCTION app.f_analyze_suspend IS
'Suspend analzye service for specified receiver';

CREATE OR REPLACE FUNCTION app.f_analyze_continue(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
AS $$
DECLARE
  _payload jsonb;
BEGIN

PERFORM TRUE FROM app.rec_cfg
WHERE rec_id = _rec_id
AND remote_ctrl
AND (vid_analyze OR verify_chan);
IF NOT FOUND THEN
  RAISE WARNING 'remote_ctrl and/or vid_analyze/verify_chan not enabled for '
  'receiver %, or receiver does not exist', _rec_id;
  RETURN FALSE;
END IF;

SELECT JSONB_BUILD_OBJECT('type', 'svc_misc', 'data', 'analyze',
                          'action', 'continue', 'instance', _rec_id)
INTO _payload;
IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
  RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
END IF;

RETURN TRUE;
END $$;

COMMENT ON FUNCTION app.f_analyze_continue IS
'Unsuspend analyze service for specified receiver';

CREATE OR REPLACE FUNCTION app.f_analyze_reset_rate_limit(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
AS $$
DECLARE
  _payload jsonb;
BEGIN

PERFORM TRUE FROM app.rec_cfg
WHERE rec_id = _rec_id
AND (vid_analyze OR verify_chan);
IF NOT FOUND THEN
  RAISE WARNING 'vid_analyze/verify_chan not enabled for '
  'receiver %, or receiver does not exist', _rec_id;
  RETURN FALSE;
END IF;

SELECT JSONB_BUILD_OBJECT('type', 'svc_misc', 'data', 'analyze',
                          'action', 'reset_rate_limit', 'instance', _rec_id)
INTO _payload;
IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
  RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
END IF;

RETURN TRUE;
END $$;

COMMENT ON FUNCTION app.f_analyze_reset_rate_limit IS
'Reset analyze service rate limiter for specified receiver';

CREATE OR REPLACE
FUNCTION app.f_analyze_reset_feed_act_delay(_feed_act_group integer)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
AS $$
DECLARE
  _payload jsonb;
BEGIN

PERFORM TRUE FROM app.feed_act
WHERE feed_act_group = _feed_act_group;
IF NOT FOUND THEN
  RAISE WARNING 'feed_act_group % does not exist', _feed_act_group;
  RETURN FALSE;
END IF;

SELECT JSONB_BUILD_OBJECT('type', 'svc_misc', 'data', 'analyze',
                          'action', 'reset_feed_act_delay',
                          'instance', _feed_act_group)
INTO _payload;
IF NOT app.f_amqp_pub('comp_mgmt', _payload) THEN
  RAISE EXCEPTION 'Failed to publish to component manager (AMQP error)';
END IF;

RETURN TRUE;
END $$;

COMMENT ON FUNCTION app.f_analyze_reset_feed_act_delay IS
'Reset analyze service feed_act_group ini delay+rex delay';

CREATE OR REPLACE FUNCTION app.f_version()
  RETURNS integer
  LANGUAGE sql
  IMMUTABLE PARALLEL SAFE
  AS 'SELECT 10002';

COMMENT ON FUNCTION app.f_version IS 'Version';

RESET ROLE;

DROP ROLE IF EXISTS cr_scheduler;
CREATE USER cr_scheduler NOINHERIT;

GRANT USAGE ON SCHEMA app TO cr_scheduler;
GRANT EXECUTE ON FUNCTION app.f_process_schedule TO cr_scheduler;
GRANT SELECT ON TABLE app.rec_cfg TO cr_scheduler;
GRANT UPDATE (channel) ON TABLE app.rec_cfg to cr_scheduler;
GRANT SELECT, DELETE ON TABLE app.chan_schedule TO cr_scheduler;
