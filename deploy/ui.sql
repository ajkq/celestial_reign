\set ON_ERROR_STOP on

\connect celestial_reign

SELECT EXISTS ( SELECT FROM information_schema.schemata
                WHERE schema_name = 'ui') AS schema_exists
\gset
\if :schema_exists
  \echo 'User interface schema already exists'
  \quit
\endif

SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_roles
                    WHERE rolname = 'cr_ui' ) AS user_not_exists
\gset
\if :user_not_exists
  CREATE ROLE cr_ui NOINHERIT;
\endif

ALTER DEFAULT PRIVILEGES FOR USER cr_ui REVOKE EXECUTE ON ROUTINES FROM PUBLIC;

CREATE SCHEMA ui AUTHORIZATION cr_ui;

COMMENT ON SCHEMA ui IS 'User interface';

SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_roles
                    WHERE rolname = 'cr_g_users' ) AS user_not_exists
\gset
\if :user_not_exists
  CREATE ROLE cr_g_users;
\endif

SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_roles
                    WHERE rolname = 'cr_g_user' ) AS user_not_exists
\gset
\if :user_not_exists
  CREATE ROLE cr_g_user;
\endif

SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_roles
                    WHERE rolname = 'cr_g_operator' ) AS user_not_exists
\gset
\if :user_not_exists
  CREATE ROLE cr_g_operator;
\endif

SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_roles
                    WHERE rolname = 'cr_g_admin' ) AS user_not_exists
\gset
\if :user_not_exists
  CREATE ROLE cr_g_admin;
\endif

GRANT cr_g_users to cr_g_user;
GRANT cr_g_users to cr_g_operator;
GRANT cr_g_users to cr_g_admin;
GRANT USAGE on SCHEMA ui to cr_g_users;
GRANT USAGE on SCHEMA app to cr_g_users;

SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_roles
                    WHERE rolname = 'sadmin' ) AS user_not_exists
\gset
\if :user_not_exists
  CREATE USER sadmin INHERIT;
\endif

GRANT cr_g_admin to sadmin;

GRANT cr_ui to cr_g_super;

GRANT USAGE on SCHEMA app to cr_ui;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE app.rec_cfg TO cr_ui;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE app.pdu_conn TO cr_ui;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE app.glb_rec_cfg TO cr_ui;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE app.feed_act TO cr_ui;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE app.chan_schedule TO cr_ui;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE app.chan_denylist TO cr_ui;
GRANT SELECT, UPDATE, INSERT, DELETE ON TABLE app.chan_catalog TO cr_ui;
GRANT SELECT ON TABLE app.rec_gen TO cr_ui;
GRANT SELECT ON TABLE app.pdu_type TO cr_ui;
GRANT SELECT ON TABLE app.comp_mgmt_cfg_sync TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_amqp_pub TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_rec_remote_control TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_analyze_suspend TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_analyze_reset_rate_limit TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_analyze_continue TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_rec_reset_power TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_rec_reset_channel TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_config_clear TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_config_defaults TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_catalog_def_chan TO cr_ui;
GRANT EXECUTE ON FUNCTION app.f_analyze_reset_feed_act_delay TO cr_ui;

SELECT NOT EXISTS ( SELECT FROM pg_catalog.pg_roles
                    WHERE rolname = 'cr_usermgmt' ) AS user_not_exists
\gset
\if :user_not_exists
  CREATE ROLE cr_usermgmt;
\endif

ALTER DEFAULT PRIVILEGES FOR USER cr_usermgmt REVOKE EXECUTE ON ROUTINES
FROM PUBLIC;
ALTER ROLE cr_usermgmt CREATEROLE;
GRANT cr_ui to cr_usermgmt;

GRANT cr_g_users to cr_usermgmt WITH ADMIN OPTION;

SET ROLE cr_ui;

-- Adminer uses input fields for varchar, textarea for text
CREATE OR REPLACE VIEW ui.channel AS
SELECT rec_id, COALESCE(descr,'')::varchar(50) AS description, channel,
CASE WHEN proc_chan_chg = TRUE
THEN 'yes'::varchar(5) ELSE 'no'::varchar(5) END AS enabled
FROM app.rec_cfg
ORDER BY rec_id;

-- Cannot use column permissions since Adminer usually updates
-- all columns. Instead, discard everything but channel
CREATE RULE channel_u AS
ON UPDATE TO ui.channel
DO INSTEAD
  UPDATE app.rec_cfg
  SET channel = NEW.channel
  WHERE rec_id = OLD.rec_id;

COMMENT ON VIEW ui.channel IS 'User: Channel assignment';

GRANT USAGE on SCHEMA ui to cr_g_users;

GRANT SELECT, UPDATE ON ui.channel TO cr_g_users;

CREATE OR REPLACE VIEW ui.chan_schedule AS
SELECT chan_schedule_id, rec_id, sched_channel, datetime
FROM app.chan_schedule
ORDER BY chan_schedule_id;

-- Adminer doesn't realize that the primary key is system generated and
-- tries to insert/update it
CREATE RULE chan_schedule_i AS
ON INSERT TO ui.chan_schedule
DO INSTEAD
  INSERT INTO app.chan_schedule(rec_id, sched_channel, datetime)
  VALUES (NEW.rec_id, NEW.sched_channel, NEW.datetime);

CREATE RULE chan_schedule_u AS
ON UPDATE TO ui.chan_schedule
DO INSTEAD
  UPDATE app.chan_schedule
  SET rec_id = NEW.rec_id, sched_channel = NEW.sched_channel,
  datetime = NEW.datetime
  WHERE chan_schedule_id = OLD.chan_schedule_id;

COMMENT ON VIEW ui.chan_schedule IS 'User: Scheduled channel assignment';

GRANT SELECT, UPDATE, INSERT, DELETE ON ui.chan_schedule TO cr_g_users;

CREATE OR REPLACE VIEW ui.rec_cfg AS
SELECT  rec_id, descr, channel, proc_chan_chg, rec_type, ctrl_conn,
        verify_chan, vid_src_conn, vid_analyze, no_feed_act, res_act,
        feed_act_group, backup_chan, pin, remote_ctrl, pdu_ctrl,
        pdu_conn, pdu_outlet, alert_net, alert_av
FROM app.rec_cfg
ORDER BY rec_id;

COMMENT ON VIEW ui.rec_cfg IS 'Admin: Receiver settings';

GRANT SELECT, UPDATE, INSERT, DELETE ON ui.rec_cfg TO cr_g_admin;

CREATE OR REPLACE VIEW ui.pdu_conn AS
SELECT pdu_conn, pdu_model, pdu_protocol, pdu_host, pdu_user, pdu_password
FROM app.pdu_conn
ORDER BY pdu_conn;

COMMENT ON VIEW ui.pdu_conn IS 'Admin: PDU connection';

GRANT SELECT, UPDATE, INSERT, DELETE ON ui.pdu_conn TO cr_g_admin;

CREATE OR REPLACE VIEW ui.glb_rec_cfg AS
SELECT  site_name, analyze_suspend, alert_net_thresh, alert_av_thresh,
        alert_email, alert, coverage, preview, alarm
FROM app.glb_rec_cfg;

COMMENT ON VIEW ui.glb_rec_cfg IS 'Admin: Global receiver settings';

GRANT SELECT, UPDATE ON ui.glb_rec_cfg TO cr_g_admin;

CREATE OR REPLACE VIEW ui.feed_act AS
SELECT  feed_act_group, feed_act_ini_delay, feed_act_rex_delay,
        res_act_ini_delay, res_act_rex_delay
FROM app.feed_act
ORDER BY feed_act_group;

COMMENT ON VIEW ui.feed_act IS 'Admin: Feed action settings';

GRANT SELECT, UPDATE, INSERT, DELETE ON ui.feed_act TO cr_g_admin;

CREATE OR REPLACE VIEW ui.chan_denylist AS
SELECT chan_denylist_id, rec_id, deny_channel
FROM app.chan_denylist
ORDER BY rec_id;

CREATE RULE chan_denylist_i AS
ON INSERT TO ui.chan_denylist
DO INSTEAD
  INSERT INTO app.chan_denylist(rec_id, deny_channel)
  VALUES (NEW.rec_id, NEW.deny_channel);

CREATE RULE chan_denylist_u AS
ON UPDATE TO ui.chan_denylist
DO INSTEAD
  UPDATE app.chan_denylist
  SET rec_id = NEW.rec_id, deny_channel = NEW.deny_channel
  WHERE chan_denylist_id = OLD.chan_denylist_id;

COMMENT ON VIEW ui.chan_denylist IS 'Admin: Denied channels';

GRANT SELECT, UPDATE, INSERT, DELETE ON ui.chan_denylist TO cr_g_admin;
GRANT SELECT ON ui.chan_denylist TO cr_g_user, cr_g_operator;

CREATE OR REPLACE VIEW ui.chan_catalog AS
SELECT chan_catalog_id, channel
FROM app.chan_catalog
ORDER BY channel;

CREATE RULE chan_catalog_i AS
ON INSERT TO ui.chan_catalog
DO INSTEAD
  INSERT INTO app.chan_catalog(channel)
  VALUES (NEW.channel);

CREATE RULE chan_catalog_u AS
ON UPDATE TO ui.chan_catalog
DO INSTEAD
  UPDATE app.chan_catalog
  SET channel = NEW.channel
  WHERE chan_catalog_id = OLD.chan_catalog_id;

COMMENT ON VIEW ui.chan_catalog IS 'Admin: Globally available channels';

GRANT SELECT, UPDATE, INSERT, DELETE ON ui.chan_catalog TO cr_g_admin;
GRANT SELECT ON ui.chan_catalog TO cr_g_user, cr_g_operator;

CREATE OR REPLACE VIEW ui.avail_rc AS
SELECT r.rec_id,
( SELECT string_agg(k.key_code, ', ') AS avail_rc
  FROM  ( SELECT JSONB_PATH_QUERY(rec_gen.remote_cmds, '$[*][*]')
            ->> 'key_code' AS key_code
          FROM app.rec_gen
          INNER JOIN app.rec_cfg t USING (rec_type)
          WHERE t.rec_id = r.rec_id
        ) k
)
FROM app.rec_cfg r
WHERE r.remote_ctrl
ORDER BY r.rec_id;

COMMENT ON VIEW ui.avail_rc IS 'Operator: Available remote commands';

GRANT SELECT ON ui.avail_rc TO cr_g_operator, cr_g_admin;

CREATE OR REPLACE FUNCTION ui.rec_remote_control(_rec_id integer,
_commands text)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _arr_commands text[];
BEGIN
  _commands = TRIM(REGEXP_REPLACE(_commands, '\s+', ' ', 'g'));
  _commands = LOWER(_commands);
  _arr_commands = STRING_TO_ARRAY(_commands, ' ');

  RETURN app.f_rec_remote_control(_rec_id, _arr_commands);
END;
$$;

COMMENT ON FUNCTION ui.rec_remote_control IS
'Transmit remote control command(s) for specified receiver';

GRANT EXECUTE ON FUNCTION ui.rec_remote_control TO cr_g_admin,
cr_g_operator;

CREATE OR REPLACE FUNCTION ui.analyze_suspend(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  RETURN app.f_analyze_suspend(_rec_id);
END;
$$;

COMMENT ON FUNCTION ui.analyze_suspend IS
'Suspend analzye service for specified receiver';

GRANT EXECUTE ON FUNCTION ui.analyze_suspend TO cr_g_admin, cr_g_operator;

CREATE OR REPLACE FUNCTION ui.analyze_continue(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  RETURN app.f_analyze_continue(_rec_id);
END;
$$;

COMMENT ON FUNCTION ui.analyze_continue IS
'Unsuspend analyze service for specified receiver';

GRANT EXECUTE ON FUNCTION ui.analyze_continue TO cr_g_admin, cr_g_operator;

CREATE OR REPLACE FUNCTION ui.analyze_reset_rate_limit(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  RETURN app.f_analyze_reset_rate_limit(_rec_id);
END;
$$;

COMMENT ON FUNCTION ui.analyze_reset_rate_limit IS
'Reset analyze service rate limiter for specified receiver';

GRANT EXECUTE ON FUNCTION ui.analyze_reset_rate_limit
TO cr_g_admin, cr_g_operator;

CREATE OR REPLACE
FUNCTION ui.analyze_reset_feed_act_delay(_feed_act_group integer)
  RETURNS boolean
  LANGUAGE plpgsql
  STABLE
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  RETURN app.f_analyze_reset_feed_act_delay(_feed_act_group);
END;
$$;

COMMENT ON FUNCTION ui.analyze_reset_feed_act_delay IS
'Reset analyze service feed_act_group ini delay+rex delay';

GRANT EXECUTE ON FUNCTION ui.analyze_reset_feed_act_delay
TO cr_g_admin;

CREATE OR REPLACE FUNCTION ui.rec_reset_power(_rec_id integer)
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  RETURN app.f_rec_reset_power(_rec_id);
END;
$$;

COMMENT ON FUNCTION ui.rec_reset_power IS
'Power cycle specified receiver, then set channel';

GRANT EXECUTE ON FUNCTION ui.rec_reset_power TO cr_g_admin, cr_g_operator;

CREATE OR REPLACE FUNCTION ui.rec_reset_channel(_rec_id varchar(5))
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _rec_id_int integer;
BEGIN

  IF _rec_id !~ '^(?:\*|(?!0$)[0-9]+)$' THEN
    RAISE WARNING 'Input validation error';
    RETURN FALSE;
  END IF;

  IF _rec_id = '*' THEN
    _rec_id := 0;
  END IF;

  _rec_id_int := _rec_id::integer;

  RETURN app.f_rec_reset_channel(_rec_id_int);
END;
$$;

COMMENT ON FUNCTION ui.rec_reset_channel IS
'Manually reset channel on (all) receiver(s)';

GRANT EXECUTE ON FUNCTION ui.rec_reset_channel TO cr_g_admin,
cr_g_operator, cr_g_user;

CREATE OR REPLACE FUNCTION ui.config_reset(_action_confirm varchar(20),
_catalog_def_chan boolean)
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  IF _action_confirm <> 'Do it' THEN
    RAISE INFO 'Enter the the phrase "Do it" (without quotes) into the '
      'field _action_confirm to proceed';
    RETURN FALSE;
  END IF;
  PERFORM app.f_config_clear();
  PERFORM app.f_config_defaults();
  IF _catalog_def_chan THEN
    PERFORM app.f_catalog_def_chan();
  END IF;
  RETURN TRUE;
END;
$$;

COMMENT ON FUNCTION ui.config_reset IS 'Factory reset';

GRANT EXECUTE ON FUNCTION ui.config_reset TO cr_g_admin;

CREATE OR REPLACE FUNCTION ui.password_change(_new_password text)
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
AS $$
DECLARE
  _password text := _new_password;
BEGIN
  IF (_new_password = '') THEN
    -- Default for empty password is removing it which effectively
    -- disables user
    RAISE WARNING 'Password cannot be empty';
    RETURN FALSE;
  END IF;

  -- Password complexity check handled by passwordcheck module
  -- Otherwise user could run command manually and bypass complexity check
  EXECUTE FORMAT($f$ALTER ROLE CURRENT_USER WITH PASSWORD '%s'$f$, _password);
  RETURN TRUE;
END;
$$;

COMMENT ON FUNCTION ui.password_change IS 'Change current user password';

GRANT EXECUTE ON FUNCTION ui.password_change TO cr_g_users;

CREATE TYPE ui.user_list AS ("user" text, "group" text);
-- Adminer fails at RETURN TABLE, and displays SETOF <type> like SETOF RECORD
CREATE OR REPLACE FUNCTION ui.user_list()
  RETURNS SETOF ui.user_list
  LANGUAGE sql
AS $$
  SELECT member AS user, REPLACE(role,'cr_g_','') AS group
  FROM ( SELECT r.rolname AS role, m.rolname AS member, g.rolname AS grantor
        FROM pg_catalog.pg_auth_members a
        INNER JOIN pg_catalog.pg_roles r ON (a.roleid = r.oid)
        INNER JOIN pg_catalog.pg_roles m ON (a.member = m.oid)
        INNER JOIN pg_catalog.pg_roles g ON (a.grantor = g.oid) ) t
  WHERE grantor = 'cr_usermgmt'
  AND (role = 'cr_g_admin' OR role = 'cr_g_operator' OR role = 'cr_g_user');
$$;

COMMENT ON FUNCTION ui.user_list IS 'List application users';

GRANT EXECUTE ON FUNCTION ui.user_list TO cr_g_admin;

SET ROLE cr_usermgmt;

CREATE OR REPLACE FUNCTION ui.user_create(_user varchar(32), _password text,
_level varchar(20))
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  IF _user !~ '^[a-z0-9-]{3,32}$' THEN
    RAISE WARNING 'Invalid user name'
    USING DETAIL = 'Lower case alphanumeric letters, hyphens; 3-32 characters';
    RETURN FALSE;
  END IF;

  PERFORM TRUE FROM pg_catalog.pg_user
  WHERE usename = _user;
  IF FOUND THEN
    RAISE WARNING 'User % already exists', _user;
    RETURN FALSE;
  END IF;

  IF _level !~* '^(user|operator|admin)$' THEN
    RAISE WARNING 'Invalid permissions level'
    USING DETAIL = 'admin, operator, or user';
    RETURN FALSE;
  END IF;

  IF (_password = '') THEN
    RAISE WARNING 'Password cannot be empty';
    RETURN FALSE;
  END IF;

  -- Password complexity check handled by passwordcheck module

  EXECUTE FORMAT('CREATE USER %I INHERIT', _user);

  IF LOWER(_level) = 'user' THEN
    EXECUTE FORMAT('GRANT cr_g_user TO %I', _user);
  ELSIF LOWER(_level) = 'operator' THEN
    EXECUTE FORMAT('GRANT cr_g_operator TO %I', _user);
  ELSIF LOWER(_level) = 'admin' THEN
    EXECUTE FORMAT('GRANT cr_g_admin TO %I', _user);
  END IF;

  EXECUTE FORMAT($f$ALTER ROLE %I WITH PASSWORD '%s'$f$, _user, _password);
  RETURN TRUE;
END;
$$;

COMMENT ON FUNCTION ui.user_create IS 'Create application user';

GRANT EXECUTE ON FUNCTION ui.user_create TO cr_g_admin;

CREATE OR REPLACE FUNCTION ui.user_reset(_user varchar(32), _new_password text)
  RETURNS boolean
  LANGUAGE plpgsql
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
DECLARE
  _password text := _new_password;
BEGIN
  IF _user !~ '^[a-z0-9-]{3,32}$' THEN
    RAISE WARNING 'Invalid user name'
    USING DETAIL = 'Lower case alphanumeric letters, hyphens; 3-32 characters';
    RETURN FALSE;
  END IF;

  IF _user = SESSION_USER THEN
    RAISE WARNING 'Use function password_change to change password of '
    'logged-in user';
    RETURN FALSE;
  END IF;

  -- Only touch users that were created during database init or by a user_*
  -- function (cr_usermgmt role)
  PERFORM role, member, grantor
  FROM ( SELECT r.rolname AS role, m.rolname AS member, g.rolname AS grantor
         FROM pg_catalog.pg_auth_members a
         INNER JOIN pg_catalog.pg_roles r ON (a.roleid = r.oid)
         INNER JOIN pg_catalog.pg_roles m ON (a.member = m.oid)
         INNER JOIN pg_catalog.pg_roles g ON (a.grantor = g.oid) ) t
  WHERE member = _user
  AND grantor = 'cr_usermgmt'
  AND (role = 'cr_g_admin' OR role = 'cr_g_operator' OR role = 'cr_g_user');
  IF NOT FOUND THEN
    RAISE WARNING 'User % not an application user', _user;
    RETURN FALSE;
  END IF;

  IF (_password = '') THEN
    RAISE WARNING 'Password cannot be empty';
    RETURN FALSE;
  END IF;

  -- Password complexity check handled by passwordcheck module

  EXECUTE FORMAT($f$ALTER ROLE %I WITH PASSWORD '%s'$f$, _user, _password);
  RETURN TRUE;
END;
$$;

COMMENT ON FUNCTION ui.user_reset IS 'Reset application user password';

GRANT EXECUTE ON FUNCTION ui.user_reset TO cr_g_admin;

CREATE OR REPLACE FUNCTION ui.user_delete(_user varchar(32))
  RETURNS boolean
  LANGUAGE plpgsql
  RETURNS NULL ON NULL INPUT
  SET search_path TO 'pg_catalog', 'pg_temp'
  SECURITY DEFINER
AS $$
BEGIN
  IF _user !~ '^[a-z0-9-]{3,32}$' THEN
    RAISE WARNING 'Invalid user name'
    USING DETAIL = 'Lower case alphanumeric letters, hyphens; 3-32 characters';
    RETURN FALSE;
  END IF;

  PERFORM role, member, grantor
  FROM ( SELECT r.rolname AS role, m.rolname AS member, g.rolname AS grantor
         FROM pg_catalog.pg_auth_members a
         INNER JOIN pg_catalog.pg_roles r ON (a.roleid = r.oid)
         INNER JOIN pg_catalog.pg_roles m ON (a.member = m.oid)
         INNER JOIN pg_catalog.pg_roles g ON (a.grantor = g.oid) ) t
  WHERE member = _user
  AND grantor = 'cr_usermgmt'
  AND (role = 'cr_g_admin' OR role = 'cr_g_operator' OR role = 'cr_g_user');
  IF NOT FOUND THEN
    RAISE WARNING 'User % not an application user', _user;
    RETURN FALSE;
  END IF;

  IF _user = SESSION_USER THEN
    RAISE WARNING 'Cannot delete logged-in user';
    RETURN FALSE;
  END IF;

  EXECUTE FORMAT('DROP USER %I', _user);
  RETURN TRUE;
END;
$$;

COMMENT ON FUNCTION ui.user_delete IS 'Delete application user';

GRANT EXECUTE ON FUNCTION ui.user_delete TO cr_g_admin;
